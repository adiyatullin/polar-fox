import drums from "./drums";
import gameConfig from "./gameConfig";
import BootScene from "./scenes/bootScene";
import gameController from "./scenes/gameController";
import GameInterface from "./scenes/gameInterface";
import graphicsScene from "./scenes/graphicsScene";
import MainScene from "./scenes/mainScene";
import RiskScene from "./scenes/riskScene";
import SettingsScene from "./scenes/settingsScene";
import soundController from "./scenes/soundController";
import "./styles/style.scss";
const SCALE = gameConfig.SCALE;

const config: GameConfig = {
  autoFocus: true,
  type: Phaser.WEBGL,
  parent: "content",
  width: gameConfig.CANVAS_SIZE.WIDTH,
  height: gameConfig.CANVAS_SIZE.HEIGHT,
  scene: [
    BootScene, gameController, graphicsScene, soundController, GameInterface, MainScene, RiskScene, SettingsScene
  ]
};


window.onload = () => {
  document.title = "Polar Fox";
  const game = new Phaser.Game(config);
};
window.addEventListener("touchmove", onTouchMove, { passive: false });
function onTouchMove(e) {
  // console.log('touch'); e.preventDefault;
}
const handler = {
  touchend() {
    launchFullscreen(document.documentElement);
  },
};
document.body.
  addEventListener("touchend", handler.touchend.bind(handler), true);

function launchFullscreen(element) {
  if (element.requestFullscreen) {
    element.requestFullscreen();
  } else if (element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if (element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if (element.msRequestFullscreen) {
    element.msRequestFullscreen();
  }
}

let xhr = null;
// определим переменную xhr:
try {
  xhr = new XMLHttpRequest();
} catch (failed) {
  xhr = false;
}
if (!xhr) {
  alert("Error! Your Internet browser don't Allow HttpRequest!");
} else {
  // getCustomerInfo();
}

// function getCustomerInfo() {
//   const params = "&Name=" + encodeURIComponent("Albert") +
//     ";Value=" + encodeURIComponent("1234509876");
//   xhr.open("POST", "http://13.81.211.232/submit?" + params, true);
//   xhr.onreadystatechange = updatePage;
//   xhr.send();
// }

// function getCustomerInfo() {
//   const body = "Name=" + encodeURIComponent("Albert") +
//     "&Value=" + encodeURIComponent("1234509876");
//   xhr.open("POST", "http://13.81.211.232/submit", true);
//   xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
//   xhr.onreadystatechange = updatePage;
//   xhr.send(body);
// }
function getCustomerInfo() {
  const json = JSON.stringify({
    Name: "Albert",
    Value: "1234509876"
  });
  xhr.open("POST", "http://13.81.211.232:5050", true);
  // xhr.open("POST", "http://localhost:8888", true);
  // xhr.open("POST", "http://httpbin.org:80/post", true);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.onreadystatechange = updatePage;
  xhr.send(json);
}
function updatePage() {
  if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
    console.log(xhr.responseText);
  } else {
    // alert("Server message:" + xhr.responseText);
  }
}
// function getCustomerInfo() {
//   // const req = JSON.stringify({
//   //    args: JSON.stringify({
//   //     method: "Command",
//   //     factors: "2"
//   //   })
//   // });
//   const req = "/submit?name=Ivan&surname=Ivanov";
//   console.log(req);
//   // const url = "http://httpbin.org/post";
//   const url = "http://13.81.211.232/";

//   srvRequest.open("GET", url, true);
//   srvRequest.onreadystatechange = updatePage;
//   srvRequest.send(req);
// }
