﻿export default class GravityBounce {
    // const
    private static EPSILON: number = 1.0 / 1000.0;
    private static BOUNCES_MAX: number = 10;
    // number of bounces
    private bounces: number;
    // elasticity - how high is next amplitude
    private elasticity: number;
    // acceleration for requested parameters
    private acceleration: number;
    // start from peek or from bottom
    private halveFirstBounce: boolean;
    // duration of particular bounces
    // private _bounceDuration: Array<number> = [];
    // or
    private bounceDuration: number[] = [];
    // height of particular bounces
    private bounceHeight: number[] = [];
    // bounce velocity
    private bouceVelocity: number[] = [];

    // -------------------------------------------------------------------------
    constructor(aBounces: number, aElasticity: number = -1.0, aHalveFirstBounce: boolean = true) {
        // check parameters validity - either bounces or elasticity must be grater than zero
        if (aBounces <= 0 && aElasticity < 0.0) {
            console.log("Invalid parameters (aBounces = " + aBounces + ", aElasticity = " + aElasticity + ")");
            return;
        }
        // calculate missing parameters
        // if defined bounces but not elasticity
        /*
            If we know the number of bounces and elasticity is unknown we have to calculate it.
            It will have such a value that after requested number of bounces the potential next bounce would
            had its height less or equal to EPSILON. It comes from calculation:
// tslint:disable-next-line:no-trailing-whitespace

            EPSILON = elasticity ^ bounces
            elasticity = (EPSILON) ^ (1 / bounces)
        */
        if (aBounces > 0 && aElasticity < 0.0) {
            aElasticity = Math.pow(GravityBounce.EPSILON, 1.0 / aBounces);
        } else if (aElasticity > 0.0 && aBounces <= 0) {
            // if defined elasticity but not bounces
            if (aElasticity >= 1.0) {
                console.log("Elasticity must be less than 1");
                return;
            }
            // EPSILON = elasticity ^ bounces ... aBounces = log_elasticity(EPSILON) = ln EPSILON / ln elasticity
            aBounces = Math.floor(Math.log(GravityBounce.EPSILON) / Math.log(aElasticity));
            aBounces = Math.min(aBounces, GravityBounce.BOUNCES_MAX);
        }
        // get "some" acceleration and calculate time for bounces
        const acceleration: number = GravityBounce.EPSILON;
        let totalDuration: number = 0.0;
        let height = 1.0;
        for (let i = 0; i < aBounces; i++) {
            // s = 1/2 a * t^2 ... 2s / a = t^2 ... sqrt(2s / a) = t
            let duration: number = Math.sqrt(2 * height / acceleration) * 2;

            if (aHalveFirstBounce && i === 0) {
                duration /= 2;
            }
            this.bounceDuration[i] = duration;
            this.bounceHeight[i] = height;
            totalDuration += duration;
            height *= aElasticity;
        }
        // adjust total duration to fit 1 second
        for (let i = 0; i < aBounces; i++) {
            const duration: number = this.bounceDuration[i] / totalDuration;
            this.bounceDuration[i] = duration;
        }
        // calculate new acceleration
        const firstHalfBounceDuration = aHalveFirstBounce ? this.bounceDuration[0] : this.bounceDuration[0] / 2;
        // s = 1/2 a * t^2 ... 2s / t^2 = a
        this.acceleration = 2.0 / (firstHalfBounceDuration * firstHalfBounceDuration);
        // calculate initial bounce velocities
        for (let i = 0; i < aBounces; i++) {
            // v = v0 + at ... on the top of bounce the v equals zero => v0 = -at
            // if bounce starts halved (on top) than its initial velocity is zero
            // halve duration of each bounce (as it contains the way up and down)
            if (i === 0 && aHalveFirstBounce) {
                this.bouceVelocity[i] = 0.0;
            } else {
                this.bouceVelocity[i] = this.bounceDuration[i] / 2.0 * this.acceleration;
            }
        }
        // change the sign of acceleration to point downwards
        this.acceleration = - this.acceleration;
        // debug output
        /*
            LOGD("Bounces: %i, Elasticity: %f, Acceleration: %f, Duration: %f, HalveFirstBounce %s",
                mBounces, mElasticity, mAcceleration, mDuration, mHalveFirstBounce ? "true" : "false");
            for (s32 i = 0; i < mBounces; i++)
            {
                LOGD("Bounce %i: height = %f, duration = %f, velocity = %f",
                i, mBounceHeight[i], mBounceDuration[i], mBouceVelocity[i]);
            }
        */
        // store parameters
        this.bounces = aBounces;
        this.elasticity = aElasticity;
        this.halveFirstBounce = aHalveFirstBounce;
    }
    // -------------------------------------------------------------------------
    public easing(aDurationProgress: number) {

        let index: number = 0;
        let totalDuration: number = 0.0;

        // get index to particular bounce
        while (index < this.bounces && aDurationProgress > totalDuration + this.bounceDuration[index]) {
            totalDuration += this.bounceDuration[index];
            ++index;
        }

        // security check if we are not beyond last entry
        if (index >= this.bounces) {
            return 0;
        }

        // get duration within bounce (if not the first one)
        aDurationProgress = aDurationProgress - totalDuration;


        let height = 0.0;
        if (index === 0 && this.halveFirstBounce) {
            // height = height + 1/2 * mAcceleration * aDurationProgress^2
            height = 1.0 + this.acceleration * (aDurationProgress * aDurationProgress) / 2.0;
        } else {
            // height = mBounceVelocity * aDurationProgress + 1/2 * mAcceleration * aDurationProgress^2
            // height = aDurationProgress * (mBounceVelocity + 1/2 * mAcceleration * aDurationProgress)
            height = aDurationProgress * (this.bouceVelocity[index] + (this.acceleration * aDurationProgress) / 2.0);
        }
        return height;
    }


}
