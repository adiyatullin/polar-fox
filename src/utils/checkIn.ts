﻿import gameConfig, { WinCoeffs, winLines } from "../gameConfig";

export default class CheckIn {
    private WinLines = [];
    constructor() {
        //
    }
    public inRange(x, min, max) {
        return ((x - min) * (x - max) <= 0);
    }

    public calculate(aLineNumber: number, aInput: number[]) {
        const winLinesCherep = [];
        function cherep3(lineIndex) {
            let result = false;
            const line = lines[lineIndex];
            const k = []; const a = []; let max = 0; let p = 0;
            for (let i = 0; i < 5; i++) {
                k.push(0);
                a.push(0);
            }
            for (let i = 0; i < 5; i++) {
                for (let j = i; j < 5; j++) {
                    if ((line[i] === line[j]) && (line[i] === 6)) {
                        k[p]++;
                        a[p] = line[i];
                    } else { break; }
                }
                p++;
            }
            max = k[0];
            for (let i = 0; i < p; i++) {
                if (max < k[i]) {
                    max = k[i];
                }
            }
            if (max >= 3) {
                for (let i = 0; i < 5; i++) {
                    if (a[i] === 0) {
                        a[i] = -1;
                    }
                }
                const sovCh = [];
                result = true;
                // console.log("lineIndex = " + (lineIndex + 1) + "; " + max + "; k = " + k + "; a = " + a);
                for (let j = 0; j < 5; j++) {
                    for (let i = 0; i < 5; i++) {
                        if ((a[i] === winLines[6][j][i]) || (a[i] === 6)) {
                            sovCh.push({ table: 6, line: (lineIndex + 1), col: i, row: j, value: a[i] });
                        }
                    }
                }
                // console.log(sovCh);
                const sovChTwo = [];
                for (let i = 0; i < 5; i++) {
                    sovChTwo.push([]);
                }
                for (let i = 0; i < 5; i++) {
                    sovCh.forEach((element) => {
                        if (element.row === i) {
                            sovChTwo[i]++;
                        }
                    });
                }
                // console.log(sovChTwo);
                let index = 0;
                for (let i = 1; i < 5; i++) {
                    if (sovChTwo[i] > sovChTwo[i - 1]) {
                        index = i;
                    }
                }
                winLinesCherep.push({ table: 6, line: lineIndex + 1, row: index, win: WinCoeffs[6][index] });
            }
            return result;
        }
        const lines: number[][] = new Array();
        for (let j = 0; j < 9; j++) {
            lines.push([]);
        }
        this.WinLines = [];
        let rNum = 0;
        for (let i = 0; i < gameConfig.REELS.COUNT; i++) {
            rNum++;
            for (let j = 0; j < 3; j++) {
                const index = (rNum - 1) * 3 + j;
                if (j === 1) {
                    lines[0].push(aInput[index]);
                }
                if (j === 0) {
                    lines[1].push(aInput[index]);
                }
                if (j === 2) {
                    lines[2].push(aInput[index]);
                }
                if (((j === 0) && (i === 0)) || ((j === 1) && (i === 1)) || ((j === 2) && (i === 2)) || ((j === 1) && (i === 3)) || ((j === 0) && (i === 4))) {
                    lines[3].push(aInput[index]);
                }
                if (((j === 2) && (i === 0)) || ((j === 1) && (i === 1)) || ((j === 0) && (i === 2)) || ((j === 1) && (i === 3)) || ((j === 2) && (i === 4))) {
                    lines[4].push(aInput[index]);
                }
                if (((j === 0) && (i === 0)) || ((j === 0) && (i === 1)) || ((j === 1) && (i === 2)) || ((j === 0) && (i === 3)) || ((j === 0) && (i === 4))) {
                    lines[5].push(aInput[index]);
                }
                if (((j === 2) && (i === 0)) || ((j === 2) && (i === 1)) || ((j === 1) && (i === 2)) || ((j === 2) && (i === 3)) || ((j === 2) && (i === 4))) {
                    lines[6].push(aInput[index]);
                }
                if (((j === 1) && (i === 0)) || ((j === 2) && (i === 1)) || ((j === 2) && (i === 2)) || ((j === 2) && (i === 3)) || ((j === 1) && (i === 4))) {
                    lines[7].push(aInput[index]);
                }
                if (((j === 1) && (i === 0)) || ((j === 0) && (i === 1)) || ((j === 0) && (i === 2)) || ((j === 0) && (i === 3)) || ((j === 1) && (i === 4))) {
                    lines[8].push(aInput[index]);
                }
            }
        }
        // console.log(lines);
        const lineSovp = [];
        for (let j = 0; j < 9; j++) {
            lineSovp.push([]);
        }
        const sovp = [];
        for (let j = 0; j < 9; j++) {
            if (cherep3(j) === false) {
                for (let t = 0; t < 8; t++) {
                    for (let i = 0; i < 5; i++) {
                        for (let k = 0; k < 5; k++) {
                            // check for 3 cherep
                            if ((winLines[t][i][k] === lines[j][k]) || (lines[j][k] === 6) || (winLines[t][i][k] === -1)) {
                                sovp.push({ table: t, line: j + 1, col: k, row: i, value: lines[j][k] });
                            }

                        }
                    }
                }
            }
        }
        const linesTwo = [];
        for (let j = 0; j < 9; j++) {
            linesTwo.push([]);
        }
        sovp.forEach((element) => {
            for (let j = 1; j <= 9; j++) {
                if (element.line === j) {
                    linesTwo[j - 1].push(element);
                }
            }
        });
        let prevcol;
        let newcol;
        let colCount;
        const maxCol = [];
        for (let j = 0; j < 5; j++) {
            maxCol.push([]);
        }
        for (let t = 0; t < 9; t++) {
            for (let i = 0; i < 9; i++) {

                for (let j = 0; j < 5; j++) {
                    newcol = -1;
                    colCount = 1;
                    linesTwo[i].forEach((element) => {
                        if ((element.table === t) && (element.row === j)) {
                            newcol = element.col;
                            if (newcol - prevcol === 1) {
                                colCount++;
                            }
                            prevcol = newcol;
                        }
                    });
                    maxCol[j] = colCount;
                }
                let maxWin = 0;
                let row = -1;
                for (let j = 0; j < 5; j++) {
                    if (maxCol[j] === 5) {
                        if (WinCoeffs[t][j] > maxWin) {
                            maxWin = WinCoeffs[t][j];
                            row = j;
                        }
                    }
                }
                if (maxWin !== 0) {
                    if (this.inRange((i + 1), 1, aLineNumber) === true) {
                        this.WinLines.push({ line: i + 1, table: t, row: row + 1, win: maxWin });
                    }
                }
            }
        }
        for (let i = 0; i < 9; i++) {
            const reduced = [];
            let MaxWin = 0;
            this.WinLines.forEach((element) => {
                if ((element.win > MaxWin) && ((element.line - 1) === i)) {
                    reduced.pop();
                    reduced.push({ line: element.line, table: element.table, row: element.row, win: element.win });
                    MaxWin = element.win;
                }
            });
            lineSovp[i] = reduced;
        }
        const reduced2 = [];
        for (let i = 0; i < 9; i++) {
            lineSovp[i].forEach((element) => {
                reduced2.push({ line: element.line, table: element.table, row: element.row, win: element.win });
            });
        }
        winLinesCherep.forEach((element) => {
            if (this.inRange((element.line + 1), 1, aLineNumber) === true) {
                reduced2.push({ line: element.line, table: element.table, row: element.row, win: element.win });
            }
        });
        return reduced2;
    }

    public getName(i) {
        switch (i) {
            case 0: {
                return "9";
            }
            case 1: {
                return "10";
            }
            case 2: {
                return "J";
            }
            case 3: {
                return "Q";
            }
            case 4: {
                return "K";
            }
            case 5: {
                return "A";
            }
            case 6: {
                return "Ёжик";
            }
            case 7: {
                return "Барсик";
            }
            case 8: {
                return "Заяц";
            }
            case 9: {
                return "Сова";
            }
            case 10: {
                return "Песец";
            }
            case 11: {
                return "Лиса";
            }
            case 12: {
                return "Лес";
            }
            default: {
                return null;
            }
        }
    }

    public checkScatters(aInput: number[][]) {
        let scatterSum = 0;
        for (let i1 = 0; i1 < 3; i1++) {
            for (let j1 = 0; j1 < 5; j1++) {
                if (aInput[i1][j1] === 12) {
                    scatterSum++;
                }
            }
        }
        return scatterSum;
    }

    public getUpdateWinLines(wins) {
        function inLine(line) {
            return function (x) {
                return x.line === line;
            };
        }
        const updatedWinLines = [];
        for (let i = 0; i < 12; i++) {
            const filtered = wins.filter(inLine(i));
            if (filtered[0] !== undefined) {
                const maxLine = { f: filtered[0], maxVal: filtered[0].win };
                if (maxLine.f !== undefined) {
                    for (const iterator of filtered) {
                        if (iterator.win > maxLine.maxVal) {
                            maxLine.f = iterator;
                            maxLine.maxVal = iterator.win;
                        }
                    }
                    if (maxLine.f !== undefined) {
                        updatedWinLines.push(maxLine.f);
                    }
                }
            }
        }
        return updatedWinLines;
    }

    public calculate2(aLineNumber: number, aInput: number[][]) {

        function countRepeatInAll(key, line) {
            let sum1 = 0;
            let sum2 = 0;
            if (key !== 12) {
                for (let i = 0; i < line.length; i++) {
                    if ((line[i] === key) || (line[i] === 11)) {
                        if (line[i - 1] !== undefined) {
                            if ((line[i - 1] === key) || (line[i - 1] === 11)) {
                                sum1++;
                                // console.log("+1");
                            } else {
                                // console.log("+1");
                                sum2 = sum1;
                                sum1 = 0;
                            }
                        }
                    }
                }
                if (sum2 > sum1) {
                    if (sum2 !== 0) {
                        return (sum2 + 1);
                    } else {
                        // console.log("нет повторяющихся");
                        return 0;
                    }
                } else {
                    if (sum1 !== 0) {
                        return (sum1 + 1);
                    } else {
                        // console.log("нет повторяющихся");
                        return 0;
                    }
                }
            } else {
                sum1 = 0;
                // tslint:disable-next-line:prefer-for-of
                for (let i1 = 0; i1 < line.length; i1++) {
                    if (line[i1] === 12) {
                        sum1++;
                    }
                }
                return sum1;
            }
        }
        function countRepeat(key, line) {
            let sum = 0;
            for (let i = 0; i < line.length; i++) {
                if ((line[i] === key) || (line[i] === 11)) {
                    if (line[i - 1] !== undefined) {
                        if ((line[i - 1] === key) || (line[i - 1] === 11)) {
                            sum++;
                        } else {
                            return sum;
                        }
                    }
                }
            }
            return sum;
        }
        // Первая линия
        const lines = [];
        let temp = [];
        for (let i = 0; i < 5; i++) {
            temp.push(aInput[1][i]);
        }
        lines.push(temp);
        // Вторая линия
        temp = [];
        for (let i = 0; i < 5; i++) {
            temp.push(aInput[0][i]);
        }
        lines.push(temp);
        // Третья линия
        temp = [];
        for (let i = 0; i < 5; i++) {
            temp.push(aInput[2][i]);
        }
        lines.push(temp);
        // Четвертая линия
        temp = [];
        temp.push(aInput[0][0]);
        temp.push(aInput[1][1]);
        temp.push(aInput[2][2]);
        temp.push(aInput[1][3]);
        temp.push(aInput[0][4]);
        lines.push(temp);
        // Пятая линия
        temp = [];
        temp.push(aInput[2][0]);
        temp.push(aInput[1][1]);
        temp.push(aInput[0][2]);
        temp.push(aInput[1][3]);
        temp.push(aInput[2][4]);
        lines.push(temp);
        // Шестая линия
        temp = [];
        temp.push(aInput[1][0]);
        temp.push(aInput[2][1]);
        temp.push(aInput[2][2]);
        temp.push(aInput[2][3]);
        temp.push(aInput[1][4]);
        lines.push(temp);
        // Седьмая линия
        temp = [];
        temp.push(aInput[1][0]);
        temp.push(aInput[0][1]);
        temp.push(aInput[0][2]);
        temp.push(aInput[0][3]);
        temp.push(aInput[1][4]);
        lines.push(temp);
        // Восьмая линия
        temp = [];
        temp.push(aInput[2][0]);
        temp.push(aInput[2][1]);
        temp.push(aInput[1][2]);
        temp.push(aInput[0][3]);
        temp.push(aInput[0][4]);
        lines.push(temp);
        // Девятая линия
        temp = [];
        temp.push(aInput[0][0]);
        temp.push(aInput[0][1]);
        temp.push(aInput[1][2]);
        temp.push(aInput[2][3]);
        temp.push(aInput[2][4]);
        lines.push(temp);

        let j = 0;
        lines.forEach((element) => {
            // console.log(this.getName(element));
            let s = "";
            element.forEach((el) => {
                s = s + this.getName(el) + " ; ";
                // console.log(this.getName(el));
            });
            j++;
            // console.log("Линия " + (j) + ":  " + s);
        });

        const tils = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        j = 0;

        this.WinLines = [];

        lines.forEach((element) => {
            tils.forEach((til) => {
                const count = countRepeat(til, element);
                if ((count > 0)) {
                    // console.log("Линия " + (j + 1) + ", Элемент - " + this.getName(til) + " встречается " + (count + 1) + " раза.");
                    if ((til === 0) || (til === 9) || (til === 10) || (til === 11)) {
                        let winFactor = 1;
                        if (til !== 11) {
                            element.forEach((el) => {
                                if (el === 11) {
                                    winFactor = 2;
                                }
                            });
                        }
                        this.WinLines.push({ line: (j + 1), til, count: (count + 1), win: WinCoeffs[til][count - 1] * winFactor });
                    } else {
                        if (count > 1) {
                            let winFactor = 1;
                            element.forEach((el) => {
                                if (el === 11) {
                                    winFactor = 2;
                                }
                            });
                            this.WinLines.push({ line: (j + 1), til, count: (count + 1), win: WinCoeffs[til][count - 1] * winFactor });
                        }
                    }
                }
            });
            j++;
        });
        // console.log(this.WinLines);
        // this.WinLines.forEach((element) => {
        //     console.log("Линия " + (element.line) + ", Элемент - " + this.getName(element.til) + " встречается " + element.count + " раза.");
        // });
        this.WinLines = this.getUpdateWinLines(this.WinLines);

        let scatterSum = 0;
        for (let i1 = 0; i1 < 3; i1++) {
            for (let j1 = 0; j1 < 5; j1++) {
                if (aInput[i1][j1] === 12) {
                    scatterSum++;
                }
            }
        }
        if (scatterSum >= 2) {
            this.WinLines.push({ line: 0, til: 12, count: (scatterSum), win: 0 });
        }
        // if (scatterSum >= 2) {
        //     console.log("Обнаружено скаттеров: " + scatterSum);
        // }

        // Проверяем, сколько одинаковых, и идут ли они подряд
        // const arr = ["aa", "aa", "aa", "ab", "ab", "ac", "ac", "a", "aa", "ac", "ac", "s"];
        // const arr = ["0", "1", "1", "0", "0"];
        // const arr2 = [];
        // arr.forEach((element) => {
        //     if (arr2[element] !== undefined) {
        //         (arr2[element]++);
        //     } else {
        //         (arr2[element] = 1);
        //     }
        // });
        // console.log(countRepeat("0", firstLine));
        // tslint:disable-next-line:no-string-literal
        // console.log(arr2["s"]); // число повторений для каждого элемента массива
        // let sum1 = 0;
        // let sum2 = 0;
        // for (let i = 0; i < arr.length; i++) {
        //     if (arr[i] === "0") {
        //         if (arr[i - 1] !== undefined) {
        //             if (arr[i - 1] === "0") {
        //                 sum1++;
        //                 console.log("+1");
        //             }
        //         }
        //     } else {
        //         console.log("ZERO");
        //         sum2 = sum1;
        //         sum1 = 0;
        //     }
        // }

        // console.log(sum1, sum2);

        // if (sum2 > sum1) {
        //     console.log("sum2 =", sum2 + 1); // число повторений для "aa"
        // } else {
        //     console.log("sum1 =", sum1 + 1); // число повторений для "aa"
        // }

        return this.WinLines;
    }
}
