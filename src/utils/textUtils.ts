﻿function rightJustify(str, length, char) {
    const fill = [];
    while (fill.length + str.length < length) {
        fill[fill.length] = char;
    }
    return fill.join("") + str;
}
function leftJustify(str, length, char) {
    const fill = [];
    while (fill.length + str.length < length) {
        fill[fill.length] = char;
    }
    return str + fill.join("");
}
function centerJustify(length, char) {
    let i = 0;
    let str = this;
    let toggle = true;
    while (i + this.length < length) {
        i++;
        if (toggle) {
            str = str + char;
        } else {
            str = char + str;
        }
        toggle = !toggle;
    }
    return str;
}
export { rightJustify, leftJustify, centerJustify };
