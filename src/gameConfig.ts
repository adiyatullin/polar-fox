const GAME_SIZE = {
    WIDTH: 1280,
    HEIGHT: 720
};

const SCREEN_SIZE = {
    WIDTH: screen.width * devicePixelRatio,
    HEIGHT: screen.height * devicePixelRatio
};

if (SCREEN_SIZE.HEIGHT > SCREEN_SIZE.WIDTH) {
    const h = SCREEN_SIZE.HEIGHT;
    SCREEN_SIZE.HEIGHT = SCREEN_SIZE.WIDTH;
    SCREEN_SIZE.WIDTH = h;
}

const x = SCREEN_SIZE.WIDTH / GAME_SIZE.WIDTH;
const y = SCREEN_SIZE.HEIGHT / GAME_SIZE.HEIGHT;

const SCALE = Math.min(x, y);

SCREEN_SIZE.WIDTH = GAME_SIZE.WIDTH * SCALE;
SCREEN_SIZE.HEIGHT = GAME_SIZE.HEIGHT * SCALE;

export enum ScreenMode {
    Landscape,
    Portrait
}

export enum GameScene {
    None,
    Main,
    Free,
    Help,
    Risk,
    Bonus,
    Jackpot,
    Settings,
    MoreSettings
}

export enum BonusWin {
    Anvil,
    Сoconut,
    Brick
}


export const WinCoeffs = {
    // Тил: [Множители для 2, 3, 4, 5 повторений]
    // 0
    0: [2, 5, 25, 100],
    // 10
    1: [1, 5, 25, 100],
    // J
    2: [1, 5, 25, 100],
    // Q
    3: [1, 5, 25, 100],
    // K
    4: [1, 10, 50, 125],
    // A
    5: [1, 10, 50, 125],
    // Ежик
    6: [1, 15, 75, 250],
    // Барсик
    7: [1, 15, 75, 250],
    // Заяц
    8: [1, 20, 100, 400],
    // Сова
    9: [2, 25, 125, 750],
    // Песец
    10: [2, 25, 125, 750],
    // Лиса
    11: [10, 250, 2500, 9000],
    // Лес
    12: [2, 5, 20, 500]
};

export const superBonusWin = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500];

export const winLines = [
    [
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, -1],
        [-1, 0, 0, 0, 0],
        [0, 0, 0, -1, -1],
        [-1, -1, 0, 0, 0]
    ],
    [
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, -1],
        [-1, 1, 1, 1, 1],
        [1, 1, 1, -1, -1],
        [-1, -1, 1, 1, 1]
    ],
    [
        [2, 2, 2, 2, 2],
        [2, 2, 2, 2, -1],
        [-1, 2, 2, 2, 2],
        [2, 2, 2, -1, -1],
        [-1, -1, 2, 2, 2]
    ],
    [
        [3, 3, 3, 3, 3],
        [3, 3, 3, 3, -1],
        [-1, 3, 3, 3, 3],
        [3, 3, 3, -1, -1],
        [-1, -1, 3, 3, 3]
    ],
    [
        [4, 4, 4, 4, 4],
        [4, 4, 4, 4, -1],
        [-1, 4, 4, 4, 4],
        [4, 4, 4, -1, -1],
        [-1, -1, 4, 4, 4]
    ],
    [
        [5, 5, 5, 5, 5],
        [5, 5, 5, 5, -1],
        [-1, 5, 5, 5, 5],
        [5, 5, 5, -1, -1],
        [-1, -1, 5, 5, 5]
    ],
    [
        [6, 6, 6, 6, 6],
        [6, 6, 6, 6, -1],
        [-1, 6, 6, 6, 6],
        [6, 6, 6, -1, -1],
        [-1, -1, 6, 6, 6]
    ],
    [
        [7, 7, 7, 7, 7],
        [7, 7, 7, 7, -1],
        [-1, 7, 7, 7, 7],
        [7, 7, 7, -1, -1],
        [-1, -1, 7, 7, 7]
    ]
];



const GameConfig = {

    CANVAS_SIZE: SCREEN_SIZE,

    TYPES: {
        BUTTON: "button",
        LINE: "Line",
        PANEL: "Panel",
        EDIT: "Edt"
    },

    BONUS_DELAY: 6333,
    BONUS_KASKA_BET: 10,

    PATHS: {
        BUTTONS: "assets/buttons/",
        FONTS: "assets/fonts/bitmap/",
        HELP: "assets/img/Help/",
        SOUNDS: "assets/sounds/",
        DOUBLE: "assets/img/Double/"
    },
    getPath() {
        return this.PATHS;
    },
    GAME_MODES: {
        MONRO: {
            NAME: "MONRO",
            SCALE: 1.0,
            BUTTONS: {

            }
        }
        ,
        VEKSEL: {
            NAME: "VEKSEL",
            SCALE: 0.8,
            BUTTONS: {
                BUTTONS_BACK: {
                    X: 0,
                    Y: 0,
                    NAME: "buttons_back",
                    PATH: "assets/buttons/Veksel/"
                }
            }
        }
    },
    CURRENT_GAME_MODE: this.GAME_MODES,
    SCALE,
    BET_INDEX: 1,
    LINE_INDEX: 4,
    BETS: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 50, 100, 200],
    LINES: [1, 3, 5, 8, 10],

    FONTS: {
        MICRO: {
            NAME: "microstyle"
        },
        LEXIA: {
            NAME: "lexia"
        },
        FLIP: {
            NAME: "flipflop"
        },
        MEGA: {
            NAME: "mega"
        },
        AZURE: {
            NAME: "azure"
        },
        UBUNTU: {
            NAME: "ubuntu"
        },
        SUPERBONUS: {
            NAME: "flipflop3"
        },
        ROBOTOBOLD: {
            NAME: "roboto-bold"
        },
        ROBOTOTHIN: {
            NAME: "roboto-thin"
        }
    },
    SYMBOLS: {
        SIZE: 160,
        COUNT: 9
    },
    // SYMBOLS: {
    //     NAME_PREFIX: "symbols_",
    //     COUNT: 9,
    //     SIZE: 159,
    //     PATH: "assets/img/Tils/"
    // },
    BUTTONS: {
        MENU: {
            NAME: "ButMenu",
            LANDSCAPE: {
                X: 35 * SCALE,
                Y: 35 * SCALE,
                Z: 1.0 * SCALE
            },
            PORTRAIT: {
                X: 15 * SCALE,
                Y: 15 * SCALE,
                Z: 1.0 * SCALE
            }
        },
        SPIN: {
            NAME: "ButSpin",
            LANDSCAPE: {
                X: 1010 * SCALE,
                Y: SCREEN_SIZE.HEIGHT / 2,
                Z: 1.0 * SCALE
            },
            PORTRAIT: {
                X: (SCREEN_SIZE.HEIGHT / 2) * SCALE - (293 / 2) * SCALE,
                Y: SCREEN_SIZE.HEIGHT / 2 + 50 * SCALE,
                Z: 1.0 * SCALE
            }
        },
        X2: {
            NAME: "ButDouble",
            LANDSCAPE: {
                X: 1080 * SCALE,
                Y: SCREEN_SIZE.HEIGHT / 2 - 240 * SCALE,
                Z: 1.0 * SCALE
            },
            PORTRAIT: {
                X: (SCREEN_SIZE.HEIGHT / 2) * SCALE - (293 / 2) * SCALE,
                Y: SCREEN_SIZE.HEIGHT / 2 - 50 * SCALE,
                Z: 1.0 * SCALE
            }
        },
        SETTINGS: {
            NAME: "ButSettings",
            LANDSCAPE: {
                X: 25 * SCALE,
                Y: 290 * SCALE,
                Z: 1.0 * SCALE
            },
            PORTRAIT: {
                X: SCREEN_SIZE.HEIGHT / 2 - 260 * SCALE,
                Y: SCREEN_SIZE.HEIGHT / 2 + 100 * SCALE,
                Z: 1.0 * SCALE
            }
        },
        MORE_SETTINGS: {
            NAME: "ButMoreSettings",
            LANDSCAPE: {
                X: 0,
                Y: SCREEN_SIZE.HEIGHT / 2 - (219 * SCALE) / 2,
                Z: 1.0 * SCALE
            },
            PORTRAIT: {
                X: 0,
                Y: SCREEN_SIZE.HEIGHT / 2 - (219 * SCALE) / 2,
                Z: 1.0 * SCALE
            }
        },
        AUTO: {
            NAME: "ButAuto",
            LANDSCAPE: {
                X: 1080 * SCALE,
                Y: SCREEN_SIZE.HEIGHT / 2 + 120 * SCALE,
                Z: 1.0 * SCALE
            },
            PORTRAIT: {
                X: SCREEN_SIZE.HEIGHT / 2 + 620 * SCALE,
                Y: SCREEN_SIZE.HEIGHT / 2 + 100 * SCALE,
                Z: 1.0 * SCALE
            }
        }
    },
    REELS: {
        COUNT: 5,
        WIDTH: 197,
        CONTAINER: {
            // X_OFFSET: 10 * SCALE,
            // Y_OFFSET: 80 * SCALE,
            GAP_X: 1 * SCALE,
            // GAP_Y: 1 * SCALE
            GAP_Y: 10 * SCALE
        }
    },
    PRELOADER: {
        BACKGROUND_PRELOADER_PORTRAIT: {
            X: SCREEN_SIZE.WIDTH / 2,
            Y: SCREEN_SIZE.HEIGHT / 2,
            NAME: "background_portrait",
            PATH: "assets/img/back/FoxPortrait.jpg"
        },
        BACKGROUND_PRELOADER_LANDSCAPE: {
            X: SCREEN_SIZE.WIDTH / 2,
            Y: SCREEN_SIZE.HEIGHT / 2,
            NAME: "background_landscape",
            PATH: "assets/img/back/scr_preload.jpg"
        },
        LOADING_TEXT: {
            X: 310 * SCALE,
            Y: 630 * SCALE
        },
        PROGRESS_BAR: {
            X: SCREEN_SIZE.WIDTH / 2 - (388 / 2) * SCALE,
            Y: SCREEN_SIZE.HEIGHT / 2 + 273 * SCALE,
            WIDTH: 388 * SCALE,
            HEIGHT: 15 * SCALE
        }
    },

    IMAGES: {
        MAIN_IMAGES: {
            TITLE: {
                NAME: "title",
                PATH: "back/"
            },
            BACKGROUND_MAIN: {
                NAME: "back",
                PATH: "back/"
            },
            BACKGROUND_TOP: {
                NAME: "top",
                PATH: "back/"
            },
            BACKGROUND_BOTTOM: {
                NAME: "bot",
                PATH: "back/"
            },
            BACKGROUND_BONUS: {
                NAME: "BackBonus",
                PATH: "back/"
            },
            TAKE_OR_RISK: {
                NAME: "takeorrisk",
                PATH: "back/"
            },
            FIND_A_PRIZE: {
                NAME: "SupBonusTitle",
                PATH: "back/"
            },
            SUP_TAB: {
                NAME: "SupBonusTab",
                PATH: "back/"
            },
            TILE_BACK: {
                NAME: "tile",
                PATH: "back/"
            }
        },
        DIGIT_IMAGES: {
            PATH: "digits"
        },
        DOUBLE_IMAGES: {
            BTN_RED: {
                X_OFFSET: 0,
                Y_OFFSET: -100,
                NAME: "DoubleBtnRed",
                PATH: "Double/"
            },
            BTN_BLACK: {
                X_OFFSET: 0,
                Y_OFFSET: -100,
                NAME: "DoubleBtnBlack",
                PATH: "Double/"
            },
            CENTER_CARD: {
                X_OFFSET: 0,
                Y_OFFSET: -100,
                NAME: "DoubleCenterCard",
                PATH: "Double/"
            },
            BACKGROUND: {
                X_OFFSET: 0,
                Y_OFFSET: -100,
                NAME: "DoubleBack",
                PATH: "Double/"
            },
            CARD_BACK: {
                X_OFFSET: 0,
                Y_OFFSET: 160,
                NAME: "DoubleCardBack",
                PATH: "Double/"
            },
            PIKA: {
                X_OFFSET: -272,
                Y_OFFSET: -110,
                NAME: "DoubleCardPika",
                PATH: "Double/"
            },
            CHIRVA: {
                X_OFFSET: -272,
                Y_OFFSET: -110,
                NAME: "DoubleCardChirva",
                PATH: "Double/"
            },
            KREST: {
                X_OFFSET: -272,
                Y_OFFSET: -110,
                NAME: "DoubleCardKrest",
                PATH: "Double/"
            },
            BUBA: {
                X_OFFSET: -272,
                Y_OFFSET: -110,
                NAME: "DoubleCardBuba",
                PATH: "Double/"
            }
        },
        DOUBLE_CARDS: {
            BUBAS: {
                NAME_PREFIX: "DoubleBuba",
                COUNT: 13,
                PATH: "Double/DoubleBuba/",
                NAMES: ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"],
                SCALE_X: 1.0,
                SCALE_Y: 1.0,
                INDEX: 0
            },
            CHIRVAS: {
                NAME_PREFIX: "DoubleChirva",
                COUNT: 13,
                PATH: "Double/DoubleChirva/",
                NAMES: ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"],
                SCALE_X: 1.0,
                SCALE_Y: 1.0,
                INDEX: 1
            },
            KRESTS: {
                NAME_PREFIX: "DoubleKrest",
                COUNT: 13,
                PATH: "Double/DoubleKrest/",
                NAMES: ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"],
                SCALE_X: 1.0,
                SCALE_Y: 1.0,
                INDEX: 2
            },
            PIKAS: {
                NAME_PREFIX: "DoublePika",
                COUNT: 13,
                PATH: "Double/DoublePika/",
                NAMES: ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"],
                SCALE_X: 1.0,
                SCALE_Y: 1.0,
                INDEX: 3
            }
        },
        PANEL_IMAGES: {
            TOTAL_BET: {
                X: 0,
                Y: 0,
                NAME: "PanelBet",
                PATH: "Panel/"
            },
            INPUT_WIN: {
                X: 0,
                Y: 0,
                NAME: "PanelInput",
                PATH: "Panel/"
            },
            CREDIT: {
                X: SCREEN_SIZE.WIDTH - (300 * SCALE),
                Y: 0,
                NAME: "PanelCredit",
                PATH: "Panel/"
            },
            LINE_PLAY: {
                X: 0,
                Y: SCREEN_SIZE.HEIGHT - (72 * SCALE),
                NAME: "PanelLine",
                PATH: "Panel/"
            },
            TOTAL_WIN: {
                X: 0,
                Y: SCREEN_SIZE.HEIGHT - (72 * SCALE),
                NAME: "PanelWin",
                PATH: "Panel/"
            },
            PRIZE_WIN: {
                X: 0,
                Y: SCREEN_SIZE.HEIGHT - (72 * SCALE),
                NAME: "PanelPrize",
                PATH: "Panel/"
            },
            BET_PER_LINE_LEFT: {
                X: 225 * SCALE,
                Y: 95 * SCALE,
                NAME: "BetTableText",
                PATH: ""
            },
            BET_PER_LINE_RIGHT: {
                X: SCREEN_SIZE.WIDTH - (360 * SCALE),
                Y: 95 * SCALE,
                NAME: "BetTableText",
                PATH: ""
            },
            BET_TABLE_LEFT: {
                X: 166 * SCALE,
                Y: 90 * SCALE,
                NAME: "BetTable",
                PATH: ""
            },
            BET_TABLE_RIGHT: {
                X: SCREEN_SIZE.WIDTH - (220 * SCALE),
                Y: 90 * SCALE,
                NAME: "BetTable",
                PATH: ""
            }
        },
        LINE_IMAGES: {
            INDICATORS: {
                NAME_PREFIX: "ind",
                COUNT: 9,
                PATH: "assets/img/Line/",
                X: [134, 130, 130, 130, 130, 130, 130, 130, 130],
                Y: [317, 112, 522,
                    204, 430, 158,
                    476, 363, 271
                ],
                SCALE_X: 0.8,
                SCALE_Y: 0.8
            },
            SOLID_LINES: {
                NAME_PREFIX: "Line",
                COUNT: 9,
                PATH: "assets/img/Line/",
                X: [175, 175, 175, 175, 175, 175, 175, 175, 175],
                Y: [334, 125, 541,
                    217, 131, 173,
                    345, 384, 185
                ],
                SCALE_X: 1.66,
                SCALE_Y: 1.55
            },
            DASHED_LINES: {
                NAME_PREFIX: "Lines",
                COUNT: 9,
                PATH: "assets/img/Line/",
                X: [175, 175, 175, 175, 175, 175, 175, 175, 175],
                Y: [334, 125, 541,
                    217, 153, 173,
                    432, 384, 185
                ],
                SCALE_X: 1.66,
                SCALE_Y: 1.55
            }
        },
        BONUS_IMAGES: {
            BANANA: {
                X: 0,
                Y: 0,
                NAME: "BonusBanan",
                PATH: "img/"
            },
            ANVIL: {
                X: 100,
                Y: 0,
                NAME: "BonusAnvil",
                PATH: "img/"
            },
            COCONUT: {
                X: 200,
                Y: 0,
                NAME: "BonusCoconut",
                PATH: "img/"
            },
            BRICK: {
                X: 300,
                Y: 0,
                NAME: "BonusBrick",
                PATH: "img/"
            }
        },
        MORE_SETTINGS_IMAGES: {
            BACKGROUND: {
                NAME: "MoreBack",
                PATH: "back/",
                X: 0,
                Y: 0,
                SCALE: 1
            },
            RETURN: {
                NAME: "MoreRet",
                PATH: "more/",
                X: 185,
                Y: -12,
                SCALE: 1.2
            },
            LINES: {
                NAME: "MoreLines",
                PATH: "more/",
                X: -50,
                Y: -280,
                SCALE: 1.0
            },
            LINES_BACK: {
                NAME: "MoreLinesBack",
                PATH: "more/",
                X: 25,
                Y: -200,
                SCALE: 0.85
            },
            LINES_PLUS: {
                NAME: "MoreLinesPlus",
                PATH: "more/",
                X: 120,
                Y: -205,
                SCALE: 0.6
            },
            LINES_MINUS: {
                NAME: "MoreLinesMinus",
                PATH: "more/",
                X: -70,
                Y: -205,
                SCALE: 0.6
            },
            BET: {
                NAME: "MoreBet",
                PATH: "more/",
                X: 50,
                Y: 100,
                SCALE: 1.0
            },
            BET_BACK: {
                NAME: "MoreBetBack",
                PATH: "more/",
                X: 25,
                Y: 225,
                SCALE: 0.85
            },
            BET_PLUS: {
                NAME: "MoreBetPlus",
                PATH: "more/",
                X: 120,
                Y: 220,
                SCALE: 0.6
            },
            BET_MINUS: {
                NAME: "MoreBetMinus",
                PATH: "more/",
                X: -70,
                Y: 220,
                SCALE: 0.6
            },
            MAX_BET: {
                NAME: "MoreMaxBet",
                PATH: "more/",
                X: 30,
                Y: 20,
                SCALE: 1.3
            }
            ,
            MAX_BET_DOWN: {
                NAME: "MoreMaxBetDown",
                PATH: "more/",
                X: -115,
                Y: 450,
                SCALE: 0.8
            }
            // ,
            // AUTO: {
            //     NAME: "MoreAuto",
            //     PATH: "more/",
            //     X: -22,
            //     Y: 300,
            //     SCALE: 0.8
            // }
        },
        SETTINGS_IMAGES: {
            BACKGROUND: {
                NAME: "SettingsBack",
                PATH: "back/",
                X: 0,
                Y: 0,
                SCALE: 0.94
            }
        },
        JACKPOT_IMAGES: {
            BACKGROUND_SUM: {
                NAME: "JackpotBackSum",
                PATH: "back/",
                X: SCREEN_SIZE.WIDTH / 2,
                Y: SCREEN_SIZE.HEIGHT / 2,
                SCALE: 0.94
            }
        }
    },
    ANIMATIONS: {
        MAIN_ANIM: {
            X: 245 * SCALE,
            Y: -236 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 25,
            NAME: "monkey_main_idle",
            PATH: "AnimMain/AnimMain",
            FRAME_RATE: 8,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: true
        },
        MAIN_ANIM_KASKA: {
            X: 245 * SCALE,
            Y: -236 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 25,
            NAME: "monkey_main_idle_kaska",
            PATH: "KaskaAnimMain/KaskaAnimMain",
            FRAME_RATE: 8,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: false
        },
        WIN_ANIM: {
            X: 245 * SCALE,
            Y: -236 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 2,
            NAME: "win_anim",
            PATH: "AnimWin/AnimWin",
            FRAME_RATE: 4,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: true
        },
        WIN_ANIM_KASKA: {
            X: 245 * SCALE,
            Y: -236 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 2,
            NAME: "win_anim_kaska",
            PATH: "KaskaAnimWin/KaskaAnimWin",
            FRAME_RATE: 4,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: false
        },
        SPIN_ANIM: {
            X: 245 * SCALE,
            Y: -236 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 5,
            NAME: "spin_anim",
            PATH: "AnimSpin/AnimSpin",
            FRAME_RATE: 10,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: true
        },
        SPIN_ANIM_KASKA: {
            X: 245 * SCALE,
            Y: -236 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 5,
            NAME: "spin_anim_kaska",
            PATH: "KaskaAnimSpin/KaskaAnimSpin",
            FRAME_RATE: 10,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: false
        },
        DOUBLE_ANIM: {
            X: 140 * SCALE,
            Y: -286 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 6,
            NAME: "double_anim",
            PATH: "Double/AnimDouble/AnimDouble",
            FRAME_RATE: 6,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: false
        },
        DOUBLE_ANIM_WIN: {
            X: 140 * SCALE,
            Y: -286 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 3,
            NAME: "double_anim_win",
            PATH: "Double/AnimDoubleWin/AnimDoubleWin",
            FRAME_RATE: 6,
            REPEAT: 0,
            DELAY: 0,
            HAS_KASKA: false
        },
        DOUBLE_ANIM_LOSE: {
            X: 140 * SCALE,
            Y: -286 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 3,
            NAME: "double_anim_lose",
            PATH: "Double/AnimDoubleLose/AnimDoubleLose",
            FRAME_RATE: 6,
            REPEAT: 0,
            DELAY: 0,
            HAS_KASKA: false
        },
        DOUBLE_ANIM_FORWARD: {
            X: 135 * SCALE,
            Y: -286 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 10,
            NAME: "double_anim_forward",
            PATH: "Double/AnimDoubleForward/AnimDoubleForward",
            FRAME_RATE: 6,
            REPEAT: 0,
            DELAY: 0,
            HAS_KASKA: false
        },
        DOUBLE_TALK: {
            X: 300 * SCALE,
            Y: -286 * SCALE + 124 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 2,
            NAME: "double_talk",
            PATH: "Double/DoubleTalk/DoubleTalk",
            FRAME_RATE: 4,
            REPEAT: 0,
            DELAY: 0,
            HAS_KASKA: false
        },
        BONUS_ANIM: {
            X: 245 * SCALE,
            Y: -236 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 4,
            NAME: "bonus_anim",
            PATH: "Bonus/AnimBonus/AnimBonus",
            FRAME_RATE: 4,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: true
        },
        BONUS_ANIM_KASKA: {
            X: 245 * SCALE,
            Y: -236 * SCALE + 144 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 4,
            NAME: "bonus_anim_kaska",
            PATH: "Bonus/KaskaAnimBonus/KaskaAnimBonus",
            FRAME_RATE: 4,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: false
        },
        BONUS_WAIT: {
            X: 140 * SCALE,
            Y: 290 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 12,
            NAME: "bonus_wait",
            PATH: "Bonus/BonusWait/BonusWait",
            FRAME_RATE: 3,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: true
        },
        BONUS_WAIT_KASKA: {
            X: 140 * SCALE,
            Y: 290 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 12,
            NAME: "bonus_wait_kaska",
            PATH: "Bonus/BonusWaitKaska/BonusWaitKaska",
            FRAME_RATE: 3,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: false
        },
        BONUS_KANAT1: {
            X: 376 * SCALE,
            Y: -683 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 11,
            NAME: "bonus_ropes1",
            PATH: "Bonus/Ropes/Ropes",
            FRAME_RATE: 3,
            REPEAT: -1,
            DELAY: 300,
            REPEAT_DELAY: 100,
            HAS_KASKA: false
        },
        BONUS_KANAT2: {
            X: 496 * SCALE,
            Y: -683 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 11,
            NAME: "bonus_ropes2",
            PATH: "Bonus/Ropes/Ropes",
            FRAME_RATE: 3,
            REPEAT: -1,
            DELAY: 150,
            REPEAT_DELAY: 300,
            HAS_KASKA: false
        },
        BONUS_KANAT3: {
            X: 617 * SCALE,
            Y: -683 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 11,
            NAME: "bonus_ropes3",
            PATH: "Bonus/Ropes/Ropes",
            FRAME_RATE: 3,
            REPEAT: -1,
            DELAY: 200,
            REPEAT_DELAY: 500,
            HAS_KASKA: false
        },
        BONUS_KANAT4: {
            X: 738 * SCALE,
            Y: -683 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 11,
            NAME: "bonus_ropes4",
            PATH: "Bonus/Ropes/Ropes",
            FRAME_RATE: 3,
            REPEAT: -1,
            DELAY: 20,
            REPEAT_DELAY: 180,
            HAS_KASKA: false
        },
        BONUS_KANAT5: {
            X: 860 * SCALE,
            Y: -683 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 11,
            NAME: "bonus_ropes5",
            PATH: "Bonus/Ropes/Ropes",
            FRAME_RATE: 3,
            REPEAT: -1,
            DELAY: 260,
            REPEAT_DELAY: 200,
            HAS_KASKA: false
        },
        BONUS_GO: {
            X: 140 * SCALE,
            Y: -717 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 13,
            NAME: "bonus_go",
            PATH: "Bonus/BonusGo/BonusGo",
            FRAME_RATE: 8,
            REPEAT: 0,
            DELAY: 0,
            HIDE_ON_COMPLETE: true,
            HAS_KASKA: true
        },
        BONUS_GO_KASKA: {
            X: 140 * SCALE,
            Y: -717 * SCALE, // 236 - размер по Y - анимации, 144 - размер по Y - нижней плашки background_bottom
            FRAMES_COUNT: 13,
            NAME: "bonus_go_kaska",
            PATH: "Bonus/BonusGoKaska/BonusGoKaska",
            FRAME_RATE: 8,
            REPEAT: 0,
            DELAY: 0,
            HIDE_ON_COMPLETE: true,
            HAS_KASKA: false
        },
        BONUS_BANAN: {
            X: 0,
            Y: 0,
            FRAMES_COUNT: 9,
            NAME: "bonus_banan",
            PATH: "Bonus/BonusBanan/BonusBanan",
            FRAME_RATE: 18,
            REPEAT: 0,
            DELAY: 0,
            HIDE_ON_COMPLETE: true,
            HAS_KASKA: true
        },
        BONUS_BANAN_KASKA: {
            X: 0,
            Y: 0,
            FRAMES_COUNT: 9,
            NAME: "bonus_banan_kaska",
            PATH: "Bonus/BonusBananKaska/BonusBananKaska",
            FRAME_RATE: 18,
            REPEAT: 0,
            DELAY: 0,
            HIDE_ON_COMPLETE: true,
            HAS_KASKA: false
        },
        BONUS_BOOM: {
            X: 0,
            Y: 0,
            FRAMES_COUNT: 6,
            NAME: "bonus_boom",
            PATH: "Bonus/BonusBoom/BonusBoom",
            FRAME_RATE: 12,
            REPEAT: 0,
            DELAY: 0,
            HIDE_ON_COMPLETE: true,
            HAS_KASKA: true
        },
        BONUS_BOOM_KASKA: {
            X: 0,
            Y: 0,
            FRAMES_COUNT: 6,
            NAME: "bonus_boom_kaska",
            PATH: "Bonus/BonusBoomKaska/BonusBoomKaska",
            FRAME_RATE: 12,
            REPEAT: 0,
            DELAY: 0,
            HIDE_ON_COMPLETE: true,
            HAS_KASKA: false
        },
        BONUS_LOSE: {
            X: 0,
            Y: 0,
            FRAMES_COUNT: 5,
            NAME: "bonus_lose",
            PATH: "Bonus/BonusLose/BonusLose",
            FRAME_RATE: 10,
            REPEAT: -1,
            DELAY: 0,
            HIDE_ON_COMPLETE: false,
            HAS_KASKA: false
        },
        SUPER_WAIT: {
            X: SCREEN_SIZE.WIDTH / 2 - 125 * SCALE,
            Y: -266 * SCALE,
            FRAMES_COUNT: 2,
            NAME: "super_wait",
            PATH: "SupBonus/SupBonusWait/SupBonusWait",
            FRAME_RATE: 4,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: true
        },
        SUPER_WAIT_KASKA: {
            X: SCREEN_SIZE.WIDTH / 2 - 125 * SCALE,
            Y: -266 * SCALE,
            FRAMES_COUNT: 2,
            NAME: "super_wait_kaska",
            PATH: "SupBonus/SupBonusWaitKaska/SupBonusWaitKaska",
            FRAME_RATE: 4,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: false
        },
        SUPER_TAB_WIN: {
            X: 0,
            Y: 0,
            FRAMES_COUNT: 6,
            NAME: "super_tab_win",
            PATH: "SupBonus/SupBonusTabWin/SupBonusTabWin",
            FRAME_RATE: 6,
            REPEAT: 0,
            DELAY: 0,
            HAS_KASKA: false
        },
        SUPER_TAB_LOSE: {
            X: 0,
            Y: 0,
            FRAMES_COUNT: 6,
            NAME: "super_tab_lose",
            PATH: "SupBonus/SupBonusTabLose/SupBonusTabLose",
            FRAME_RATE: 6,
            REPEAT: 0,
            DELAY: 0,
            HAS_KASKA: false
        },
        SUPER_WIN: {
            X: 0,
            Y: 0,
            FRAMES_COUNT: 2,
            NAME: "super_win",
            PATH: "SupBonus/SupBonusWin/SupBonusWin",
            FRAME_RATE: 4,
            REPEAT: 0,
            DELAY: 0,
            HAS_KASKA: true
        },
        SUPER_WIN_KASKA: {
            X: 0,
            Y: 0,
            FRAMES_COUNT: 2,
            NAME: "super_win_kaska",
            PATH: "SupBonus/SupBonusWinKaska/SupBonusWinKaska",
            FRAME_RATE: 4,
            REPEAT: 0,
            DELAY: 0,
            HAS_KASKA: false
        },
        SUPER_LOSE: {
            X: 0,
            Y: 0,
            FRAMES_COUNT: 12,
            NAME: "super_lose",
            PATH: "SupBonus/SupBonusLose/SupBonusLose",
            FRAME_RATE: 9,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: true
        },
        SUPER_LOSE_KASKA: {
            X: 0,
            Y: 0,
            FRAMES_COUNT: 12,
            NAME: "super_lose_kaska",
            PATH: "SupBonus/SupBonusLoseKaska/SupBonusLoseKaska",
            FRAME_RATE: 9,
            REPEAT: -1,
            DELAY: 0,
            HAS_KASKA: false
        },
        COINS_1: {
            X: 150 * SCALE,
            Y: - 460 * SCALE,
            FRAMES_COUNT: 64,
            NAME: "coins",
            PATH: "coins/coins",
            FRAME_RATE: 12,
            REPEAT: -1,
            DELAY: 0,
            HIDE_ON_COMPLETE: true,
            HAS_KASKA: false
        }
    },
    SOUNDS: {
        JACKPOT_START: {
            NAME: "JackpotStart"
        },
        JACKPOT_MUSIC: {
            NAME: "JackpotMusic"
        },
        JACKPOT_COINS: {
            NAME: "JackpotCoins"
        },
        BARABAN: {
            NAME: "Baraban"
        },
        BARABAN_STOP: {
            NAME: "BarabanStop"
        },
        BONUS: {
            NAME: "Bonus"
        },
        BONUS_BANAN: {
            NAME: "BonusBanan"
        },
        BONUS_BOOM: {
            NAME: "BonusBoom"
        },
        BONUS_BOOM_AFTER: {
            NAME: "BonusBoomAfter"
        },
        BONUS_CREDITS: {
            NAME: "BonusCredits"
        },
        BONUS_PRIZE: {
            NAME: "BonusPrize"
        },
        BUTTON_BET: {
            NAME: "ButtonBet"
        },
        BUTTON_MAX_BET: {
            NAME: "ButtonMaxBet"
        },
        CARD_LOSE: {
            NAME: "CardLose"
        },
        CARD_ROVNO: {
            NAME: "CardRovno"
        },
        CARD_WIN: {
            NAME: "CardWin"
        },
        CREDITS: {
            NAME: "Credits"
        },
        CREDITS_BIG: {
            NAME: "CreditsBig"
        },
        LINE1: {
            NAME: "Line1"
        },
        LINE3: {
            NAME: "Line3"
        },
        LINE5: {
            NAME: "Line5"
        },
        LINE7: {
            NAME: "Line7"
        },
        LINE9: {
            NAME: "Line9"
        },
        MENU_CLICK: {
            NAME: "MenuClick"
        },
        S_BONUS: {
            NAME: "SBonus"
        },
        S_BONUS_LOSE: {
            NAME: "SBonusLose"
        },
        S_BONUS_WIN: {
            NAME: "SBonusWin"
        },
        S_BONUS_SLIV: {
            NAME: "SbonusSliv"
        },
        WIN_LINE1: {
            NAME: "WinLine1"
        },
        WIN_LINE2: {
            NAME: "WinLine2"
        },
        WIN_LINE3: {
            NAME: "WinLine3"
        },
        WIN_LINE4: {
            NAME: "WinLine4"
        },
        WIN_LINE5: {
            NAME: "WinLine5"
        },
        WIN_LINE6: {
            NAME: "WinLine6"
        },
        WIN_LINE7: {
            NAME: "WinLine7"
        },
        WIN_LINE8: {
            NAME: "WinLine8"
        },
        WIN_LINE9: {
            NAME: "WinLine9"
        }
    }
};
export default GameConfig;
