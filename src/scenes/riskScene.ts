import gameConfig, { GameScene } from "../gameConfig";
import { centerJustify, leftJustify, rightJustify } from "../utils/textUtils";
import GameController from "./gameController";
import GameInterface from "./gameInterface";
import MainScene from "./mainScene";
import SoundController from "./soundController";
const SCALE = gameConfig.SCALE;
const helpConfig = {
    key: "risk",
    height: gameConfig.CANVAS_SIZE.HEIGHT / 2,
    width: gameConfig.CANVAS_SIZE.WIDTH / 2
};
let riskScene: RiskScene;
export default class RiskScene extends Phaser.Scene {
    public container: Phaser.GameObjects.Container;
    public isBusy: boolean;
    private backgroundBonus: Phaser.GameObjects.Image;

    private btnBlackCard: Phaser.GameObjects.Image;
    private btnRedCard: Phaser.GameObjects.Image;
    private centerCard: Phaser.GameObjects.Image;
    private cards6: Phaser.GameObjects.Image[];

    private doubleBack: Phaser.GameObjects.Image;
    // private title: Phaser.GameObjects.Image;
    private edits = [];
    private riskText: Phaser.GameObjects.DynamicBitmapText;
    private riskSum: Phaser.GameObjects.DynamicBitmapText;

    private redText: Phaser.GameObjects.DynamicBitmapText;
    private blackText: Phaser.GameObjects.DynamicBitmapText;

    private gc: GameController;
    private gi: GameInterface;
    private main: MainScene;
    private sc: SoundController;
    private riskStep: number;
    private dealer: number;
    private picked: number;
    private doubleTo: number;
    private prizeWin: number;
    private sceneBelow: boolean;
    private takeOrRiskHighLight: {
        event: Phaser.Time.TimerEvent,
        switcher: boolean
    };
    private redOrBlack: {
        event: Phaser.Time.TimerEvent,
        switcher: boolean
    };
    private currSuit: {
        random: number;
        selected: number;
    };
    constructor() {
        super(helpConfig);
        this.container = null;
        this.backgroundBonus = null;
        this.doubleBack = null;
        this.btnBlackCard = null;
        this.centerCard = null;
        // this.dealerCard = null;
        // this.dealerBoard = null;
        // this.pickBoard = null;
        // this.talkWinImg = null;
        // this.talkLoseImg = null;
        // this.talkForwardImg = null;
        this.sceneBelow = false;
        this.isBusy = true;
        this.currSuit = {
            random: -1,
            selected: -1
        };
    }


    public create(data) {
        riskScene = this;
        const gc = this.scene.get("gamecontroller");
        if (gc instanceof GameController === false) {
            console.log("Error! GameController not found");
        } else {
            this.gc = gc as GameController;
        }
        const gi = this.scene.get("gameinterface");
        if (gi instanceof GameInterface === false) {
            console.log("Error! GameInterface not found");
        } else {
            this.gi = gi as GameInterface;
        }
        const sc = this.scene.get("soundcontroller");
        if (sc instanceof SoundController === false) {
            console.log("Error! soundController not found");
        } else {
            this.sc = sc as SoundController;
        }
        this.takeOrRiskHighLight = {
            event: null,
            switcher: false
        };
        this.redOrBlack = {
            event: null,
            switcher: false
        };
        this.cards6 = [];
        const width = gameConfig.CANVAS_SIZE.WIDTH;
        const height = gameConfig.CANVAS_SIZE.HEIGHT;
        this.container = this.make.container({ x: 0, y: 0, add: true });
        this.add.existing(this.container);
        this.container.x = 0;
        this.container.y = 0;
        this.backgroundBonus = this.add.image(width / 2, (height / 2),
            "img", gameConfig.IMAGES.MAIN_IMAGES.BACKGROUND_BONUS.PATH +
            gameConfig.IMAGES.MAIN_IMAGES.BACKGROUND_BONUS.NAME + ".png");
        this.backgroundBonus.setVisible(true);
        this.backgroundBonus.setScale(SCALE);
        let xOffset = 0;
        for (let i = 0; i < 6; i++) {
            this.cards6.push();
            this.cards6[i] = this.add.image((width / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.X_OFFSET * SCALE + xOffset,
                (height / 2) - gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.Y_OFFSET * SCALE,
                gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.NAME);
            this.cards6[i].setInteractive();
            this.cards6[i].setTexture("img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.NAME + ".png");
            this.cards6[i].name = i.toString();
            this.cards6[i].setVisible(true);
            this.cards6[i].setScale(SCALE);
            xOffset += 75 * SCALE;
        }

        this.doubleBack = this.add.image(width / 2, (height / 2),
            "img", gameConfig.IMAGES.DOUBLE_IMAGES.BACKGROUND.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.BACKGROUND.NAME + ".png");
        this.doubleBack.setVisible(true);
        this.doubleBack.setScale(SCALE);

        this.btnBlackCard = this.add.image(width / 2 - 250 * SCALE, (height / 2) + 20 * SCALE,
            "img", gameConfig.IMAGES.DOUBLE_IMAGES.BACKGROUND.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.BTN_BLACK.NAME + ".png");
        this.btnBlackCard.setVisible(true);
        this.btnBlackCard.setInteractive();
        this.btnBlackCard.setScale(SCALE);
        this.btnBlackCard.setData("Name", "black");
        this.btnBlackCard.setData("Picked", false);

        this.btnRedCard = this.add.image(width / 2 + 250 * SCALE, (height / 2) + 20 * SCALE,
            "img", gameConfig.IMAGES.DOUBLE_IMAGES.BACKGROUND.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.BTN_RED.NAME + ".png");
        this.btnRedCard.setVisible(true);
        this.btnRedCard.setInteractive();
        this.btnRedCard.setScale(SCALE);
        this.btnRedCard.setData("Name", "red");
        this.btnRedCard.setData("Picked", false);

        this.centerCard = this.add.image(width / 2, (height / 2) + 30 * SCALE,
            "img", gameConfig.IMAGES.DOUBLE_IMAGES.BACKGROUND.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.CENTER_CARD.NAME + ".png");
        this.centerCard.setVisible(true);
        this.centerCard.setScale(SCALE);

        this.riskSum = new Phaser.GameObjects.DynamicBitmapText(this, (width / 2) + 220 * SCALE, (height / 2) + 155 * SCALE,
            "microstyle", "11554", 42 * SCALE);
        this.add.existing(this.riskSum);
        this.riskSum.setTint(0xFFFF00, 0xFFFF00, 0xFFFF00, 0xFFFF00);
        this.edits.push(this.riskSum);
        this.riskSum.setVisible(true);

        this.riskText = new Phaser.GameObjects.DynamicBitmapText(this, (width / 2) + 250 * SCALE, (height / 2) + 115 * SCALE,
            "roboto-bold", "TAKE                       \n\nOR RISK ?", 42 * SCALE);
        this.add.existing(this.riskText);
        this.edits.push(this.riskText);
        this.riskText.setVisible(true);

        this.redText = new Phaser.GameObjects.DynamicBitmapText(this, (width / 2) + 215 * SCALE, (height / 2) - 8 * SCALE,
            "roboto-bold", "RED", 42 * SCALE);
        this.add.existing(this.redText);
        this.redText.setTint(0xFF0000, 0xFF0000, 0xFF0000, 0xFF0000);
        this.edits.push(this.redText);
        this.redText.setVisible(true);

        this.blackText = new Phaser.GameObjects.DynamicBitmapText(this, (width / 2) - 310 * SCALE, (height / 2) - 8 * SCALE,
            "roboto-bold", "BLACK", 42 * SCALE);
        this.add.existing(this.blackText);
        this.blackText.setTint(0x000000, 0x000000, 0x000000, 0x000000);
        this.edits.push(this.blackText);
        this.blackText.setVisible(true);

        this.container.add(this.backgroundBonus);
        this.container.add(this.doubleBack);
        this.container.add(this.cards6);
        this.container.add(this.btnRedCard);
        this.container.add(this.btnBlackCard);
        this.container.add(this.riskText);
        this.container.add(this.riskSum);


        this.container.add(this.edits);
        this.input.on("gameobjectdown", this.pickCard);

        const mainScene = this.scene.get("main");
        if (mainScene instanceof MainScene) {
            this.main = mainScene;
        }
    }

    public setTopCards() {
        for (let i = 0; i < 6; i++) {
            riskScene.cards6[i].setVisible(true);
            riskScene.cards6[i].setTexture("img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.NAME + ".png");
        }
        let j = 0;
        riskScene.gc.riskGames.forEach((element) => {
            if (element !== -1) {
                switch (element) {
                    case 0: {
                        riskScene.cards6[j].setTexture("img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.PATH + "DoubleCardBuba.png");
                        break;
                    }
                    case 1: {
                        riskScene.cards6[j].setTexture("img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.PATH + "DoubleCardChirva.png");
                        break;
                    }
                    case 2: {
                        riskScene.cards6[j].setTexture("img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.PATH + "DoubleCardKrest.png");
                        break;
                    }
                    case 3: {
                        riskScene.cards6[j].setTexture("img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.PATH + "DoubleCardPika.png");
                        break;
                    }
                }
                // const rt = riskScene.getRandomTexture(element);
                // console.log(rt);
                // riskScene.cards6[j].setTexture("img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_BACK.NAME + ".png");
                j++;
            }
        });
        // riskScene.gc.riskGames
    }

    public start() {
        const width = gameConfig.CANVAS_SIZE.WIDTH;
        const height = gameConfig.CANVAS_SIZE.HEIGHT;
        this.gi.setBtnStatuses("risk");
        if (this.sceneBelow === false) {
            this.scene.moveBelow("gamecontroller");
            this.sceneBelow = true;
        }
        this.gi.btnX2SpinEvent.started = false;
        this.gi.time.removeAllEvents();
        this.time.removeAllEvents();
        this.backgroundBonus.setVisible(true);
        this.backgroundBonus.setTint(0x354f77);
        this.highLightRedOrBlack();
        this.isBusy = false;
        this.prizeWin = this.gc.currentTotalWin;
        riskScene.riskSum.setText(rightJustify(riskScene.prizeWin.toString(), 6, " "));
        riskScene.riskText.setText(rightJustify("TAKE                       \n\nOR RISK ?", 6, " "));
        this.doubleTo = this.prizeWin * 2;
        riskScene.doubleBack.setVisible(true);
        riskScene.btnRedCard.setVisible(true);
        riskScene.btnBlackCard.setVisible(true);
        riskScene.centerCard.setVisible(true);
        riskScene.riskSum.setVisible(true);
        riskScene.riskText.setVisible(true);
        riskScene.redText.setVisible(true);
        riskScene.blackText.setVisible(true);
        riskScene.centerCard.setTexture("img", "Double/DoubleCenterCard.png");
        for (let i = 0; i < 6; i++) {
            riskScene.cards6[i].setVisible(true);
            riskScene.cards6[i].name = i.toString();
        }
        riskScene.gc.currentScene = GameScene.Risk;
        riskScene.scene.resume("risk");

    }
    public stop(key) {
        riskScene.riskText.setText(rightJustify("LOSE...", 6, " "));
        riskScene.gi.setBtnStatuses("main");
        riskScene.time.removeAllEvents();
        riskScene.events.emit("takeOrRisk-OFF");
        riskScene.backgroundBonus.setVisible(false);
        riskScene.doubleBack.setVisible(false);
        riskScene.btnRedCard.setVisible(false);
        riskScene.btnBlackCard.setVisible(false);
        riskScene.centerCard.setVisible(false);
        riskScene.riskSum.setVisible(false);
        riskScene.riskText.setVisible(false);
        riskScene.redText.setVisible(false);
        riskScene.blackText.setVisible(false);
        for (let i = 0; i < 6; i++) {
            riskScene.cards6[i].setVisible(false);
            riskScene.cards6[i].name = i.toString();
        }
        riskScene.gc.currentScene = GameScene.Main;
        if (key === "lose") {
            riskScene.gc.currentTotalWin = 0;
            riskScene.gi.showDefault();
        }
        riskScene.scene.pause("risk");
    }

    private getRandomTexture(suit) {
        // const suit = riskScene.currSuit.random;
        // const suit = Math.floor(Math.random() * 4);
        // const symbol = Math.floor(Math.random() * 13);
        const symbol = 12;
        for (const img in gameConfig.IMAGES.DOUBLE_CARDS) {
            if (gameConfig.IMAGES.DOUBLE_CARDS.hasOwnProperty(img)) {
                if (gameConfig.IMAGES.DOUBLE_CARDS[img].INDEX === suit) {
                    return {
                        texture: gameConfig.IMAGES.DOUBLE_CARDS[img].NAME_PREFIX +
                            gameConfig.IMAGES.DOUBLE_CARDS[img].NAMES[symbol],
                        number: symbol,
                        path: gameConfig.IMAGES.DOUBLE_CARDS[img].PATH
                    };
                }
            }
        }
    }

    private changeTexture(t) {
        const rt = riskScene.getRandomTexture(riskScene.currSuit.random);
        t.setTexture("img", rt.path + rt.texture + ".png");
        if (t.name === "dealer") {
            riskScene.dealer = rt.number;
        }
        if (t.name === "picked") {
            riskScene.picked = rt.number;
        }
    }

    private swapAnim(target) {
        this.time.delayedCall(250, this.changeTexture, [target], this);
        this.tweens.add({
            targets: target,
            scaleX: 0,
            ease: "Power",
            duration: 250,
            yoyo: true,
            repeat: 0,
            onComplete: this.reverseAnim(target)
        });
    }
    private reverseAnim(target) {
        this.tweens.add({
            targets: target,
            scaleX: 1,
            ease: "Power2",
            duration: 100,
            yoyo: false,
            repeat: 0,
            onComplete: this.onCompleteAnimCenterCard
        });
    }

    private goNext() {
        riskScene.prizeWin *= 2;
        riskScene.riskSum.setText(rightJustify(riskScene.prizeWin.toString(), 6, " "));
        riskScene.riskSum.setVisible(true);
        riskScene.riskText.setVisible(true);
        riskScene.doubleTo = riskScene.prizeWin * 2;
        riskScene.gc.currentTotalWin = riskScene.prizeWin;
        riskScene.gi.updateEdits();
        riskScene.centerCard.setTexture("img", "Double/DoubleCenterCard.png");
        riskScene.highLightRedOrBlack();
        riskScene.gi.setBtnStatuses("risk");
        riskScene.isBusy = false;
    }

    private onCompleteAnimCenterCard() {
        console.log(riskScene.currSuit);
        if (riskScene.gc.riskGames.length > 5) {
            riskScene.gc.riskGames.splice(0, 0, riskScene.currSuit.random);
            riskScene.gc.riskGames.pop();
        } else {
            riskScene.gc.riskGames.push(riskScene.currSuit.random);
        }
        console.log(riskScene.gc.riskGames);

        riskScene.setTopCards();

        // var arr = [];
        // arr[0] = "Jani";
        // arr[1] = "Hege";
        // arr[2] = "Stale";
        // arr[3] = "Kai Jim";
        // arr[4] = "Borge";
        // console.log(arr.join());
        // arr.splice(0, 0, "Lene");
        // arr.pop();
        // console.log(arr.join());


        if (riskScene.currSuit.selected === 0) {
            if ((riskScene.currSuit.random === 0) || (riskScene.currSuit.random === 1)) {
                console.log("Угадали красную");
                riskScene.time.addEvent({
                    delay: 2500, callback: riskScene.goNext, callbackScope: this, loop: false, args: [riskScene.prizeWin]
                });
            } else {
                riskScene.time.addEvent({
                    delay: 2500, callback: riskScene.stop, callbackScope: this, loop: false, args: ["lose"]
                });
            }
        }
        if (riskScene.currSuit.selected === 1) {
            if ((riskScene.currSuit.random === 2) || (riskScene.currSuit.random === 3)) {
                console.log("Угадали черную");
                riskScene.time.addEvent({
                    delay: 2500, callback: riskScene.goNext, callbackScope: this, loop: false, args: [riskScene.prizeWin]
                });
            } else {
                riskScene.time.addEvent({
                    delay: 2500, callback: riskScene.stop, callbackScope: this, loop: false, args: ["lose"]
                });
            }
        }
    }

    private pickCard(pointer, gameObject) {
        if (riskScene.isBusy === false) {
            riskScene.gi.setBtnStatuses("alldis");
            riskScene.riskSum.setVisible(false);
            riskScene.riskText.setVisible(false);
            riskScene.isBusy = true;
            gameObject.name = "picked";
            riskScene.currSuit.random = Math.floor(Math.random() * 4);
            riskScene.time.removeAllEvents();
            if (gameObject.getData("Name") === "red") {
                riskScene.btnRedCard.clearTint();
                riskScene.btnBlackCard.setTint(0x354f77);
                riskScene.currSuit.selected = 0;
            } else {
                riskScene.btnRedCard.setTint(0x354f77);
                riskScene.btnBlackCard.clearTint();
                riskScene.currSuit.selected = 1;
            }
            riskScene.riskStep++;
            riskScene.swapAnim(riskScene.centerCard);

            // console.log("gameObject", gameObject.getData("Name"));

            // riskScene.sc.playSound({ NAME: "CardLose", LOOP: false });
            // for (let i = 0; i < 4; i++) {
            //     riskScene.cards4[i].disableInteractive();
            // }
            // riskScene.events.emit("takeOrRisk-OFF");
            // riskScene.changeTexture(gameObject);
            // riskScene.time.addEvent({
            //     delay: 900, callback: riskScene.openOtherCards, callbackScope: this, loop: false, args: [gameObject]
            // });

            // if (riskScene.picked > riskScene.dealer) {
            //     riskScene.prizeWin *= 2;
            //     riskScene.riskSum.setText(rightJustify(riskScene.prizeWin.toString(), 6, " "));
            //     riskScene.doubleTo = riskScene.prizeWin * 2;
            //     riskScene.gc.currentTotalWin = riskScene.prizeWin;
            //     riskScene.events.emit("showPrizeWinSum", riskScene.prizeWin);
            //     riskScene.time.addEvent({
            //         delay: 3500, callback: riskScene.processNext, callbackScope: this, loop: false, args: [riskScene.prizeWin]
            //     });
            // } else
            //     if (riskScene.picked < riskScene.dealer) {
            //         riskScene.gc.currentTotalWin = 0;
            //         riskScene.time.addEvent({
            //             delay: 3500, callback: riskScene.stop, callbackScope: this, loop: false, args: ["lose"]
            //         });
            //     } else {
            //         riskScene.time.addEvent({
            //             delay: 3500, callback: riskScene.processNext, callbackScope: this, loop: false
            //         });
            //     }
        }
    }

    private onHighLightRedOrBlack() {
        if (riskScene.redOrBlack.switcher) {
            riskScene.btnRedCard.clearTint();
            riskScene.btnBlackCard.setTint(0x354f77);
        } else {
            riskScene.btnRedCard.setTint(0x354f77);
            riskScene.btnBlackCard.clearTint();
        }
        riskScene.redOrBlack.switcher = !riskScene.redOrBlack.switcher;
    }

    private highLightRedOrBlack() {
        riskScene.time.removeAllEvents();
        riskScene.redOrBlack.event = riskScene.time.addEvent({
            delay: 100, callback: riskScene.onHighLightRedOrBlack, callbackScope: riskScene, loop: true
        });
        riskScene.redOrBlack.switcher = false;
    }
}

// export default class RiskScene extends Phaser.Scene {
//     public container: Phaser.GameObjects.Container;
//     public isBusy: boolean;
//     private backgroundBonus: Phaser.GameObjects.Image;
//     private doubleBack: Phaser.GameObjects.Image;
//     private dealerCard: Phaser.GameObjects.Image;
//     private dealerBoard: Phaser.GameObjects.Image;
//     private pickBoard: Phaser.GameObjects.Image;
//     private talkWinImg: Phaser.GameObjects.Image;
//     private talkLoseImg: Phaser.GameObjects.Image;
//     private talkForwardImg: Phaser.GameObjects.Image;
//     private title: Phaser.GameObjects.Image;
//     private edits = [];
//     private cards4: Phaser.GameObjects.Image[];
//     private riskText: Phaser.GameObjects.DynamicBitmapText;
//     private riskSum: Phaser.GameObjects.DynamicBitmapText;
//     private gc: GameController;
//     private gi: GameInterface;
//     private sc: SoundController;
//     private riskStep: number;
//     private dealer: number;
//     private picked: number;
//     private doubleTo: number;
//     private prizeWin: number;
//     private sceneBelow: boolean;
//     private takeOrRiskHighLight: {
//         event: Phaser.Time.TimerEvent,
//         switcher: boolean
//     };
//     constructor() {
//         super(helpConfig);
//         this.container = null;
//         this.backgroundBonus = null;
//         this.doubleBack = null;
//         this.dealerCard = null;
//         this.dealerBoard = null;
//         this.pickBoard = null;
//         this.talkWinImg = null;
//         this.talkLoseImg = null;
//         this.talkForwardImg = null;
//         this.sceneBelow = false;
//         this.isBusy = true;
//     }
//     public start() {
//         const width = gameConfig.CANVAS_SIZE.WIDTH;
//         const height = gameConfig.CANVAS_SIZE.HEIGHT;

//         this.gi.setBtnStatuses("risk");
//         if (this.sceneBelow === false) {
//             this.scene.moveBelow("gamecontroller");
//             this.sceneBelow = true;
//         }
//         this.gi.btnX2SpinEvent.started = false;
//         this.gi.time.removeAllEvents();

//         this.time.removeAllEvents();
//         this.backgroundBonus.setVisible(true);
//         this.doubleBack.setVisible(true);
//         this.dealerCard.setVisible(true);
//         this.dealerBoard.setVisible(true);
//         riskScene.riskSum.setVisible(true);
//         riskScene.riskText.setVisible(true);
//         for (let i = 0; i < 4; i++) {
//             this.cards4[i].setTexture("img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.NAME + ".png");
//             this.cards4[i].setVisible(true);
//             this.input.setHitArea(this.cards4[i]);
//             this.cards4[i].disableInteractive();
//         }
//         this.dealerCard.setTexture("img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.NAME + ".png");
//         this.riskStep = 1;
//         this.swapAnim(this.dealerCard);
//         this.takeOrRiskHighLight = {
//             event: null,
//             switcher: false
//         };
//         this.prizeWin = this.gc.currentTotalWin;
//         riskScene.riskSum.setText(rightJustify(riskScene.prizeWin.toString(), 6, " "));
//         this.doubleTo = this.prizeWin * 2;
//     }

//     public create(data) {
//         riskScene = this;
//         const gc = this.scene.get("gamecontroller");
//         if (gc instanceof GameController === false) {
//             console.log("Error! GameController not found");
//         } else {
//             this.gc = gc as GameController;
//         }
//         const gi = this.scene.get("gameinterface");
//         if (gi instanceof GameInterface === false) {
//             console.log("Error! GameInterface not found");
//         } else {
//             this.gi = gi as GameInterface;
//         }
//         const sc = this.scene.get("soundcontroller");
//         if (sc instanceof SoundController === false) {
//             console.log("Error! soundController not found");
//         } else {
//             this.sc = sc as SoundController;
//         }

//         this.takeOrRiskHighLight = {
//             event: null,
//             switcher: false
//         };
//         this.cards4 = [];
//         const width = gameConfig.CANVAS_SIZE.WIDTH;
//         const height = gameConfig.CANVAS_SIZE.HEIGHT;
//         this.container = this.make.container({ x: 0, y: 0, add: true });
//         this.add.existing(this.container);
//         this.container.x = 0;
//         this.container.y = 0;
//         this.backgroundBonus = this.add.image(width / 2, (height / 2),
//             "img", gameConfig.IMAGES.MAIN_IMAGES.BACKGROUND_BONUS.PATH +
//             gameConfig.IMAGES.MAIN_IMAGES.BACKGROUND_BONUS.NAME + ".png");
//         this.backgroundBonus.setVisible(false);
//         this.backgroundBonus.setScale(SCALE);
//         this.dealerCard = this.add.image((width / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.X_OFFSET * SCALE,
//             (height / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.Y_OFFSET * SCALE,
//             "img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.PATH +
//             gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.NAME + ".png");
//         this.dealerCard.name = "dealer";
//         this.dealerCard.setVisible(false);
//         this.dealerCard.setScale(SCALE);
//         this.dealerBoard = this.add.image((width / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.BOARD_DEALER.X_OFFSET * SCALE,
//             (height / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.BOARD_DEALER.Y_OFFSET * SCALE,
//             "img", gameConfig.IMAGES.DOUBLE_IMAGES.BOARD_DEALER.PATH +
//             gameConfig.IMAGES.DOUBLE_IMAGES.BOARD_DEALER.NAME + ".png");
//         this.dealerBoard.setVisible(false);
//         this.dealerBoard.setScale(SCALE);
//         let xOffset = 165 * SCALE;
//         for (let i = 0; i < 4; i++) {
//             this.cards4.push();
//             this.cards4[i] = this.add.image((width / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.X_OFFSET * SCALE + xOffset,
//                 (height / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.Y_OFFSET * SCALE,
//                 gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.NAME);
//             this.cards4[i].setInteractive();
//             this.cards4[i].setTexture("img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.NAME + ".png");
//             this.cards4[i].name = i.toString();
//             this.cards4[i].setVisible(false);
//             this.cards4[i].setScale(SCALE);
//             xOffset += 125 * SCALE;
//         }

//         this.doubleBack = this.add.image(width / 2, (height / 2),
//             "img", gameConfig.IMAGES.DOUBLE_IMAGES.BACKGROUND.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.BACKGROUND.NAME + ".png");
//         this.doubleBack.setVisible(false);
//         this.doubleBack.setScale(SCALE);

//         this.pickBoard = this.add.image((width / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.BOARD_PICK.X_OFFSET * SCALE,
//             (height / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.BOARD_PICK.Y_OFFSET * SCALE,
//             "img", gameConfig.IMAGES.DOUBLE_IMAGES.BOARD_PICK.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.BOARD_PICK.NAME + ".png");
//         this.pickBoard.setVisible(true);
//         this.pickBoard.setScale(SCALE);

//         this.talkWinImg = this.add.image((width / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.TALK_WIN.X_OFFSET * SCALE,
//             (height / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.TALK_WIN.Y_OFFSET * SCALE,
//             "img", gameConfig.IMAGES.DOUBLE_IMAGES.TALK_WIN.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.TALK_WIN.NAME + ".png");
//         this.talkWinImg.setVisible(false);
//         this.talkWinImg.setScale(SCALE);

//         this.talkLoseImg = this.add.image((width / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.TALK_LOSE.X_OFFSET * SCALE,
//             (height / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.TALK_LOSE.Y_OFFSET * SCALE,
//             "img", gameConfig.IMAGES.DOUBLE_IMAGES.TALK_LOSE.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.TALK_LOSE.NAME + ".png");
//         this.talkLoseImg.setVisible(false);
//         this.talkLoseImg.setScale(SCALE);

//         this.talkForwardImg = this.add.image((width / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.TALK_FORWARD.X_OFFSET * SCALE,
//             (height / 2) + gameConfig.IMAGES.DOUBLE_IMAGES.TALK_FORWARD.Y_OFFSET * SCALE,
//             "img", gameConfig.IMAGES.DOUBLE_IMAGES.TALK_FORWARD.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.TALK_FORWARD.NAME + ".png");
//         this.talkForwardImg.setVisible(false);
//         this.talkForwardImg.setScale(SCALE);

//         this.riskSum = new Phaser.GameObjects.DynamicBitmapText(this, (width / 2) - 20 * SCALE, (height / 2) + 114 * SCALE,
//             "microstyle", "11554", 42 * SCALE);
//         this.add.existing(this.riskSum);
//         this.riskSum.setTint(0xFFFF00, 0xFFFF00, 0xFFFF00, 0xFFFF00);
//         this.edits.push(this.riskSum);
//         this.riskSum.setVisible(true);

//         this.riskText = new Phaser.GameObjects.DynamicBitmapText(this, (width / 2) - 120 * SCALE, (height / 2) + 115 * SCALE,
//             "roboto-bold", "TAKE                       OR RISK ?", 42 * SCALE);
//         this.add.existing(this.riskText);
//         this.edits.push(this.riskText);
//         this.riskText.setVisible(true);

//         this.container.add(this.backgroundBonus);
//         this.container.add(this.doubleBack);
//         this.container.add(this.dealerCard);
//         this.container.add(this.dealerBoard);
//         this.container.add(this.cards4);
//         this.container.add(this.pickBoard);
//         this.container.add(this.talkWinImg);
//         this.container.add(this.talkLoseImg);
//         this.container.add(this.talkForwardImg);
//         this.container.add(this.edits);
//         this.input.on("gameobjectdown", this.pickCard);
//     }
//     public stop(key) {
//         riskScene.gi.setBtnStatuses("main");
//         riskScene.time.removeAllEvents();
//         riskScene.events.emit("takeOrRisk-OFF");
//         riskScene.backgroundBonus.setVisible(false);
//         riskScene.doubleBack.setVisible(false);
//         riskScene.dealerCard.setVisible(false);
//         riskScene.dealerBoard.setVisible(false);
//         riskScene.pickBoard.setVisible(false);
//         riskScene.riskSum.setVisible(false);
//         riskScene.riskText.setVisible(false);
//         riskScene.talkWinImg.setVisible(false);
//         riskScene.talkLoseImg.setVisible(false);
//         riskScene.talkForwardImg.setVisible(false);
//         for (let i = 0; i < 4; i++) {
//             riskScene.cards4[i].setVisible(false);
//             riskScene.cards4[i].name = i.toString();
//         }
//         riskScene.scene.pause("risk");
//         riskScene.gc.currentScene = GameScene.Main;
//         if (key === "lose") {
//             riskScene.gi.showDefault();
//         }
//     }

//     private changeTextureToSuit(tex) {
//         tex.setTexture("img", gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.PATH + gameConfig.IMAGES.DOUBLE_IMAGES.CARD_DEALER.NAME + ".png");
//     }

//     private changeCardsToSuit() {
//         let k = 1;
//         riskScene.changeTextureToSuit(riskScene.dealerCard);
//         for (let i = 0; i < 4; i++) {
//             const card = riskScene.cards4[i];
//             riskScene.time.addEvent({
//                 delay: 33 * k++, callback: riskScene.changeTextureToSuit, callbackScope: riskScene, loop: false, args: [card]
//             });
//         }
//     }

//     private openOtherCards(gameObject) {
//         riskScene.sc.playSound({ NAME: "CardLose", LOOP: false });
//         let k = 1;
//         for (let i = 0; i < 4; i++) {
//             const card = riskScene.cards4[i];
//             if (card.name !== gameObject.name) {
//                 riskScene.time.addEvent({
//                     delay: 33 * k++, callback: riskScene.changeTexture, callbackScope: riskScene, loop: false, args: [card]
//                 });
//             }
//         }
//     }

//     private processNext(sum) {
//         riskScene.talkWinImg.setVisible(false);
//         riskScene.talkForwardImg.setVisible(false);
//         for (let i = 0; i < 4; i++) {
//             riskScene.cards4[i].name = i.toString();
//         }
//         riskScene.changeCardsToSuit();
//         riskScene.swapAnim(riskScene.dealerCard);
//         riskScene.gi.setBtnStatuses("risk");
//         riskScene.isBusy = false;
//     }
//     private pickCard(pointer, gameObject) {
//         if (riskScene.isBusy === false) {
//             riskScene.isBusy = true;
//             gameObject.name = "picked";
//             riskScene.time.removeAllEvents();
//             riskScene.riskStep++;
//             riskScene.sc.playSound({ NAME: "CardLose", LOOP: false });
//             for (let i = 0; i < 4; i++) {
//                 riskScene.cards4[i].disableInteractive();
//             }
//             riskScene.events.emit("takeOrRisk-OFF");
//             riskScene.changeTexture(gameObject);
//             riskScene.time.addEvent({
//                 delay: 900, callback: riskScene.openOtherCards, callbackScope: this, loop: false, args: [gameObject]
//             });

//             if (riskScene.picked > riskScene.dealer) {
//                 riskScene.prizeWin *= 2;
//                 riskScene.riskSum.setText(rightJustify(riskScene.prizeWin.toString(), 6, " "));
//                 riskScene.doubleTo = riskScene.prizeWin * 2;
//                 riskScene.gc.currentTotalWin = riskScene.prizeWin;
//                 riskScene.events.emit("showPrizeWinSum", riskScene.prizeWin);
//                 riskScene.time.addEvent({
//                     delay: 3500, callback: riskScene.processNext, callbackScope: this, loop: false, args: [riskScene.prizeWin]
//                 });
//             } else
//                 if (riskScene.picked < riskScene.dealer) {
//                     riskScene.gc.currentTotalWin = 0;
//                     riskScene.time.addEvent({
//                         delay: 3500, callback: riskScene.stop, callbackScope: this, loop: false, args: ["lose"]
//                     });
//                 } else {
//                     riskScene.time.addEvent({
//                         delay: 3500, callback: riskScene.processNext, callbackScope: this, loop: false
//                     });
//                 }
//         }
//     }
//     private getRandomTexture() {
//         const suit = Math.floor(Math.random() * 4);
//         const symbol = Math.floor(Math.random() * 13);
//         for (const img in gameConfig.IMAGES.DOUBLE_CARDS) {
//             if (gameConfig.IMAGES.DOUBLE_CARDS.hasOwnProperty(img)) {
//                 if (gameConfig.IMAGES.DOUBLE_CARDS[img].INDEX === suit) {
//                     return {
//                         texture: gameConfig.IMAGES.DOUBLE_CARDS[img].NAME_PREFIX +
//                             gameConfig.IMAGES.DOUBLE_CARDS[img].NAMES[symbol],
//                         number: symbol,
//                         path: gameConfig.IMAGES.DOUBLE_CARDS[img].PATH
//                     };
//                 }
//             }
//         }
//     }
//     private changeTexture(t) {
//         const rt = riskScene.getRandomTexture();
//         t.setTexture("img", rt.path + rt.texture + ".png");
//         if (t.name === "dealer") {
//             riskScene.dealer = rt.number;
//         }
//         if (t.name === "picked") {
//             riskScene.picked = rt.number;
//         }
//     }
//     private swapAnim(target) {
//         this.time.delayedCall(250, this.changeTexture, [target], this);
//         this.tweens.add({
//             targets: target,
//             scaleX: 0,
//             ease: "Power",
//             duration: 250,
//             yoyo: true,
//             repeat: 0,
//             onComplete: this.reverseAnim(target)
//         });
//     }
//     private reverseAnim(target) {
//         this.tweens.add({
//             targets: target,
//             scaleX: 1,
//             ease: "Power2",
//             duration: 100,
//             yoyo: false,
//             repeat: 0,
//             onComplete: this.highLightTakeOrRisk(target)
//         });
//     }

//     private onHighLightTakeOrRisk(target) {
//         if (riskScene.takeOrRiskHighLight.switcher) {
//             riskScene.events.emit("takeOrRisk-ON");
//         } else {
//             riskScene.events.emit("takeOrRisk-OFF");
//         }
//         riskScene.takeOrRiskHighLight.switcher = !riskScene.takeOrRiskHighLight.switcher;
//     }
//     private enableCards(target) {
//         for (let i = 0; i < 4; i++) {
//             riskScene.cards4[i].setInteractive();
//         }
//         if (target.name === "dealer") {
//             riskScene.isBusy = false;
//         }
//         this.gi.setBtnStatuses("risk");
//     }
//     private highLightTakeOrRisk(target) {
//         riskScene.time.removeAllEvents();
//         riskScene.takeOrRiskHighLight.event = riskScene.time.addEvent({
//             delay: 520, callback: riskScene.onHighLightTakeOrRisk, callbackScope: riskScene, loop: true, args: [target]
//         });
//         riskScene.takeOrRiskHighLight.event = riskScene.time.addEvent({
//             delay: 550, callback: riskScene.enableCards, callbackScope: riskScene, loop: false, args: [target]
//         });
//     }
// }
