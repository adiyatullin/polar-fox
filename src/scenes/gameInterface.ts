import gameConfig, { GameScene, ScreenMode } from "../gameConfig";
import GameConfig from "../gameConfig";
import { rightJustify } from "../utils/textUtils";
import GameController from "./gameController";
import RiskScene from "./riskScene";
import SettingsScene from "./settingsScene";
import SoundController from "./soundController";
const SCALE = gameConfig.SCALE;

let gameInt: GameInterface;

const gameInterfaceConfig = {
    key: "gameinterface",
    height: gameConfig.CANVAS_SIZE.HEIGHT / 2,
    width: gameConfig.CANVAS_SIZE.WIDTH / 2
};
export default class GameInterface extends Phaser.Scene {

    public bandImage: Phaser.GameObjects.Graphics;
    public gameBonusText: Phaser.GameObjects.Image; // Текст 15 Bonus Free Games посредине экрана
    public gameBonusWin: {
        image: Phaser.GameObjects.Image; // Деревяшка с суммой выигрыша и количеством фригеймов посредине экрана
        text: Phaser.GameObjects.DynamicBitmapText;
        sum: Phaser.GameObjects.DynamicBitmapText
    };
    public btnX2SpinEvent: {
        event: Phaser.Time.TimerEvent;
        started: boolean;
        switch: boolean;
    };
    public currWinDisplay = {
        win: 0,
        credit: 0,
        count: 0,
        discount: 1,
        progress: 0,
        sound: false
    };
    public gc: GameController;
    private settingsScene: SettingsScene;
    private sc: SoundController;
    private riskScene: RiskScene;
    private balanceEdt: {
        text: Phaser.GameObjects.DynamicBitmapText,
        sum: Phaser.GameObjects.DynamicBitmapText
    };
    private linesEdt: {
        text: Phaser.GameObjects.DynamicBitmapText,
        sum: Phaser.GameObjects.DynamicBitmapText
    };
    private winEdt: {
        text: Phaser.GameObjects.DynamicBitmapText,
        sum: Phaser.GameObjects.DynamicBitmapText
    };
    private betPerLineEdt: {
        text: Phaser.GameObjects.DynamicBitmapText,
        sum: Phaser.GameObjects.DynamicBitmapText
    };
    private betTotalEdt: {
        text: Phaser.GameObjects.DynamicBitmapText,
        sum: Phaser.GameObjects.DynamicBitmapText
    };
    private bonusTotalWinEdt: {
        text: Phaser.GameObjects.DynamicBitmapText,
        sum: Phaser.GameObjects.DynamicBitmapText
    };
    private bonusCurrentGame: {
        text: Phaser.GameObjects.DynamicBitmapText,
        sum: Phaser.GameObjects.DynamicBitmapText
    };

    private dischargeWinEvent: Phaser.Time.TimerEvent;
    private btnSpin: Phaser.GameObjects.Sprite;
    private btnX2: Phaser.GameObjects.Sprite;
    private btnMenu: Phaser.GameObjects.Sprite;
    private btnSettings: Phaser.GameObjects.Sprite;
    private btnMoreSettings: Phaser.GameObjects.Sprite;
    private btnAuto: Phaser.GameObjects.Sprite;
    private container: Phaser.GameObjects.Container;

    private edits = [];
    private bandTexts = [];
    private buttons = [];





    constructor() {
        super(gameInterfaceConfig);
        this.balanceEdt = {
            text: null,
            sum: null
        };
        this.linesEdt = {
            text: null,
            sum: null
        };
        this.winEdt = {
            text: null,
            sum: null
        };
        this.betPerLineEdt = {
            text: null,
            sum: null
        };
        this.betTotalEdt = {
            text: null,
            sum: null
        };
        this.bonusTotalWinEdt = {
            text: null,
            sum: null
        };
        this.bonusCurrentGame = {
            text: null,
            sum: null
        };
        this.container = null;
        this.gameBonusText = null;
        this.gameBonusWin = {
            image: null,
            text: null,
            sum: null
        };
        this.btnX2SpinEvent = {
            event: null,
            started: false,
            switch: false
        };
    }
    public create(data) {
        gameInt = this;
        const width = gameConfig.CANVAS_SIZE.WIDTH;
        const height = gameConfig.CANVAS_SIZE.HEIGHT;




        const gc = this.scene.get("gamecontroller");
        if (gc instanceof GameController === false) {
            // console.log("Error! GameController not found");
        } else {
            this.gc = gc as GameController;
        }

        const sc = this.scene.get("soundcontroller");
        if (sc instanceof SoundController === false) {
            // console.log("Error! GameController not found");
        } else {
            this.sc = sc as SoundController;
        }

        const riskScene = this.scene.get("risk");
        if (riskScene instanceof RiskScene === false) {
            // console.log("Error! RiskScene not found");
        } else {
            this.riskScene = riskScene as RiskScene;
        }

        const settingsScene = this.scene.get("settings");
        if (settingsScene instanceof SettingsScene === false) {
            console.log("Error! SettingsScene not found");
        } else {
            this.settingsScene = settingsScene as SettingsScene;
        }

        this.balanceEdt.text = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 94 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 5 * SCALE, "roboto-bold", "БАЛАНС", 25 * SCALE);
        this.balanceEdt.sum = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 5 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 30 * SCALE, "microstyle", "555", 40 * SCALE);
        this.balanceEdt.sum.setTint(0xF1F100, 0xF1F100, 0xF1F100, 0xF1F100);
        this.balanceEdt.text.setVisible(true);
        this.balanceEdt.sum.setVisible(true);
        this.balanceEdt.text.setDepth(100);
        this.balanceEdt.sum.setDepth(100);
        this.bandTexts.push(this.balanceEdt.text);
        this.edits.push(this.balanceEdt.sum);
        this.add.existing(this.balanceEdt.text);
        this.add.existing(this.balanceEdt.sum);

        this.linesEdt.text = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 340 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 5 * SCALE, "roboto-bold", "ЛИНИИ", 25 * SCALE);
        this.linesEdt.sum = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 350 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 30 * SCALE, "microstyle", "10", 40 * SCALE);
        this.linesEdt.sum.setTint(0xF1F100, 0xF1F100, 0xF1F100, 0xF1F100);
        this.linesEdt.text.setVisible(true);
        this.linesEdt.sum.setVisible(true);
        this.linesEdt.text.setDepth(100);
        this.linesEdt.sum.setDepth(100);
        this.bandTexts.push(this.linesEdt.text);
        this.edits.push(this.linesEdt.sum);
        this.add.existing(this.linesEdt.text);
        this.add.existing(this.linesEdt.sum);

        this.winEdt.text = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 580 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 5 * SCALE, "roboto-bold", "ВЫИГРЫШ", 25 * SCALE);
        this.winEdt.sum = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 610 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y - 50 * SCALE, "microstyle", "10", 40 * SCALE);
        this.winEdt.sum.setTint(0xF1F100, 0xF1F100, 0xF1F100, 0xF1F100);
        this.winEdt.text.setVisible(true);
        this.winEdt.sum.setVisible(true);
        this.winEdt.text.setDepth(100);
        this.winEdt.sum.setDepth(100);
        this.bandTexts.push(this.winEdt.text);
        this.edits.push(this.winEdt.sum);
        this.add.existing(this.winEdt.text);
        this.add.existing(this.winEdt.sum);

        this.betPerLineEdt.text = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 800 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 5 * SCALE, "roboto-bold", "СТАВКА НА ЛИНИЮ", 25 * SCALE);
        this.betPerLineEdt.sum = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 870 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 30 * SCALE, "microstyle", "10", 40 * SCALE);
        this.betPerLineEdt.sum.setTint(0xF1F100, 0xF1F100, 0xF1F100, 0xF1F100);
        this.betPerLineEdt.text.setVisible(true);
        this.betPerLineEdt.sum.setVisible(true);
        this.betPerLineEdt.text.setDepth(100);
        this.betPerLineEdt.sum.setDepth(100);
        this.bandTexts.push(this.betPerLineEdt.text);
        this.edits.push(this.betPerLineEdt.sum);
        this.add.existing(this.betPerLineEdt.text);
        this.add.existing(this.betPerLineEdt.sum);

        this.betTotalEdt.text = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 1100 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 5 * SCALE, "roboto-bold", "СТАВКА", 25 * SCALE);
        this.betTotalEdt.sum = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 1100 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 30 * SCALE, "microstyle", "100", 40 * SCALE);
        this.betTotalEdt.sum.setTint(0xF1F100, 0xF1F100, 0xF1F100, 0xF1F100);
        this.betTotalEdt.text.setVisible(true);
        this.betTotalEdt.sum.setVisible(true);
        this.betTotalEdt.text.setDepth(100);
        this.betTotalEdt.sum.setDepth(100);
        this.bandTexts.push(this.betTotalEdt.text);
        this.edits.push(this.betTotalEdt.sum);
        this.add.existing(this.betTotalEdt.text);
        this.add.existing(this.betTotalEdt.sum);


        this.bonusTotalWinEdt.text = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 340 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 20 * SCALE, "roboto-bold", "ВЫИГРЫШ:", 25 * SCALE);
        this.bonusTotalWinEdt.sum = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 500 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 10 * SCALE, "microstyle", "99999", 40 * SCALE);
        this.bonusTotalWinEdt.sum.setTint(0xF1F100, 0xF1F100, 0xF1F100, 0xF1F100);
        this.bonusTotalWinEdt.text.setVisible(false);
        this.bonusTotalWinEdt.sum.setVisible(false);
        this.bonusTotalWinEdt.text.setDepth(100);
        this.bonusTotalWinEdt.sum.setDepth(100);
        this.bandTexts.push(this.bonusTotalWinEdt.text);
        this.edits.push(this.bonusTotalWinEdt.sum);
        this.add.existing(this.bonusTotalWinEdt.text);
        this.add.existing(this.bonusTotalWinEdt.sum);


        this.bonusCurrentGame.text = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 740 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 20 * SCALE, "roboto-bold", "ИГРА:", 25 * SCALE);
        this.bonusCurrentGame.sum = new Phaser.GameObjects.DynamicBitmapText(this, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 840 * SCALE,
            gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 10 * SCALE, "microstyle", "15/15", 40 * SCALE);
        this.bonusCurrentGame.sum.setTint(0xF1F100, 0xF1F100, 0xF1F100, 0xF1F100);
        this.bonusCurrentGame.text.setVisible(false);
        this.bonusCurrentGame.sum.setVisible(false);
        this.bonusCurrentGame.text.setDepth(100);
        this.bonusCurrentGame.sum.setDepth(100);
        this.bandTexts.push(this.bonusCurrentGame.text);
        this.edits.push(this.bonusCurrentGame.sum);
        this.add.existing(this.bonusCurrentGame.text);
        this.add.existing(this.bonusCurrentGame.sum);



        this.gameBonusText = this.add.image(width / 2, (height / 2), "img", "back/GameBonusText.png");
        this.gameBonusText.setVisible(false);
        this.gameBonusText.setScale(SCALE);
        this.container = this.make.container({ x: 0, y: 0, add: true });
        this.add.existing(this.container);
        this.container.add(this.gameBonusText);

        this.gameBonusWin.image = this.add.image(width / 2, (height / 2), "img", "back/GameBonusWin.png");
        this.gameBonusWin.image.setVisible(false);
        // this.gameBonusWin.image.setOrigin(0);
        this.gameBonusWin.image.setScale(SCALE);
        this.container.add(this.gameBonusWin.image);


        this.gameBonusWin.text = new Phaser.GameObjects.DynamicBitmapText(this, width / 2 - 90 * SCALE, (height / 2) - 45 * SCALE, "roboto-bold", "ВЫИГРЫШ:", 25 * SCALE);
        this.gameBonusWin.text.setVisible(false);
        this.gameBonusWin.text.setScale(SCALE);
        this.container.add(this.gameBonusWin.text);

        this.gameBonusWin.sum = new Phaser.GameObjects.DynamicBitmapText(this, width / 2 - 60 * SCALE, (height / 2), "microstyle", "9999", 35 * SCALE);
        this.gameBonusWin.sum.setTint(0xF1F100, 0xF1F100, 0xF1F100, 0xF1F100);
        this.gameBonusWin.sum.setVisible(false);
        this.gameBonusWin.sum.setScale(SCALE);
        this.container.add(this.gameBonusWin.sum);


        // this.container.add(this.title);
        this.container.x = 0;
        this.container.y = 0;

        this.bandImage = this.add.graphics();

        // Load Buttons
        for (const p in data.buttons) {
            if (data.buttons.hasOwnProperty(p)) {
                // console.log(data.buttons[p].X, data.buttons[p].Y);
                const btn = this.add.image(data.buttons[p].X, data.buttons[p].Y, "buttons", data.buttons[p].NAME + ".png");
                btn.name = data.buttons[p].NAME; btn.setScale(data.buttons[p].ZOOM); btn.setOrigin(0); btn.setInteractive();
                switch (btn.name) {
                    case gameConfig.BUTTONS.SPIN.NAME: {
                        btn.on("pointerdown", this.gc.startPlay, this.gc);
                        break;
                    }
                    case gameConfig.BUTTONS.X2.NAME: {
                        btn.on("pointerdown", this.showRisk, this);
                        break;
                    }
                    case gameConfig.BUTTONS.MENU.NAME: {
                        btn.on("pointerdown", this.showSettings, this);
                        break;
                    }
                    case gameConfig.BUTTONS.AUTO.NAME: {
                        btn.on("pointerdown", this.startAutoGame, this);
                        break;
                    }
                    default: {

                        break;
                    }
                }
                this.buttons.push(btn);
            }
        }

        const btnSpin = this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.SPIN.NAME });
        if (btnSpin != null) {
            this.btnSpin = btnSpin;
        }

        const btnX2 = this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.X2.NAME });
        if (btnX2 != null) {
            this.btnX2 = btnX2;
        }

        const btnMenu = this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.MENU.NAME });
        if (btnMenu != null) {
            this.btnMenu = btnMenu;
        }
        const btnSettings = this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.SETTINGS.NAME });
        if (btnSettings != null) {
            this.btnSettings = btnSettings;
        }
        const btnMoreSettings = this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.MORE_SETTINGS.NAME });
        if (btnMoreSettings != null) {
            this.btnMoreSettings = btnMoreSettings;
        }
        const btnAuto = this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.AUTO.NAME });
        if (btnAuto != null) {
            this.btnAuto = btnAuto;
        }

        this.setBtnStatuses("main");
    }

    public freeGamesPrepare() {
        this.bandImage.setVisible(false);
        for (const e of this.edits) {
            e.setVisible(false);
        }
        for (const b of this.buttons) {
            b.setVisible(false);
        }
        for (const bt of this.bandTexts) {
            bt.setVisible(false);
        }
        this.gameBonusText.setVisible(true);
    }

    public enableAllElements() {
        this.bandImage.setVisible(true);
        for (const e of this.edits) {
            e.setVisible(true);
        }
        // for (const b of this.buttons) {
        //     b.setVisible(true);
        // }
        for (const bt of this.bandTexts) {
            bt.setVisible(true);
        }
        this.gameBonusText.setVisible(true);
    }

    public startAutoGame() {
        // if ((this.gc.isAutoGame() === false) && (this.gc.isWheelsKeepTurning === false)) {
        if (this.gc.isAutoGame() === false) {
            this.setBtnStatuses("alloff");
            this.gc.startAutoGame();
            this.gc.startPlay();
            console.log("auto game started");
        } else {
            this.gc.stopAutoGame();
            this.gc.time.removeAllEvents();
            this.time.removeAllEvents();
            this.setBtnStatuses("stopAuto");
            // this.gc.stopPlay();
            console.log("auto game stoped");
        }
    }
    // public drawRoundRect(rect) {
    //     const graphics = this.add.graphics();
    //     graphics.lineStyle(3 * SCALE, 0xffff00, 1);
    //     // graphics.strokeRoundedRect(rect.x, rect.y, rect.width, rect.height, rect.radius);
    //     console.log(rect);
    //     graphics.strokeRoundedRect(0 * SCALE, 242 * SCALE, 160 * SCALE, 160 * SCALE, 32 * SCALE);
    // }

    public showRisk() {
        // Включить риск сцену
        // if (this.btnX2SpinEvent !== undefined) {
        //     this.btnX2SpinEvent.remove(null);
        //     this.btnX2SpinEvent = undefined;
        // }
        this.tweens.killAll();
        this.time.removeAllEvents();
        this.gc.setDefault();
        this.gc.currentScene = GameScene.Risk;
        this.sc.playSound({ NAME: "ButtonBet", LOOP: false });
        this.scene.resume("risk");
        this.riskScene.start();
        console.log("risk");
        // this.riskScene.start();
        // this.hideToRisk();
        // this.showBottomLeftPanel(gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.NAME, this.prizeWinEdt);
        // this.showTopLeftPanel(gameConfig.IMAGES.PANEL_IMAGES.INPUT_WIN.NAME, this.inputWinEdt);
        // this.inputWinEdt.sum.setText(rightJustify(this.gc.currentTotalWin.toString(), 6, " "));
        // this.prizeWinEdt.sum.setText(rightJustify(this.gc.currentTotalWin.toString(), 6, " "));
    }

    public showDefault() {
        console.log("showDefault");
        this.scene.pause("jackpot");
        this.scene.pause("risk");
        this.scene.pause("bonus");
        this.scene.pause("super");
        // this.gc.isWheelsKeepTurning = false;
        this.tweens.killAll();
        this.showBonusWin(false);
        // this.time.addEvent({ delay: 250, callback: this.setBtnStatuses, callbackScope: this, repeat: 0, args: ["main"] });
        this.updateEdits();

        if ((this.gc.currentScene === GameScene.Main) || (this.gc.freeGame.autoEnabled === false)) {
            this.setBtnStatuses("main");
        }
        if ((this.gc.freeGame.autoEnabled === true) && (this.gc.currentScene === GameScene.Free)) {
            console.log("Автоигра была включена до фриспинов");
            this.gc.autoGame.started = true;
            this.gc.time.clearPendingEvents();
            this.gc.time.delayedCall(1000, this.gc.startPlay, [false], this.gc);
        }

        // this.setBtnStatuses("main");
        // if (this.gc.isAutoGame() === true) {
        // this.time.delayedCall(2000, this.gc.startPlay, [false], this);
        // }
    }

    public updateEdits() {
        this.balanceEdt.sum.setText(rightJustify(this.gc.totalCredit.toString(), 6, " "));
        this.linesEdt.sum.setText(rightJustify(this.gc.currentLine.value.toString(), 6, " "));
        this.winEdt.sum.setText(rightJustify(this.gc.currentTotalWin.toString(), 6, " "));
        this.betPerLineEdt.sum.setText(rightJustify(this.gc.currentBet.value.toString(), 6, " "));
        this.betTotalEdt.sum.setText(rightJustify(this.gc.currentBet.total.toString(), 6, " "));
    }

    public showSettings() {
        if (this.gc.currentScene === GameScene.Settings) {
            // Выключить сцену настроек
            this.gc.currentScene = GameScene.Main;
            this.settingsScene.stop();
        } else {
            // Включить сцену настроек
            this.gc.currentScene = GameScene.Settings;
            for (const p in this.edits) {
                if (this.edits.hasOwnProperty(p)) {
                    // this.edits[p].setVisible(false);
                }
            }
            this.settingsScene.start("settings");
        }
    }

    public setBtnStatuses(status) {
        function setBtnStatus(btn: Phaser.GameObjects.Sprite, enabled: boolean, visible: boolean) {
            if (enabled === false) {
                btn.disableInteractive();
                btn.setTint(0xF5F5F5);
                btn.setAlpha(0.6);
            }
            if (enabled === true) {
                btn.clearTint();
                btn.setAlpha(1.0);
                btn.setInteractive();
            }
            btn.setVisible(visible);
        }
        console.log("status = ", status);
        switch (status) {
            case "settings": {
                setBtnStatus(this.btnAuto, false, false);
                setBtnStatus(this.btnSpin, false, false);
                setBtnStatus(this.btnX2, false, false);
                break;
            }
            case "main": {
                setBtnStatus(this.btnSpin, true, true);
                setBtnStatus(this.btnX2, false, false);
                setBtnStatus(this.btnAuto, true, true);
                setBtnStatus(this.btnMenu, true, true);
                setBtnStatus(this.btnMoreSettings, true, true);
                break;
            }
            case "spin": {
                setBtnStatus(this.btnSpin, false, true);
                setBtnStatus(this.btnX2, false, false);
                setBtnStatus(this.btnAuto, true, true);
                setBtnStatus(this.btnMenu, false, true);
                setBtnStatus(this.btnMoreSettings, false, true);
                break;
            }
            case "alldis": {
                setBtnStatus(this.btnSpin, false, true);
                setBtnStatus(this.btnX2, false, false);
                setBtnStatus(this.btnAuto, false, false);
                setBtnStatus(this.btnMenu, false, true);
                setBtnStatus(this.btnMoreSettings, false, true);
                break;
            }
            case "alloff": {
                setBtnStatus(this.btnSpin, false, false);
                setBtnStatus(this.btnX2, false, false);
                setBtnStatus(this.btnAuto, false, false);
                setBtnStatus(this.btnMenu, false, false);
                setBtnStatus(this.btnMoreSettings, false, false);
                break;
            }
            case "x2enabled": {
                setBtnStatus(this.btnSpin, true, true);
                setBtnStatus(this.btnX2, true, true);
                break;
            }
            case "risk": {
                setBtnStatus(this.btnSpin, true, true);
                setBtnStatus(this.btnX2, false, false);
                setBtnStatus(this.btnAuto, false, false);
                setBtnStatus(this.btnMenu, false, true);
                setBtnStatus(this.btnMoreSettings, false, true);
                break;
            }
            default: {
                break;
            }
        }
    }

    public getGameObjectByTypeAndName(key) {
        switch (key.type) {
            case gameConfig.TYPES.BUTTON: {
                for (const btn in this.buttons) {
                    if (this.buttons.hasOwnProperty(btn)) {
                        if (this.buttons[btn].name === key.name) {
                            return this.buttons[btn];
                        }
                    }
                }
                break;
            }
            case (gameConfig.TYPES.EDIT): {
                for (const edit in this.edits) {
                    if (this.edits.hasOwnProperty(edit)) {
                        if (this.edits[edit].name === key.name) {
                            return this.edits[edit];
                        }
                    }
                }
                break;
            }
            default: {
                return null;
            }
        }
    }
    public setRotate(key, newSize) {
        if (key === ScreenMode.Landscape) {
            const x = GameConfig.CANVAS_SIZE.WIDTH / 1280;
            const y = GameConfig.CANVAS_SIZE.HEIGHT / 720;
            this.bandImage.clear();
            this.bandImage.fillStyle(0x000000, 0.45);
            this.bandImage.fillRect(0, 640 * SCALE, 1280 * SCALE, 480 * SCALE);

            this.balanceEdt.text.setPosition(gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 94 * SCALE, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 5 * SCALE);
            this.balanceEdt.sum.setPosition(this.balanceEdt.text.x - 45 * SCALE, this.balanceEdt.text.y + 15 * SCALE);

            this.linesEdt.text.setPosition(gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 340 * SCALE, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 5 * SCALE);
            this.linesEdt.sum.setPosition(this.linesEdt.text.x - 50 * SCALE, this.linesEdt.text.y + 15 * SCALE);

            this.winEdt.text.setPosition(gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 580 * SCALE, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 5 * SCALE);
            this.winEdt.sum.setPosition(this.winEdt.text.x - 35 * SCALE, this.winEdt.text.y + 15 * SCALE);

            this.betPerLineEdt.text.setPosition(gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 800 * SCALE, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 5 * SCALE);
            this.betPerLineEdt.sum.setPosition(this.betPerLineEdt.text.x + 5 * SCALE, this.betPerLineEdt.text.y + 15 * SCALE);

            this.betTotalEdt.text.setPosition(gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.X + 1100 * SCALE, gameConfig.IMAGES.PANEL_IMAGES.PRIZE_WIN.Y + 5 * SCALE);
            this.betTotalEdt.sum.setPosition(this.betTotalEdt.text.x - 45 * SCALE, this.betTotalEdt.text.y + 15 * SCALE);

            this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.SETTINGS.NAME }).setVisible(false);
            this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.AUTO.NAME }).setVisible(true);
            this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.MORE_SETTINGS.NAME }).setVisible(true);

            // console.log("setRotate Land");
            // for (const p in gameConfig.BUTTONS) {
            //     if (gameConfig.BUTTONS.hasOwnProperty(p)) {
            //         const btn =
            //             this.getGameObjectByTypeAndName({
            //                 type: gameConfig.TYPES.BUTTON,
            //                 name: gameConfig.BUTTONS[p].NAME
            //             });
            //         if (btn !== undefined) {
            //             btn.setOrigin(0.5, 0.5);
            //             btn.setPosition(gameConfig.BUTTONS[p].LANDSCAPE.X,
            //                 gameConfig.BUTTONS[p].LANDSCAPE.Y);
            //             console.log(btn.x, btn.y);
            //             btn.setScale(x, y);
            //         }
            //     }
            // }

            const centerX = GameConfig.CANVAS_SIZE.WIDTH / 2;
            const centerY = GameConfig.CANVAS_SIZE.HEIGHT / 2;

            if (this.btnSpin != null) {
                this.btnSpin.setOrigin(0.5, 0.5);
                this.btnSpin.setScale(SCALE);
                this.btnSpin.setPosition(centerX + 500 * SCALE, centerY);
            }
            if (this.btnMenu != null) {
                this.btnMenu.setOrigin(0.5, 0.5);
                this.btnMenu.setScale(SCALE);
                this.btnMenu.setPosition(centerX - 570 * SCALE, centerY - 290 * SCALE);
            }
            if (this.btnMoreSettings != null) {
                this.btnMoreSettings.setOrigin(0.5, 0.5);
                this.btnMoreSettings.setScale(SCALE);
                this.btnMoreSettings.setPosition(centerX - 612 * SCALE, centerY);
            }
        } else {
            this.bandImage.clear();
            this.bandImage.fillStyle(0x000000, 0.45);
            this.bandImage.fillRect(0, 570 * SCALE, 1280 * SCALE, 480 * SCALE);
            // Выигрыш
            this.winEdt.text.setPosition(1280 * SCALE / 2 - 100 * SCALE, 580 * SCALE);
            this.winEdt.sum.setPosition(this.winEdt.text.x + 50 * SCALE, this.winEdt.text.y + 15 * SCALE);

            // Линии
            this.linesEdt.text.setPosition(140 * SCALE, 630 * SCALE);
            this.linesEdt.sum.setPosition(this.linesEdt.text.x + 35 * SCALE, this.linesEdt.text.y + 15 * SCALE);

            // Ставка на линию
            this.betPerLineEdt.text.setPosition(470 * SCALE, this.linesEdt.text.y);
            this.betPerLineEdt.sum.setPosition(this.betPerLineEdt.text.x + 125 * SCALE, this.betPerLineEdt.text.y + 15 * SCALE);

            // Ставка
            this.betTotalEdt.text.setPosition(1000 * SCALE, this.linesEdt.text.y);
            this.betTotalEdt.sum.setPosition(this.betTotalEdt.text.x + 15 * SCALE, this.betTotalEdt.text.y + 15 * SCALE);

            // Баланс
            this.balanceEdt.text.setPosition(this.winEdt.text.x + 20 * SCALE, 675 * SCALE);
            this.balanceEdt.sum.setPosition(this.balanceEdt.text.x + 5 * SCALE, this.balanceEdt.text.y + 15 * SCALE);

            this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.SETTINGS.NAME }).setVisible(true);
            this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.AUTO.NAME }).setVisible(true);
            this.getGameObjectByTypeAndName({ type: gameConfig.TYPES.BUTTON, name: gameConfig.BUTTONS.MORE_SETTINGS.NAME }).setVisible(false);


            const centerX = GameConfig.CANVAS_SIZE.WIDTH / 2;
            const centerY = GameConfig.CANVAS_SIZE.HEIGHT / 2;

            // const scaleX = newSize.Width / 400 * SCALE;
            // const scaleY = newSize.Height / 700 * SCALE;

            const width = window.innerWidth;
            const height = window.innerHeight;

            const scaleX = window.devicePixelRatio * newSize.Width / width / 2;
            const scaleY = window.devicePixelRatio * newSize.Height / height / 2;

            /// console.log(width / height - 720 / 1280);

            // console.log(newSize);
            if (this.btnSpin != null) {
                this.btnSpin.setOrigin(0.5, 0.5);
                this.btnSpin.setScale(scaleX, scaleY);
                this.btnSpin.setPosition(centerX, centerY + 135 * SCALE);
            }
            if (this.btnMenu != null) {
                this.btnMenu.setOrigin(0.5, 0.5);
                this.btnMenu.setScale(scaleX, scaleY);
                this.btnMenu.setPosition(centerX - 510 * SCALE, centerY - 320 * SCALE);
            }
            if (this.btnSettings != null) {
                this.btnSettings.setOrigin(0.5, 0.5);
                this.btnSettings.setScale(scaleX, scaleY);
                this.btnSettings.setPosition(centerX - 400 * SCALE, centerY + 135 * SCALE);
            }
            if (this.btnAuto != null) {
                this.btnAuto.setOrigin(0.5, 0.5);
                this.btnAuto.setScale(scaleX, scaleY);
                this.btnAuto.setPosition(centerX + 400 * SCALE, centerY + 135 * SCALE);
            }
            // const centerX = GameConfig.CANVAS_SIZE.WIDTH / 2;
            // const centerY = GameConfig.CANVAS_SIZE.HEIGHT / 2;
            // console.log(centerX, centerY);

            // for (const p in gameConfig.BUTTONS) {
            //     if (gameConfig.BUTTONS.hasOwnProperty(p)) {
            //         const btn =
            //             this.getGameObjectByTypeAndName({
            //                 type: gameConfig.TYPES.BUTTON,
            //                 name: gameConfig.BUTTONS[p].NAME
            //             });
            //         if (btn !== undefined) {
            //             btn.setPosition(gameConfig.BUTTONS[p].PORTRAIT.X,
            //                 gameConfig.BUTTONS[p].PORTRAIT.Y);
            //             btn.setScale(SCALE * 1.7778, SCALE * 0.5625);
            //         }
            //     }
            // }
        }
        this.setTextScale(key);
    }
    public setBonusGame(key) {
        this.gameBonusText.setVisible(false);
        if (key === true) {
            this.bandImage.setVisible(key);
        }
        if (key === false) {
            this.btnSettings.setVisible(false);
            this.showBonusWin(true);
        }
        this.bonusCurrentGame.text.setVisible(key);
        this.bonusCurrentGame.sum.setVisible(key);
        this.bonusTotalWinEdt.text.setVisible(key);
        this.bonusTotalWinEdt.sum.setVisible(key);
    }

    public showBonusWin(key) {
        this.gameBonusWin.image.setVisible(key);
        this.gameBonusWin.text.setVisible(key);
        this.gameBonusWin.sum.setVisible(key);
    }

    public setBonusTotalWin(value) {
        this.gameBonusWin.sum.text = value.toString();
    }

    public setBonusWin(value) {
        this.bonusTotalWinEdt.sum.text = value.toString();
    }
    public setCurrentBonusGame(value, total) {
        this.bonusCurrentGame.sum.text = value.toString() + "/" + total.toString();
    }

    public onX2SpinEvent() {
        // console.log("event");
        if (this.btnX2SpinEvent.switch) {
            this.btnX2.setTint(0x354f77);
            // gameInt.btnX2.setVisible(true);
            // gameInt.btnSpin.setVisible(false);
            // gameInt.btnX2.setTexture("buttons", "ButDoubleWin.png");
            // gameInt.btnSpin.setTexture("buttons", "ButSpin.png");
        } else {
            this.btnX2.clearTint();
            // gameInt.btnX2.setVisible(false);
            // gameInt.btnSpin.setVisible(true);
            // gameInt.btnX2.setTexture("buttons", "ButDouble.png");
            // gameInt.btnSpin.setTexture("buttons", "ButSpinWin.png");
        }
        this.btnX2SpinEvent.switch = !this.btnX2SpinEvent.switch;
        // this.x2SwitchCount++;
        // if ((gameInt.gc.isAutoGame() === true) && (this.x2SwitchCount === 15)) {
        //     this.x2SwitchCount = 0;
        //     gameInt.time.delayedCall(0, this.spinUp, [false], this);
        // }
    }

    public startX2SpinEvent() {
        console.log("X2 started");
        gameInt.btnX2SpinEvent.started = true;
        gameInt.setBtnStatuses("x2enabled");

        gameInt.time.removeAllEvents();
        gameInt.time.clearPendingEvents();
        gameInt.btnX2SpinEvent.event = gameInt.time.addEvent({
            delay: 300, callback: gameInt.onX2SpinEvent, callbackScope: gameInt, loop: true
        });
    }

    public dischargeCoins() {
        if (gameInt.currWinDisplay.win !== 0) {
            if ((gameInt.currWinDisplay.progress) > 10) {
                if (gameInt.currWinDisplay.sound === false) {
                    gameInt.currWinDisplay.sound = true;
                }
                gameInt.currWinDisplay.discount += gameInt.currWinDisplay.count;
                gameInt.currWinDisplay.count++;
                if (gameInt.currWinDisplay.win <= gameInt.currWinDisplay.discount) {
                    gameInt.currWinDisplay.win = 0;
                } else {
                    gameInt.currWinDisplay.win -= gameInt.currWinDisplay.discount;
                    gameInt.currWinDisplay.credit += gameInt.currWinDisplay.discount;
                }
            } else {
                gameInt.currWinDisplay.win--;
                gameInt.currWinDisplay.credit++;
            }
            gameInt.winEdt.sum.setText(rightJustify(gameInt.currWinDisplay.win.toString(), 6, " "));
            gameInt.balanceEdt.sum.setText(rightJustify(gameInt.currWinDisplay.credit.toString(), 6, " "));
            gameInt.currWinDisplay.progress++;
        } else {
            if (gameInt.dischargeWinEvent !== undefined) {
                gameInt.gc.setDefault();
                gameInt.balanceEdt.sum.setText(rightJustify(gameInt.gc.totalCredit.toString(), 6, " "));
                gameInt.dischargeWinEvent.remove(null);
                gameInt.currWinDisplay.win = 0;
                gameInt.currWinDisplay.credit = 0;
                gameInt.currWinDisplay.count = 0;
                gameInt.currWinDisplay.discount = 1;
                gameInt.currWinDisplay.progress = 0;
                gameInt.currWinDisplay.sound = false;
                gameInt.gc.currentTotalWin = 0;
                if (gameInt.gc.freeGame.autoEnabled === false) {
                    gameInt.gc.currentScene = GameScene.Main;
                }
                gameInt.time.delayedCall(700, gameInt.showDefault, [], gameInt);
            }
        }
    }

    public discardWins() {
        console.log("discard wins");
        // gameInt.soundController.playSound({ NAME: gameConfig.SOUNDS.CREDITS_BIG.NAME, LOOP: true });
        gameInt.dischargeWinEvent = gameInt.time.addEvent({ delay: 66, callback: gameInt.dischargeCoins, repeat: -1, startAt: 0 });
    }


    private setTextScale(key) {
        for (const p in this.edits) {
            if (this.edits.hasOwnProperty(p)) {
                if (key === ScreenMode.Landscape) {
                    this.edits[p].setScale(1.0, 1.0);
                } else {

                    this.edits[p].setScale(1.778, 0.667);
                }
            }
        }
        for (const p in this.bandTexts) {
            if (this.bandTexts.hasOwnProperty(p)) {
                if (key === ScreenMode.Landscape) {
                    this.bandTexts[p].setScale(1.0, 1.0);
                } else {

                    this.bandTexts[p].setScale(1.778, 0.667);
                }
            }
        }
    }

}
