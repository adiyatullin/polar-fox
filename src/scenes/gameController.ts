import Drums from "../drums";
import gameConfig, { GameScene, WinCoeffs } from "../gameConfig";
import CheckIn from "../utils/checkIn";
import GameInterface from "./gameInterface";
import GraphicsScene from "./graphicsScene";
import MainScene from "./mainScene";
import RiskScene from "./riskScene";
import SoundController from "./soundController";

const SCALE = gameConfig.SCALE;
let gameInt: any;
let gameContr: GameController;

const nineLines = [
    // [
    //     [4, 12, 4, 5, 6],
    //     [1, 0, 1, 2, 12],
    //     [9, 1, 9, 0, 8]
    // ],
    // [
    //     [11, 11, 8, 7, 5],
    //     [7, 2, 1, 1, 11],
    //     [2, 1, 12, 0, 2],
    // ],
    // [
    //     [1, 1, 5, 1, 0],
    //     [1, 5, 1, 5, 0],
    //     [0, 1, 5, 1, 0],
    // ],

    // [
    //     [0, 0, 1, 0, 1],
    //     [0, 0, 1, 0, 0],
    //     [0, 0, 1, 0, 0],
    // ],

    [
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1],
    ],
    // [
    //     [5, 5, 7, 7, 1],
    //     [2, 7, 5, 3, 8],
    //     [3, 12, 2, 5, 12],
    // ],
    [
        [12, 6, 8, 1, 12],
        [0, 4, 1, 2, 3],
        [11, 11, 2, 8, 8],
    ],
    [
        [7, 7, 7, 1, 7],
        [6, 10, 2, 3, 4],
        [8, 4, 1, 5, 8],
    ],
    [
        [11, 8, 11, 4, 1],
        [4, 5, 4, 1, 8],
        [2, 2, 2, 11, 12],
    ],
    [
        [7, 7, 4, 5, 7],
        [1, 5, 7, 7, 1],
        [10, 3, 8, 8, 2],
    ],
    [
        [10, 1, 7, 12, 12],
        [8, 10, 4, 4, 6],
        [0, 11, 2, 7, 0],
    ],
    [
        [10, 10, 7, 3, 5],
        [5, 0, 10, 0, 6],
        [6, 3, 2, 5, 8],
    ],
    [
        [1, 1, 1, 8, 4],
        [12, 5, 7, 1, 10],
        [3, 4, 5, 5, 5],
    ],
    [
        [6, 8, 5, 2, 1],
        [3, 2, 2, 4, 5],
        [2, 1, 1, 11, 3],
    ],
    [
        [12, 3, 3, 8, 6],
        [2, 10, 7, 2, 5],
        [10, 12, 4, 0, 4],
    ],
    [
        [7, 1, 2, 1, 6],
        [2, 4, 1, 3, 11],
        [11, 7, 11, 2, 2]
    ],

    [
        [0, 1, 12, 2, 2],
        [0, 0, 0, 0, 12],
        [12, 8, 0, 1, 1],
    ],
    [
        [12, 3, 4, 6, 7],
        [0, 4, 0, 4, 3],
        [1, 2, 0, 2, 12],
    ],
    [
        [6, 4, 7, 11, 3],
        [4, 3, 8, 7, 8],
        [7, 5, 6, 0, 6],
    ],
    [
        [1, 3, 7, 7, 1],
        [2, 7, 11, 11, 8],
        [7, 12, 2, 0, 5],
    ],
    [
        [12, 6, 8, 7, 12],
        [1, 4, 11, 2, 1],
        [4, 1, 1, 1, 8],
    ],
    [
        [7, 7, 4, 7, 7],
        [1, 5, 6, 4, 1],
        [10, 10, 10, 10, 10],
    ],
    [
        [10, 1, 7, 12, 12],
        [8, 10, 4, 4, 6],
        [0, 11, 2, 7, 0],
    ],
    [
        [7, 10, 7, 3, 5],
        [5, 0, 4, 0, 6],
        [6, 3, 2, 5, 8],
    ],
    [
        [1, 3, 7, 4, 8],
        [8, 8, 2, 11, 12],
        [0, 7, 5, 5, 1],
    ],
    [
        [11, 4, 4, 6, 12],
        [1, 2, 8, 1, 2],
        [7, 7, 3, 4, 4],
    ],
    [
        [3, 8, 2, 2, 1],
        [1, 7, 12, 8, 12],
        [6, 3, 1, 11, 2],
    ],
    [
        [1, 2, 4, 3, 12],
        [6, 0, 0, 4, 5],
        [5, 5, 6, 11, 8],
    ],
    [
        [7, 1, 1, 10, 3],
        [8, 6, 4, 2, 5],
        [11, 2, 5, 8, 4],
    ],
    [
        [2, 4, 7, 6, 2],
        [11, 12, 8, 1, 0],
        [7, 0, 3, 7, 4],
    ],
    [
        [6, 4, 2, 10, 5],
        [11, 1, 5, 1, 8],
        [3, 3, 3, 11, 12],
    ],
    [
        [11, 11, 8, 7, 5],
        [7, 2, 1, 1, 11],
        [2, 1, 12, 0, 2],
    ],
    [
        [2, 6, 6, 1, 7],
        [7, 8, 1, 3, 4],
        [5, 5, 2, 4, 11],
    ],
    [
        [7, 3, 8, 2, 4],
        [8, 0, 3, 3, 0],
        [2, 2, 2, 8, 10],
    ],
    [
        [7, 12, 8, 12, 4],
        [2, 11, 11, 4, 8],
        [5, 1, 5, 3, 10],
    ],
    [
        [6, 1, 1, 8, 4],
        [1, 2, 0, 4, 6],
        [4, 12, 10, 2, 11],
    ],
    [
        [3, 10, 1, 3, 7],
        [7, 5, 8, 1, 2],
        [8, 6, 3, 7, 0],
    ],
    [
        [1, 7, 1, 1, 5],
        [4, 6, 0, 8, 7],
        [3, 3, 3, 6, 4],
    ],
    [
        [1, 10, 1, 8, 4],
        [5, 8, 8, 2, 12],
        [2, 2, 4, 12, 8],
    ],
    [
        [4, 2, 4, 4, 3],
        [12, 4, 6, 6, 6],
        [0, 1, 0, 7, 2],
    ],
    [
        [5, 3, 6, 4, 4],
        [2, 2, 1, 11, 5],
        [0, 1, 0, 3, 3],
    ],
    [
        [2, 11, 2, 8, 3],
        [7, 1, 4, 2, 7],
        [3, 7, 7, 5, 4],
    ],
    [
        [10, 10, 2, 3, 5],
        [7, 2, 5, 5, 3],
        [4, 6, 3, 12, 7],
    ]
];

const defLines = [
    [
        // [7, 11, 1, 12, 6],
        // [1, 5, 7, 2, 12],
        // [10, 8, 10, 0, 8]
        // [4, 11, 4, 5, 6],
        // [1, 1, 1, 2, 12],
        // [9, 9, 9, 0, 8]
        [4, 12, 4, 5, 6],
        [1, 0, 1, 2, 12],
        [9, 1, 9, 0, 8]
    ],

    [
        [1, 1, 2, 2, 2],
        [0, 11, 1, 11, 7],
        [2, 8, 6, 1, 1],
    ],
    [
        [8, 3, 4, 6, 7],
        [12, 4, 12, 4, 3],
        [11, 2, 0, 2, 2],
    ],
    [
        [6, 4, 7, 11, 3],
        [4, 3, 8, 7, 8],
        [7, 5, 6, 0, 6],
    ],
    [
        [1, 3, 7, 7, 1],
        [2, 7, 11, 11, 8],
        [7, 12, 2, 0, 5],
    ],
    [
        [12, 6, 8, 7, 12],
        [1, 4, 11, 2, 1],
        [4, 1, 1, 1, 8],
    ],
    [
        [4, 1, 8, 1, 7],
        [12, 12, 2, 3, 4],
        [0, 0, 1, 5, 8],
    ],
    [
        [10, 10, 10, 2, 1],
        [4, 6, 4, 1, 8],
        [2, 2, 2, 3, 11],
    ],
    [
        [7, 7, 4, 7, 7],
        [1, 5, 6, 4, 1],
        [10, 10, 10, 10, 10],
    ],
    [
        [10, 1, 7, 12, 12],
        [8, 10, 4, 4, 6],
        [0, 11, 2, 7, 0],
    ],
    [
        [7, 10, 7, 3, 5],
        [5, 0, 4, 0, 6],
        [6, 3, 2, 5, 8],
    ]
];

let readableStates = nineLines;

const gameControllerConfig = {
    key: "gamecontroller",
    height: gameConfig.CANVAS_SIZE.HEIGHT / 2,
    width: gameConfig.CANVAS_SIZE.WIDTH / 2
};

export default class GameController extends Phaser.Scene {
    public graphics: Phaser.GameObjects.Graphics;
    public drums: Drums;
    public currentScene: GameScene;
    public currentBet: { index: number; value: number; total: number }; // Ставка
    public bonusWin: number;
    public currentTotalWin: number; // Выигрыш
    public currMap: integer;
    public totalCredit: number;
    public superWin: boolean;
    public superSum: number;
    public currentLine: { index: number; value: number };
    public container: Phaser.GameObjects.Container;
    public currentGameMode: { NAME: string; SCALE: number; BUTTONS: {}; };
    public mainWindow: { x: number; y: number; width: number; height: number; inner_window: { x: number; y: number; width: number; height: number; }; };
    public main: MainScene;


    public winLines = [];

    public animLine: {
        event: Phaser.Time.TimerEvent,
        currIndex: number,
        realIndex: number
    };

    public scatter: {
        event: Phaser.Time.TimerEvent,
        animIndex: number,
        length: number
    };

    public riskGames = [];

    public scatters = [];

    public helmet: { ENABLED: boolean; BET: number; TEMP_REMOVED: boolean };
    public isWheelsKeepTurning: boolean;
    public autoGame: {
        started: boolean;
    };

    public freeGame: {
        currentGame: number;
        totalGames: number;
        currentWin: number;
        totalWin: number;
        started: boolean;
        autoEnabled: boolean;
    };

    private checkIn: CheckIn;
    private scattersCount: number;
    private jackpotCount: number;
    private soundController: SoundController;
    private monroCount: number;
    private isMobile: boolean;

    constructor() {
        super(gameControllerConfig);
        // TO-DO - загрузка с сервера
        this.currentGameMode = gameConfig.GAME_MODES.VEKSEL;
        this.currentLine = { index: gameConfig.LINE_INDEX, value: gameConfig.LINES[gameConfig.LINE_INDEX] };
        this.currentBet = {
            index: gameConfig.BET_INDEX, value: gameConfig.BETS[gameConfig.BET_INDEX],
            total: this.currentLine.value * gameConfig.BETS[gameConfig.BET_INDEX]
        };
        this.helmet = { ENABLED: false, BET: gameConfig.BONUS_KASKA_BET, TEMP_REMOVED: false };
        if (this.currentBet.total > gameConfig.BONUS_KASKA_BET) {
            this.helmet.ENABLED = true;
        }
        this.autoGame = { started: false };
        this.totalCredit = 50000;

        this.freeGame = {
            currentGame: 0,
            totalGames: 0,
            currentWin: 0,
            totalWin: 0,
            started: false,
            autoEnabled: false
        };
        for (let i = 0; i < 6; i++) {
            this.riskGames.push(-1);
        }
        // this.createReadableStates();
    }
    public isMobileVersion() {
        return this.isMobile;
    }
    public isAutoGame() {
        return gameContr.autoGame.started;
    }
    public startAutoGame() {
        gameContr.autoGame.started = true;
    }
    public stopAutoGame() {
        gameContr.time.removeAllEvents();
        gameContr.autoGame.started = false;
    }
    public setViewPort(x, y, width, height, isPortrait, isIPad) {
        // x = x - 20 * SCALE;
        // width = width + 40 * SCALE;

        const gs = (this.scene.get("graphicsscene") as GraphicsScene);
        this.graphics = gs.graphics;


        if (isPortrait === true) {
            if (isIPad !== true) {
                this.cameras.main.setViewport(x, y, width - 250 * SCALE, height - 552 * SCALE);
                // this.drums.reelContainer.setScale(1.06, 0.35);
                this.drums.reelContainer.setScale(1.06, 0.35);
            } else {
                const WIDTH = gameConfig.REELS.COUNT * gameConfig.REELS.WIDTH * SCALE + 30 * SCALE;
                const HEIGHT = 1.49 * SCALE * (gameConfig.SYMBOLS.SIZE + gameConfig.REELS.CONTAINER.GAP_Y);
                this.cameras.main.setViewport(x, y, WIDTH, HEIGHT);
                // this.drums.reelContainer.setScale(1.055, 0.5);
                this.drums.reelContainer.setScale(1.06, 0.5);
            }
        } else {
            const WIDTH = gameConfig.REELS.COUNT * gameConfig.REELS.WIDTH * SCALE;
            const HEIGHT = 3 * (gameConfig.SYMBOLS.SIZE + gameConfig.REELS.CONTAINER.GAP_Y) * SCALE;
            // const HEIGHT = 3 * (gameConfig.SYMBOLS.SIZE) * SCALE + 3 * gameConfig.REELS.CONTAINER.GAP_Y;
            this.drums.reelContainer.setScale(1.01, 1);
            this.cameras.main.setViewport(x, y, WIDTH, HEIGHT);
        }
        console.log(this.scene);
        if (gs.graphics !== undefined) {
            this.graphics = gs.graphics;
            this.graphics.setPosition(this.cameras.main.x, this.cameras.main.y);



            console.log("this.cameras.main");

        }
    }
    public checkScatter(stateCalc) {
        let scatterSum = 0;
        for (const s of stateCalc) {
            if (s === 12) {
                scatterSum++;
            }
        }
        return scatterSum;
    }
    public checkJackPot(stateCalc) {
        let jackpotSum = 0;
        for (const s of stateCalc) {
            if (s === 9) {
                jackpotSum++;
            }
        }
        return jackpotSum;
    }
    public create(data) {
        gameContr = this;

        this.scatter = {
            event: null,
            animIndex: 0,
            length: 0
        };

        this.animLine = {
            event: null,
            currIndex: 0,
            realIndex: 0
        };

        // this.graphics = this.add.graphics();

        this.scattersCount = 0;
        this.jackpotCount = 0;
        this.currentTotalWin = 0;
        this.bonusWin = 0;
        const soundController = this.scene.get("soundcontroller");
        if (soundController instanceof SoundController === false) {
            // console.log("Error! soundScene not found");
        } else {
            this.soundController = soundController as SoundController;
        }
        // this.setViewPort(0, 0, 1.0, 1.0);
        this.container = this.add.container(0, 0);
        this.container.x = 0;
        this.container.y = 0;
        gameContr.currMap = -1;
        this.checkIn = new CheckIn();
        this.isWheelsKeepTurning = false;
        this.isMobile = data.mobile;
        this.drums = new Drums(this, gameConfig.REELS.WIDTH, gameConfig.SYMBOLS.SIZE, gameConfig.SYMBOLS.COUNT, gameConfig.REELS.COUNT,
            gameConfig.REELS.CONTAINER.GAP_Y, gameConfig.SCALE);
        this.drums.initialize(data);
        const randomInitState = this.drums.initStates[Math.floor(Math.random() * this.drums.initStates.length)];
        // console.log(randomInitState);
        this.drums.setTextures(randomInitState);
        this.container.add(this.drums);
        // console.log("create gameController");

        const mainScene = this.scene.get("main");
        if (mainScene instanceof MainScene) {
            this.main = mainScene;
        }
    }
    public update() {
        this.drums.update();
    }
    public drawRoundRect(rectangle, line) {
        // const graphics = gameContr.add.graphics();
        // graphics.lineStyle(3 * SCALE, 0xffff00, 1);
        const lineWidth = 4 * SCALE;
        switch (line) {
            case 1: {
                this.graphics.lineStyle(lineWidth, 0x4286f4, 1);
                break;
            }
            case 2: {
                this.graphics.lineStyle(lineWidth, 0xef150e, 1);
                break;
            }
            case 3: {
                this.graphics.lineStyle(lineWidth, 0x02ff0a, 1);
                break;
            }
            case 4: {
                this.graphics.lineStyle(lineWidth, 0xf6ff00, 1);
                break;
            }
            case 5: {
                this.graphics.lineStyle(lineWidth, 0xff00dc, 1);
                break;
            }
            case 6: {
                this.graphics.lineStyle(lineWidth, 0x00ffbf, 1);
                break;
            }
            case 7: {
                this.graphics.lineStyle(lineWidth, 0xffffff, 1);
                break;
            }
            case 8: {
                this.graphics.lineStyle(lineWidth, 0x00faff, 1);
                break;
            }
            case 9: {
                this.graphics.lineStyle(lineWidth, 0xffae00, 1);
                break;
            }
            case 12: {
                this.graphics.lineStyle(lineWidth, 0xe5c822, 1);
                break;
            }
        }

        // this.graphics.strokeRoundedRect(rectangle[0].x, rectangle[0].y - 3 * SCALE,
        //     rectangle[0].width * SCALE + 2 * SCALE, rectangle[0].height * SCALE + 2 * SCALE,
        //     rectangle[0].radius * SCALE);
        this.graphics.strokeRect(rectangle[0].x, rectangle[0].y - 3 * SCALE,
            rectangle[0].width * SCALE + 2 * SCALE, rectangle[0].height * SCALE + 2 * SCALE);
    }

    public showWinLines() {
        // console.log("showWinLines");
        const gi = (this.scene.get("gameinterface") as GameInterface);


        gameContr.graphics.clear();
        gameContr.dropShadows();
        if ((gameContr.scattersCount > 1)) {
            for (const r of gameContr.drums.reels) {
                for (const s of r.symbols) {
                    if (s.name === 12) {
                        s.clearTint();
                        const imRect = [];
                        imRect.push({ x: gameContr.getReelXPos(r.reelIndex), y: s.y + 1.3 * SCALE, width: s.width + 2 * SCALE, height: s.height + 1.3 * SCALE });
                        gameContr.drawRoundRect(imRect, 12);
                    }
                }
            }
        }
        if (gameContr.animLine.currIndex >= gameContr.winLines.length) {
            gameContr.animLine.currIndex = 0;
            if ((gameContr.scattersCount < 3)) {
                if (gameContr.isAutoGame() === true) {
                    gameContr.time.clearPendingEvents();
                    gameContr.time.delayedCall(0, gameContr.startPlay, [false], gameContr);
                }
            } else {
                if (gameContr.freeGame.started === false) {
                    gameContr.freeGame.started = true;
                    gameContr.freeGame.autoEnabled = gameContr.isAutoGame();
                }
                console.log("gameContr.freeGame.autoEnabled", gameContr.freeGame.autoEnabled);
                gameContr.time.delayedCall(0, gameContr.preloadFreeGames, [false], gameContr);
            }
            if ((gameContr.freeGame.currentGame === (gameContr.freeGame.totalGames)) && (gameContr.freeGame.currentGame !== 0)) {
                // console.log("Стоп машина!");
                console.log("Завершение фригеймов. Стоп всех ивентов....");
                gameContr.time.clearPendingEvents();
                gameContr.time.removeAllEvents();
                gameContr.freeGame.started = false;
                gameContr.stopAutoGame();
                gi.setBonusGame(false);
                gi.showBonusWin(true);
                gi.setBonusTotalWin(gameContr.currentTotalWin);
                gameContr.graphics.clear();
                for (const r of gameContr.drums.reels) {
                    for (const s of r.symbols) {
                        if (s.name !== "") {
                            s.setTexture("anims", s.name + "/1.png");
                            // console.log(s.name + "/1.png");
                        }
                    }
                }
                gameContr.time.delayedCall(2000, gameContr.stopFreeGames, [false], gameContr);
            }
        }
        let j = 0;
        gameContr.winLines.forEach((element) => {
            if ((j === gameContr.animLine.currIndex)) {
                gameContr.drawWinLine(element.line, element.count);
            }
            j++;
        });
        gameContr.animLine.currIndex++;
        // gameContr.animLine.realIndex++;
        // if (gameContr.animLine.realIndex >= gameContr.winLines.length) {
        //     console.log("Last Frame");
        //     gameContr.animLine.realIndex = 0;
        //     if (gameContr.winLines.length > 0) {

        //     }
        // }
    }

    public getReelXPos(reelIndex) {
        let reel = null;
        let i = 0;
        gameContr.drums.reels.forEach((element) => {
            if (element.reelIndex === reelIndex) {
                reel = element.container.x - 2 * SCALE + (i) * (SCALE);
            }
            i = i + 1.85;
        });
        return reel;
    }

    public onTilEvent(imageForAnim, tilEvent) {
        if (tilEvent !== null) {
            if (tilEvent.animIndex >= tilEvent.animLength) {
                tilEvent.animIndex = 0;
            }
            tilEvent.animIndex++;
            imageForAnim.setTexture("anims", tilEvent.name + "/" + tilEvent.animIndex + ".png");
        }
    }

    public onScatterEvent() {
        if (gameContr.scatter.animIndex >= gameContr.scatter.length) {
            gameContr.scatter.animIndex = 0;
            if (gameContr.isAutoGame() === true) {
                //
            }
        }
        gameContr.scatter.animIndex++;
        for (const r of gameContr.drums.reels) {
            for (const s of r.symbols) {
                if (s.name === 12) {
                    // s.clearTint();
                    s.setTexture("anims", "12" + "/" + gameContr.scatter.animIndex + ".png");
                    const imRect = [];
                    // imRect.push({ x: this.getReelXPos(r.reelIndex), y: s.y, width: s.width + 2 * SCALE, height: s.height + 1.3 * SCALE });
                    // gameContr.drawRoundRect(imRect, 12);
                }
            }
        }
    }

    public intersection(line1, line2) {

        let l1: Phaser.Curves.Line;
        let l2: Phaser.Curves.Line;

        let start1: Phaser.Math.Vector2;
        let end1: Phaser.Math.Vector2;
        let start2: Phaser.Math.Vector2;
        let end2: Phaser.Math.Vector2;
        let dir1: Phaser.Math.Vector2;
        let dir2: Phaser.Math.Vector2;
        // tslint:disable-next-line:variable-name
        let out_intersection: Phaser.Math.Vector2;

        let a1: number; let a2: number;
        let b1: number; let b2: number;
        let d1: number; let d2: number;
        // tslint:disable-next-line:variable-name
        let seg1_line2_start: number;
        // tslint:disable-next-line:variable-name
        let seg1_line2_end: number;
        // tslint:disable-next-line:variable-name
        let seg2_line1_start: number;
        // tslint:disable-next-line:variable-name
        let seg2_line1_end: number;
        let u: number;
        // tslint:disable-next-line:prefer-const
        let m: Phaser.Math.Vector2;

        l1 = line1;
        l2 = line2;

        start1 = l1.p0;
        end1 = l1.p1;
        start2 = l2.p0;
        end2 = l2.p1;

        dir1 = end1.subtract(start1);
        dir2 = end2.subtract(start2);

        // считаем уравнения прямых проходящих через отрезки
        a1 = -dir1.y;
        b1 = +dir1.x;
        d1 = -(a1 * start1.x + b1 * start1.y);

        a2 = -dir2.y;
        b2 = +dir2.x;
        d2 = -(a2 * start2.x + b2 * start2.y);

        // подставляем концы отрезков, для выяснения в каких полуплоскотях они
        seg1_line2_start = a2 * start1.x + b2 * start1.y + d2;
        seg1_line2_end = a2 * end1.x + b2 * end1.y + d2;

        seg2_line1_start = a1 * start2.x + b1 * start2.y + d1;
        seg2_line1_end = a1 * end2.x + b1 * end2.y + d1;

        // если концы одного отрезка имеют один знак, значит он в одной полуплоскости и пересечения нет.
        if ((seg1_line2_start * seg1_line2_end >= 0) || (seg2_line1_start * seg2_line1_end >= 0)) {
            return null;
        }
        u = seg1_line2_start / (seg1_line2_start - seg1_line2_end);
        // tslint:disable-next-line:prefer-const
        m.x *= u;
        m.y *= u;
        out_intersection = start1.add(m);
        return out_intersection;
    }

    public getLineCoeff(x1, y1, x2, y2) {
        let line;
        const a = y1 - y2;
        const b = x2 - x1;
        const c = x1 * y2 - x2 * y1;
        line = { A: a, B: b, C: c };
        return line;
    }

    public getCrossPoints(rect1, rect2, line) {
        function getPoint(line1, line2) {
            let point = null;
            const X = - (line1.C * line2.B - line2.C * line1.B) / (line1.A * line2.B - line2.A * line1.B);
            const Y = - (line1.A * line2.C - line2.A * line1.C) / (line1.A * line2.B - line2.A * line1.B);
            if ((isNaN(X) === false) && (isNaN(Y) === false)) {
                point = {
                    x: X, y: Y
                };
            }
            return point;
        }
        let crosspoints;
        const r2x1 = rect2.x;
        const r2y1 = rect2.y;
        const r2x2 = rect2.x;
        const r2y2 = rect2.y + rect2.height * SCALE;
        // const r1x1 = rect1.x + rect1.width;
        // const r1y1 = rect1.y;
        // const r1x2 = rect1.x + rect1.width;
        // const r1y2 = rect1.y + rect1.height;
        const r1x1 = rect2.x - 33 * SCALE;
        const r1y1 = rect1.y;
        const r1x2 = rect2.x - 33 * SCALE;
        const r1y2 = rect1.y + (rect1.height * SCALE);
        const l1 = this.getLineCoeff(r2x1, r2y1, r2x2, r2y2);
        const l2 = this.getLineCoeff(r1x1, r1y1, r1x2, r1y2);
        const p1 = getPoint(l1, line);
        const p2 = getPoint(l2, line);
        return crosspoints = { point1: p1, point2: p2 };
    }

    public drawLinePath(images2, images, line) {
        // tslint:disable-next-line:new-parens
        let path: Phaser.Curves.Path;
        const lineWidth = 4 * SCALE;

        const graphScene = this.scene.get("graphicsscene");
        if (graphScene instanceof GraphicsScene) {
            graphScene.clearTintForLine(line);
        }

        switch (line) {
            case 1: {
                this.graphics.lineStyle(lineWidth, 0x4286f4, 1);
                break;
            }
            case 2: {
                this.graphics.lineStyle(lineWidth, 0xef150e, 1);
                break;
            }
            case 3: {
                this.graphics.lineStyle(lineWidth, 0x02ff0a, 1);
                break;
            }
            case 4: {
                this.graphics.lineStyle(lineWidth, 0xf6ff00, 1);
                break;
            }
            case 5: {
                this.graphics.lineStyle(lineWidth, 0xff00dc, 1);
                break;
            }
            case 6: {
                this.graphics.lineStyle(lineWidth, 0x00ffbf, 1);
                break;
            }
            case 7: {
                this.graphics.lineStyle(lineWidth, 0xffffff, 1);
                break;
            }
            case 8: {
                this.graphics.lineStyle(lineWidth, 0x00faff, 1);
                break;
            }
            case 9: {
                this.graphics.lineStyle(lineWidth, 0xffae00, 1);
                break;
            }
        }

        const allRects = [];
        images.forEach((element) => {
            const rect = [];
            rect.push({ x: this.getReelXPos(element.reel), y: element.image.y, width: element.image.width, height: element.image.height, radius: 10 });
            allRects.push(rect);
        });

        const rects = [];
        images2.forEach((element) => {
            const rect = [];
            rect.push({ x: this.getReelXPos(element.reel), y: element.image.y, width: element.image.width, height: element.image.height, radius: 10 });
            rects.push(rect);
        });
        for (let i = 0; i < images2.length; i++) {
            if ((i + 1) < images2.length) {
                const x1 = rects[i][0].x + (rects[i][0].width / 2) * SCALE;
                const y1 = rects[i][0].y + (rects[i][0].height / 2) * SCALE;
                const x2 = rects[i + 1][0].x + (rects[i + 1][0].width / 2) * SCALE;
                const y2 = rects[i + 1][0].y + (rects[i + 1][0].height / 2) * SCALE;
                const lineTo = gameContr.getLineCoeff(x1, y1, x2, y2);
                const cp = this.getCrossPoints(rects[i][0], rects[i + 1][0], lineTo);
                this.graphics.lineBetween(cp.point1.x, cp.point1.y, cp.point2.x, cp.point2.y);
            }
        }
        for (let i = images2.length; i < images.length; i++) {
            const element = images[i];
            if (i === images2.length) {
                const x1 = rects[images2.length - 1][0].x + (rects[images2.length - 1][0].width / 2) * SCALE;
                const y1 = rects[images2.length - 1][0].y + (rects[images2.length - 1][0].height / 2) * SCALE;
                const rect2 = [];
                rect2.push({ x: this.getReelXPos(element.reel), y: element.image.y, width: element.image.width, height: element.image.height, radius: 10 });
                const x2 = rect2[0].x + (rect2[0].width / 2) * SCALE;
                const y2 = rect2[0].y + (rect2[0].height / 2) * SCALE;
                const lineTo = gameContr.getLineCoeff(x1, y1, x2, y2);
                const cp = this.getCrossPoints(rects[images2.length - 1][0], rect2[0], lineTo);
                this.graphics.lineBetween(cp.point2.x, cp.point2.y, x2, y2);
            }
            const rect = [];
            rect.push({ x: this.getReelXPos(element.reel), y: element.image.y, width: element.image.width, height: element.image.height, radius: 10 });
            if (path === undefined) {
                path = new Phaser.Curves.Path(rect[0].x + (rect[0].width / 2) * SCALE, rect[0].y + (rect[0].height / 2) * SCALE);
            } else {
                path.lineTo(rect[0].x + (rect[0].width / 2) * SCALE, rect[0].y + (rect[0].height / 2) * SCALE);
            }
        }
        if (path !== undefined) {
            path.draw(this.graphics);
        }

        let yOffset = allRects[0][0].height / 2;
        if (line === 4) {
            yOffset -= 65;
        }
        if (line === 5) {
            yOffset += 65;
        }
        if (line === 6) {
            yOffset -= 65;
        }
        if (line === 7) {
            yOffset += 65;
        }
        if (line === 8) {
            yOffset += 65;
        }
        if (line === 9) {
            yOffset -= 65;
        }
        {
            let x1 = allRects[0][0].x;
            let y1 = allRects[0][0].y + yOffset * SCALE;
            let x2 = allRects[0][0].x - 40 * SCALE;
            let y2 = allRects[0][0].y + yOffset * SCALE;
            if (line === 8) {
                y1 -= 120 * SCALE;
                y2 -= 120 * SCALE;
            }
            if (line === 9) {
                y1 += 120 * SCALE;
                y2 += 120 * SCALE;
            }
            // Первая линия от цифра слева
            this.graphics.lineBetween(x1, y1, x2, y2);

            x1 = allRects[4][0].x + (gameConfig.SYMBOLS.SIZE + 3) * SCALE;
            y1 = allRects[4][0].y + yOffset * SCALE;
            x2 = allRects[4][0].x + (gameConfig.SYMBOLS.SIZE + 3) * SCALE + 40 * SCALE;
            y2 = allRects[4][0].y + yOffset * SCALE;
            if (line === 8) {
                y1 -= 7 * SCALE;
                y2 -= 7 * SCALE;
            }
            if (line === 9) {
                y1 += 11 * SCALE;
                y2 += 11 * SCALE;
            }
            // Последняя линия к цифрам справа
            this.graphics.lineBetween(x1, y1, x2, y2);

            if (allRects.length > rects.length) {
                x1 = allRects[4][0].x + (gameConfig.SYMBOLS.SIZE - allRects[4][0].width / 2) * SCALE;
                y1 = allRects[4][0].y + allRects[4][0].height / 2 * SCALE;
                x2 = allRects[4][0].x + (gameConfig.SYMBOLS.SIZE + 3) * SCALE;
                y2 = allRects[4][0].y + yOffset * SCALE;
                if (line === 8) {
                    y2 -= 7 * SCALE;
                }
                if (line === 9) {
                    y2 += 11 * SCALE;
                }
                // Предпоследняя линия от центра последнего тила к последней линии (справа - идет к цифрам справа)
                this.graphics.lineBetween(x1, y1, x2, y2);
            }
        }

        this.graphics.lineStyle(10, 0x4286f4, 1);

        this.graphics.setDepth(10);
    }

    public drawWinLine(line, count) {
        if (line !== 0) {
            gameContr.graphics.clear();
            gameContr.dropShadows();
            const images = gameContr.drums.getLineImages(line);
            let i = 0;
            const images2 = [];
            images.forEach((element) => {
                if (i < count) {
                    const rect = [];
                    // rect.push({ x: this.getReelXPos(element.reel), y: element.image.y, width: element.image.width, height: element.image.height });
                    rect.push({ x: this.getReelXPos(element.reel), y: element.image.y + 1.3 * SCALE, width: element.image.width + 2 * SCALE, height: element.image.height + 1.3 * SCALE });
                    element.image.setBlendMode(Phaser.BlendModes.ADD);
                    element.image.clearTint();
                    gameContr.drawRoundRect(rect, line);
                    images2.push(element);
                }
                i++;
            });
            gameContr.drawLinePath(images2, images, line);
        }
        // path.draw(this.graphics);
    }

    public getAnimLength(i) {
        switch (i) {
            case 0: // 9
            case 1: // 10
            case 2: // J
            case 3: // Q
            case 4: // K
            case 5: // A
                {
                    return 17;
                }
            case 6: // Ежик
                {
                    return 39;
                }
            case 7: // Барсик
                {
                    return 39;
                }
            case 8: // Заяц
                {
                    return 40;
                }
            case 9: // Сова
                {
                    return 41;
                }
            case 10: // Песец
                {
                    return 36;
                }
            case 11: // Лиса
                {
                    return 37;
                }
            case 12: // Лес
                {
                    return 41;
                }
        }
    }

    public unique(arr) {
        const obj = {};
        arr.forEach((element) => {
            obj[element] = true; // запомнить строку в виде свойства объекта
        });
        return Object.keys(obj); // или собрать ключи перебором для IE8-
    }

    public dropShadows() {
        const mainScene = this.scene.get("main");
        if (mainScene instanceof MainScene) {
            this.main = mainScene;
        }
        this.main.backgroundMain.setTint(0x354f77);
        for (const r of gameContr.drums.reels) {
            for (const s of r.symbols) {
                s.setTint(0x354f77);
            }
        }
        const graphScene = this.scene.get("graphicsscene");
        if (graphScene instanceof GraphicsScene) {
            graphScene.tintAllDigits();
        }
    }

    public startTilEvents() {
        gameContr.currentTotalWin = 0;
        gameContr.winLines.forEach((element) => {
            // console.log("Линия " + (element.line) + ", Элемент - " + (element.til) + " встречается " + element.count + " раза." + ". Выигрыш: " + element.win);
            gameContr.currentTotalWin += element.win * gameContr.currentBet.value;
        });
        const strs = [];
        gameContr.winLines.forEach((element) => {
            const images = gameContr.drums.getLineImages(element.line);
            let j = 0;
            images.forEach((image) => {
                if (j < element.count) {
                    strs.push(image.reel + "," + image.line);
                }
                j++;
            });
        });
        const uStrings = gameContr.unique(strs);
        gameContr.scatters = [];
        let imCount = 0;
        uStrings.forEach((uStr) => {
            const indexs = uStr.split(",");
            const image = gameContr.drums.getImage(Number(indexs[0]), Number(indexs[1]));
            const tilEvent = {
                event: null,
                name: image.name,
                animIndex: 0,
                animLength: 0,
                index: imCount++
            };
            tilEvent.animLength = gameContr.getAnimLength(image.name);
            gameContr.scatters.push(tilEvent);
            tilEvent.event = gameContr.time.addEvent({
                delay: 75, callback: gameContr.onTilEvent, callbackScope: gameContr, loop: true, paused: false, args: [image, tilEvent]
            });
        });
        gameInt = gameContr.scene.get("gameinterface");
        console.log("Общий выигрыш по линиям: " + gameContr.currentTotalWin);
        gameInt.updateEdits();
    }

    public startScatterEvents() {
        if (gameContr.scattersCount > 1) {
            gameContr.main.backgroundMain.setTint(0x354f77);
            for (const r of gameContr.drums.reels) {
                for (const s of r.symbols) {
                    if (s.name === 12) {
                        // s.clearTint();
                        const imRect = [];
                        // imRect.push({ x: gameContr.getReelXPos(r.reelIndex), y: s.y, width: s.width + 2 * SCALE, height: s.height + 1.3 * SCALE });
                        imRect.push({ x: gameContr.getReelXPos(r.reelIndex), y: s.y + 1.3 * SCALE, width: s.width + 2 * SCALE, height: s.height + 1.3 * SCALE });
                        // gameContr.drawRoundRect(imRect, 12);
                    }
                }
            }
            let scattersSum = 0;
            if (gameContr.scattersCount > 1) {
                scattersSum = scattersSum + WinCoeffs[12][gameContr.scattersCount - 2];
            }
            scattersSum *= gameContr.currentBet.total;
            console.log("Общий выигрыш по скаттерам: " + scattersSum);
            gameContr.currentTotalWin += scattersSum;
            gameContr.scatter.length = gameContr.getAnimLength(12);
            gameContr.scatter.event = gameContr.time.addEvent({
                delay: 75, callback: gameContr.onScatterEvent, callbackScope: gameContr, loop: true, paused: false
            });
        }
    }


    public reelsComplete(t) {
        // console.log("reels complete");
        const gi = (this.scene.get("gameinterface") as GameInterface);
        console.log(gameContr.winLines, gameContr.currentScene);
        if ((gameContr.winLines.length > 0) && (gameContr.scattersCount < 3) && (gameContr.currentScene !== GameScene.Free)) {
            gameContr.time.delayedCall(200, gi.startX2SpinEvent, [], gameContr);
        }

        if (gameContr.scattersCount >= 3) {
            gi.setBtnStatuses("alloff");
        }

        gameContr.time.removeAllEvents();
        gameContr.animLine.event = null;
        gameContr.animLine.currIndex = 0;

        // console.log(gameContr.winLines);

        if (gameContr.winLines.length > 0) {
            gameContr.dropShadows();
            gameContr.startTilEvents();
            gameContr.startScatterEvents();
            if (gameContr.freeGame.started === true) {
                gameContr.currentTotalWin = gameContr.currentTotalWin * 3;
            }
            gi.setBonusWin(gameContr.currentTotalWin);
            // gameContr.totalCredit += gameContr.currentTotalWin;
            gameContr.animLine.event = gameContr.time.addEvent({
                delay: 1000, callback: gameContr.showWinLines, callbackScope: gameContr, loop: true
            });
        } else {
            if ((gameContr.freeGame.currentGame === (gameContr.freeGame.totalGames)) && (gameContr.freeGame.currentGame !== 0)) {
                // console.log("Стоп машина!");
                console.log("Завершение фригеймов. Стоп всех ивентов....");
                gameContr.time.clearPendingEvents();
                gameContr.time.removeAllEvents();
                gameContr.freeGame.started = false;
                gameContr.stopAutoGame();

                gi.setBonusGame(false);
                gi.showBonusWin(true);
                gi.setBonusTotalWin(gameContr.currentTotalWin);
                gameContr.graphics.clear();
                for (const r of gameContr.drums.reels) {
                    for (const s of r.symbols) {
                        if (s.name !== "") {
                            s.setTexture("anims", s.name + "/1.png");
                        }
                    }
                }
                gameContr.time.delayedCall(2000, gameContr.stopFreeGames, [false], gameContr);
            }
            if (gameContr.isAutoGame() === true) {
                gameContr.time.clearPendingEvents();
                gameContr.time.delayedCall(1000, gameContr.startPlay, [false], gameContr);
            } else {
                gameContr.setDefault();
                // gi.showDefault();
            }
        }
        gameInt = this.scene.get("gameinterface");
        gameInt.updateEdits();
    }

    public startFreeGames() {
        const gi = (gameContr.scene.get("gameinterface") as GameInterface);
        gi.btnX2SpinEvent.started = false;
        gi.time.removeAllEvents();
        gi.time.clearPendingEvents();
        gi.btnX2SpinEvent.event = null;

        gameContr.currentScene = GameScene.Free;
        gameContr.freeGame.started = true;
        gameContr.freeGame.totalGames += 2;
        // gameContr.freeGame.currentGame = 0;
        gameContr.time.removeAllEvents();
        const main = (this.scene.get("main") as MainScene);
        // main.backgroundMain.setTexture("img", "back/BackFreeGameAll.png").setScale(SCALE);
        main.backgroundMain.setTexture("img", "back/BackBonus.png").setScale(SCALE);
        main.backgroundMain.clearTint();
        for (const r of gameContr.drums.reels) {
            for (const s of r.symbols) {
                s.setVisible(true);
                s.clearAlpha();
                s.clearTint();
            }
        }
        gi.setBonusWin(0);
        gi.setCurrentBonusGame(gameContr.freeGame.currentGame, gameContr.freeGame.totalGames);
        gi.setBonusGame(true);

        const gs = (this.scene.get("graphicsscene") as GraphicsScene);
        gs.showDigits(true);
        gs.tintAllDigits();

        gameContr.startAutoGame();
        gameContr.startPlay();
        console.log("free games started");
    }

    public stopFreeGames() {
        gameContr.time.removeAllEvents();

        gameContr.animLine.event = null;
        gameContr.animLine.currIndex = 0;
        gameContr.freeGame.currentGame = 0;
        gameContr.freeGame.totalGames = 0;
        gameContr.freeGame.totalWin = 0;

        gameContr.freeGame.started = false;
        gameContr.freeGame.totalGames = 0;
        gameContr.freeGame.currentGame = 0;
        gameContr.freeGame.currentWin = 0;
        gameContr.freeGame.totalWin = 0;
        const main = (gameContr.scene.get("main") as MainScene);
        if (gameContr.graphics !== undefined) {
            gameContr.graphics.clear();
        }
        main.backgroundMain.setTexture("img", "back/BackBonus.png").setScale(SCALE);
        main.backgroundMain.clearTint();
        for (const r of gameContr.drums.reels) {
            for (const s of r.symbols) {
                s.setVisible(true);
                s.clearAlpha();
                s.clearTint();
            }
        }

        for (const r of gameContr.drums.reels) {
            for (const s of r.symbols) {
                if (s.name !== "") {
                    s.setTexture("anims", s.name + "/1.png");
                    // console.log(s.name + "/1.png");
                }
            }
        }

        const gi = (gameContr.scene.get("gameinterface") as GameInterface);
        gi.showBonusWin(false);
        gi.gameBonusText.setVisible(false);
        console.log("stopFreeGames");

        const gs = (this.scene.get("graphicsscene") as GraphicsScene);
        gs.clearTintAllDigits();

        gi.enableAllElements();
        gi.setBonusGame(false);
        gi.setBonusWin(false);
        gi.showBonusWin(false);

        if (gameContr.currentTotalWin > 0) {
            gameContr.time.delayedCall(200, gi.startX2SpinEvent, [], gameContr);
            if (gameContr.freeGame.autoEnabled === true) {
                // console.log("Автоигра была включена до фриспинов");
                // gameContr.autoGame.started = true;
                // gi.time.removeAllEvents();
                // gi.time.clearPendingEvents();
                // gi.gc.totalCredit += gameInt.gc.currentTotalWin;
                // gi.currWinDisplay.win = gameInt.gc.currentTotalWin;
                // gi.currWinDisplay.credit = gameInt.gc.totalCredit - gameInt.gc.currentTotalWin;
                // gi.setBtnStatuses("alldis");
                // gameContr.currentScene = GameScene.Main;
                // gi.time.delayedCall(450, gi.discardWins, [], gameInt);
            }
        } else {
            if (gameContr.freeGame.autoEnabled === true) {
                console.log("Автоигра была включена до фриспинов");
                gameContr.autoGame.started = true;
                gameContr.time.clearPendingEvents();
                gameContr.time.delayedCall(1000, gameContr.startPlay, [false], gameContr);
            }
        }
    }

    public preloadFreeGames() {
        gameContr.time.removeAllEvents();
        const main = (this.scene.get("main") as MainScene);
        if (gameContr.graphics !== undefined) {
            gameContr.graphics.clear();
        }
        main.backgroundMain.clearTint();
        const gi = (this.scene.get("gameinterface") as GameInterface);
        gi.freeGamesPrepare();
        for (const r of gameContr.drums.reels) {
            for (const s of r.symbols) {
                s.setVisible(false);
                s.clearAlpha();
                s.clearTint();
            }
        }
        const gs = (this.scene.get("graphicsscene") as GraphicsScene);
        gs.showDigits(false);
        main.backgroundMain.setTexture("img", "back/BackFreeGame.png").setScale(SCALE);
        gameContr.currentScene = GameScene.Free;
        gameContr.time.delayedCall(2000, gameContr.startFreeGames, [false], gameContr);
    }

    // public stopPlay() {
    //     if (gameContr.graphics !== undefined) {
    //         gameContr.graphics.clear();
    //     }
    //     const main = (gameContr.scene.get("main") as MainScene);
    //     if (main !== undefined) {
    //         main.backgroundMain.clearTint();
    //     }
    //     for (const r of gameContr.drums.reels) {
    //         for (const s of r.symbols) {
    //             s.clearAlpha();
    //             s.clearTint();
    //         }
    //     }
    // }

    public startPlay() {

        console.log("gameContr.currentScene", gameContr.currentScene);

        const gi = (this.scene.get("gameinterface") as GameInterface);
        gi.showBonusWin(false);
        if (gi.btnX2SpinEvent.started === true) {
            gi.btnX2SpinEvent.started = false;
            gi.time.removeAllEvents();
            gi.time.clearPendingEvents();
            gi.gc.totalCredit += gameInt.gc.currentTotalWin;
            gi.currWinDisplay.win = gameInt.gc.currentTotalWin;
            gi.currWinDisplay.credit = gameInt.gc.totalCredit - gameInt.gc.currentTotalWin;
            gi.setBtnStatuses("alldis");
            gi.time.delayedCall(450, gi.discardWins, [], gameInt);
        } else {

            gi.time.removeAllEvents();
            gi.time.clearPendingEvents();
            gi.setBtnStatuses("spin");
            // const gs = (this.scene.get("graphicsscene") as GraphicsScene);
            // gs.tintAllDigits();

            if (gameContr.currentScene === GameScene.Risk) {
                // gi.setBtnStatuses("spin");
                const riskScene = (this.scene.get("risk") as RiskScene);
                if (gameContr.currentTotalWin !== 0) {
                    riskScene.stop("win");
                } else {
                    riskScene.stop("lose");
                }

                gi.gc.totalCredit += gameInt.gc.currentTotalWin;
                gi.currWinDisplay.win = gameInt.gc.currentTotalWin;
                gi.currWinDisplay.credit = gameInt.gc.totalCredit - gameInt.gc.currentTotalWin;
                gi.setBtnStatuses("alldis");
                gi.time.delayedCall(450, gi.discardWins, [], gameInt);

                // gameInt.gc.totalCredit += gameInt.gc.currentTotalWin;
                // gameInt.currWinDisplay.win = gameInt.gc.currentTotalWin;
                // gameInt.currWinDisplay.credit = gameInt.gc.totalCredit - gameInt.gc.currentTotalWin;

                // console.log(gameInt.currWinDisplay);

                // gameInt.time.delayedCall(450, this.discardWins, [], this);

            } else {
                if ((gameContr.currentTotalWin > 0) && (gameContr.currentScene === GameScene.Main)) {
                    console.log("сорян, сливай");
                } else {

                    if (gameContr.currentScene === GameScene.Main) {
                        gi.setBtnStatuses("spin");
                    }

                    if (gameContr.currentScene === GameScene.Free) {
                        gameInt.setBtnStatuses("alloff");
                        gameContr.freeGame.currentGame++;
                        // gameInt.setBonusWin(0);
                        const gs = (this.scene.get("graphicsscene") as GraphicsScene);
                        gs.tintAllDigits();
                        gameInt.setCurrentBonusGame(gameContr.freeGame.currentGame, gameContr.freeGame.totalGames);
                    }
                    gameContr.currentTotalWin = 0;
                    gameContr.time.removeAllEvents();
                    gameInt = gameContr.scene.get("gameinterface");
                    if (gameContr.graphics !== undefined) {
                        gameContr.graphics.clear();
                    }

                    const main = (gameContr.scene.get("main") as MainScene);
                    if (main !== undefined) {
                        main.backgroundMain.clearTint();
                    }
                    for (const r of gameContr.drums.reels) {
                        for (const s of r.symbols) {
                            s.clearAlpha();
                            s.clearTint();
                        }
                    }
                    gameContr.winLines = [];
                    gameContr.monroCount = 0;
                    gameContr.isWheelsKeepTurning = true;
                    gameContr.totalCredit -= gameContr.currentBet.value * gameContr.currentLine.value;
                    gameInt.updateEdits();
                    if (gameContr.currMap === readableStates.length - 1) {
                        gameContr.currMap = 0;
                    } else {
                        gameContr.currMap++;
                    }
                    const state = readableStates[gameContr.currMap];
                    const stateCalc = [];
                    let rNum = 0;
                    for (const r of gameContr.drums.reels) {
                        for (let j = 0; j < 3; j++) {
                            stateCalc.push(state[j][rNum]);
                        }
                        rNum++;
                    }
                    console.clear();
                    gameContr.winLines = gameContr.checkIn.calculate2(gameContr.currentLine.value, state);
                    gameContr.scattersCount = gameContr.checkScatter(stateCalc);
                    gameContr.jackpotCount = gameContr.checkJackPot(stateCalc);
                    gameContr.drums.startPlay(state);
                }
            }
        }
    }

    public setDefault() {
        const gi = (this.scene.get("gameinterface") as GameInterface);
        gi.time.removeAllEvents();
        gi.btnX2SpinEvent.event = null;
        gameContr.time.clearPendingEvents();
        gameContr.time.removeAllEvents();
        gameContr.freeGame.started = false;
        gameContr.stopAutoGame();
        gi.setBonusGame(false);
        gi.showBonusWin(true);
        gi.setBonusTotalWin(gameContr.currentTotalWin);
        gameContr.graphics.clear();
        for (const r of gameContr.drums.reels) {
            for (const s of r.symbols) {
                if (s.name !== "") {
                    s.setTexture("anims", s.name + "/1.png");
                    // console.log(s.name + "/1.png");
                }
            }
        }
        gameContr.time.removeAllEvents();
        gameContr.animLine.event = null;
        gameContr.animLine.currIndex = 0;
        gameContr.freeGame.currentGame = 0;
        gameContr.freeGame.totalGames = 0;
        gameContr.freeGame.totalWin = 0;
        gameContr.freeGame.started = false;
        gameContr.freeGame.totalGames = 0;
        gameContr.freeGame.currentGame = 0;
        gameContr.freeGame.currentWin = 0;
        gameContr.freeGame.totalWin = 0;
        const main = (gameContr.scene.get("main") as MainScene);
        if (gameContr.graphics !== undefined) {
            gameContr.graphics.clear();
        }
        main.backgroundMain.setTexture("img", "back/BackBonus.png").setScale(SCALE);
        main.backgroundMain.clearTint();
        for (const r of gameContr.drums.reels) {
            for (const s of r.symbols) {
                s.setVisible(true);
                s.clearAlpha();
                s.clearTint();
            }
        }
        for (const r of gameContr.drums.reels) {
            for (const s of r.symbols) {
                if (s.name !== "") {
                    s.setTexture("anims", s.name + "/1.png");
                }
            }
        }
        const gs = (this.scene.get("graphicsscene") as GraphicsScene);
        gs.clearTintAllDigits();

        gi.showBonusWin(false);
        gi.gameBonusText.setVisible(false);
        gi.setBtnStatuses("main");
        gameContr.currentScene = GameScene.Main;
        gi.enableAllElements();
        gi.setBonusGame(false);
        gi.setBonusWin(false);
        gi.showBonusWin(false);
    }

    private createReadableStates() {
        const newArrLen = 100;
        if (readableStates.length > newArrLen) {
            const maxPosition = readableStates.length - newArrLen;
            const randomPosition = Math.round(Math.random() * maxPosition);
            readableStates = readableStates.slice(randomPosition, randomPosition + newArrLen);
        }
    }
}
