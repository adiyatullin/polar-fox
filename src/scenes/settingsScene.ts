import gameConfig, { GameScene, WinCoeffs } from "../gameConfig";
import { rightJustify } from "../utils/textUtils";
import GameController from "./gameController";
import GameInterface from "./gameInterface";
import SoundController from "./soundController";
const SCALE = gameConfig.SCALE;
const helpConfig = {
    key: "settings",
    height: gameConfig.CANVAS_SIZE.HEIGHT / 2,
    width: gameConfig.CANVAS_SIZE.WIDTH / 2
};
let settingsScene: any;
let initPos: Phaser.Geom.Point = null;
export default class SettingsScene extends Phaser.Scene {
    public bottomTween: Phaser.Tweens.Tween;
    public topTween: Phaser.Tweens.Tween;
    public leftTween: Phaser.Tweens.Tween;
    public rightTween: Phaser.Tweens.Tween;
    public tweenContainer: Phaser.GameObjects.Container;
    public settingsContainer: Phaser.GameObjects.Container;
    public container: Phaser.GameObjects.Container;

    private gi: GameInterface;
    private gc: GameController;
    private sc: SoundController;

    private backGroundSettings: Phaser.GameObjects.Image;

    private btnLeft: Phaser.GameObjects.Image;
    private btnRight: Phaser.GameObjects.Image;

    private settingsImg = [];
    private currFrameIndex: integer;
    private shown: boolean;

    private payments: Phaser.GameObjects.DynamicBitmapText[];

    constructor() {
        super(helpConfig);
        this.container = null;
        this.tweenContainer = null;
        this.settingsContainer = null;
        this.payments = null;
    }
    public start(key) {
        this.tweenContainer.visible = true;
        this.settingsContainer.visible = true;
        this.container.visible = true;
        // this.scene.bringToTop("boot");
        // this.scene.bringToTop("soundcontroller");
        // this.scene.bringToTop("gamecontroller");
        // this.scene.bringToTop("main");
        // this.scene.bringToTop("risk");
        // this.scene.bringToTop("bonus");
        // this.scene.bringToTop("super");
        // this.scene.bringToTop("videocontroller");
        this.currFrameIndex = 0;
        for (const p in this.settingsImg) {
            if (this.settingsImg.hasOwnProperty(p)) {
                this.settingsImg[p].setVisible(false);
            }
        }
        this.settingsImg[this.currFrameIndex].setVisible(true);
        if (key === "settings") {
            this.backGroundSettings.setVisible(true);
            // this.settingsLongImg.setVisible(true);
            this.gi.setBtnStatuses("settings");
            this.initSettings();
            // this.settingsImg[this.currFrameIndex].setVisible(true);
            this.tweenContainer.setVisible(false);
            this.topTween.restart();
            this.scene.bringToTop("settings");
            this.scene.bringToTop("gameinterface");
            this.shown = true;
        }
    }
    public isShown() {
        return this.shown;
    }

    public stop() {
        // this.gi.setBtnStatuses("settings_off");
        settingsScene.payments.forEach((element) => {
            element.setVisible(false);
        });
        this.tweens.killAll();
        this.bottomTween.restart();
        this.shown = false;
    }

    public createPayments() {

        for (let i = 0; i < 28; i++) {
            const payment = new Phaser.GameObjects.DynamicBitmapText(this, 0, 0, "roboto-bold", "750", 20 * SCALE).setVisible(false);
            this.payments.push(payment);
        }
        this.container.add(this.payments);
        let payCoeff;

        // Песец, сова
        for (let i = 0; i < 4; i++) {
            payCoeff = WinCoeffs[10][3 - i] * this.gc.currentBet.value;
            this.payments[i].setText(rightJustify(payCoeff.toString(), 6, " "));
            this.payments[i].setPosition(377 * SCALE, 248 * SCALE + (i * 18) * SCALE);
        }
        // Лиса
        for (let i = 4; i < 8; i++) {
            payCoeff = WinCoeffs[11][7 - i] * this.gc.currentBet.value;
            this.payments[i].setText(rightJustify(payCoeff.toString(), 6, " "));
            this.payments[i].setPosition(625 * SCALE, 213 * SCALE + (i - 4) * 19 * SCALE);
        }

        // Ежик, барсик
        for (let i = 8; i < 11; i++) {
            payCoeff = WinCoeffs[6][11 - i] * this.gc.currentBet.value;
            this.payments[i].setText(rightJustify(payCoeff.toString(), 6, " "));
            this.payments[i].setPosition(900 * SCALE, 254 * SCALE + ((i - 8) * 19) * SCALE);
        }

        // Лес
        for (let i = 11; i < 15; i++) {
            payCoeff = WinCoeffs[12][14 - i] * this.gc.currentBet.value;
            this.payments[i].setText(rightJustify(payCoeff.toString(), 6, " "));
            this.payments[i].setPosition(625 * SCALE, 356 * SCALE + (i - 11) * 19 * SCALE);
        }

        // Заяц
        for (let i = 15; i < 18; i++) {
            payCoeff = WinCoeffs[8][18 - i] * this.gc.currentBet.value;
            this.payments[i].setText(rightJustify(payCoeff.toString(), 6, " "));
            this.payments[i].setPosition(377 * SCALE, 392 * SCALE + (i - 15) * 19 * SCALE);
        }

        // Туз, Король
        for (let i = 18; i < 21; i++) {
            payCoeff = WinCoeffs[4][21 - i] * this.gc.currentBet.value;
            this.payments[i].setText(rightJustify(payCoeff.toString(), 6, " "));
            this.payments[i].setPosition(900 * SCALE, 392 * SCALE + (i - 18) * 19 * SCALE);
        }

        // Дама, Валет, Десятка
        for (let i = 21; i < 24; i++) {
            payCoeff = WinCoeffs[1][24 - i] * this.gc.currentBet.value;
            this.payments[i].setText(rightJustify(payCoeff.toString(), 6, " "));
            this.payments[i].setPosition(492 * SCALE, 487 * SCALE + (i - 21) * 19 * SCALE);
        }

        // Девятка
        for (let i = 24; i < 28; i++) {
            payCoeff = WinCoeffs[0][27 - i] * this.gc.currentBet.value;
            this.payments[i].setText(rightJustify(payCoeff.toString(), 6, " "));
            this.payments[i].setPosition(792 * SCALE, 480 * SCALE + (i - 24) * 19 * SCALE);
        }

    }

    public create(data) {
        settingsScene = this;
        const gc = this.scene.get("gamecontroller");
        if (gc instanceof GameController === false) {
            console.log("Error! GameController not found");
        } else {
            this.gc = gc as GameController;
        }
        const gint = this.scene.get("gameinterface");
        if (gint instanceof GameInterface === false) {
            console.log("Error! GameInterface not found");
        } else {
            this.gi = gint as GameInterface;
        }
        const sc = this.scene.get("soundcontroller");
        if (sc instanceof SoundController === false) {
            console.log("Error! soundController not found");
        } else {
            this.sc = sc as SoundController;
        }
        const width = gameConfig.CANVAS_SIZE.WIDTH;
        const height = gameConfig.CANVAS_SIZE.HEIGHT;
        const gi = this.scene.get("gameinterface");
        if (gi instanceof GameInterface) {
            this.gi = gi;
        }
        this.scene.moveUp("gamecontroller");
        this.container = this.make.container({ x: 0, y: 0, add: true });
        this.add.existing(this.container);

        this.tweenContainer = this.make.container({ x: 0, y: 0, add: true });
        this.settingsContainer = this.make.container({ x: 0, y: 0, add: true });
        // this.add.existing(this.tweenContainer);

        this.backGroundSettings = this.add.image(width / 2, (height / 2),
            "img", gameConfig.IMAGES.MAIN_IMAGES.BACKGROUND_BONUS.PATH +
            gameConfig.IMAGES.MAIN_IMAGES.BACKGROUND_BONUS.NAME + ".png");
        this.backGroundSettings.setVisible(false);
        this.backGroundSettings.setScale(SCALE);

        for (let i = 0; i < 3; i++) {
            const img = this.add.image(130 * SCALE, -(height) + 60 * SCALE, "settingsback", (i + 1).toString() + ".png");
            img.setScale(SCALE * 0.95); img.setVisible(false);
            img.setOrigin(0);
            img.name = (i + 1).toString();
            this.settingsImg.push(img);
        }

        this.btnLeft = this.add.image(200 * SCALE, -(height) / 2 - 20 * SCALE,
            "buttons", "ButHelpLeft.png");
        this.btnLeft.name = "ButHelpLeft";
        this.btnLeft.setVisible(true);
        this.btnLeft.setScale(SCALE * 0.8);
        this.btnLeft.setInteractive();

        this.btnRight = this.add.image(width - 200 * SCALE, -(height) / 2 - 20 * SCALE,
            "buttons", "ButHelpRight.png");
        this.btnRight.name = "ButHelpRight";
        this.btnRight.setVisible(true);
        this.btnRight.setScale(SCALE * 0.8);
        this.btnRight.setInteractive();

        this.bottomTween = this.tweens.add({
            targets: [this.tweenContainer, this.settingsContainer],
            y: 0,
            ease: "Power2",
            duration: 500,
            yoyo: false,
            repeat: 0,
            paused: true,
            onComplete: this.onRightTweenComplete
        });

        this.topTween = this.tweens.add({
            targets: [this.tweenContainer, this.settingsContainer],
            y: height + 30 * SCALE,
            ease: "Power2",
            duration: 500,
            yoyo: false,
            repeat: 0,
            paused: true,
            onComplete: this.onTopTweenComplete
        });

        this.leftTween = this.tweens.add({
            targets: [this.tweenContainer, this.settingsContainer],
            x: - 470 * SCALE,
            ease: "Power2",
            duration: 500,
            yoyo: false,
            repeat: 0,
            paused: true
        });
        this.rightTween = this.tweens.add({
            targets: [this.tweenContainer, this.settingsContainer],
            x: 0,
            ease: "Power2",
            duration: 500,
            yoyo: false,
            repeat: 0,
            paused: true,
            onComplete: this.onRightTweenComplete
        });
        this.settingsContainer.add(this.settingsImg);
        this.settingsContainer.add(this.btnLeft);
        this.settingsContainer.add(this.btnRight);

        this.container.add(this.backGroundSettings);
        this.container.add(this.tweenContainer);
        this.container.add(this.settingsContainer);

        this.input.on("gameobjectup", this.btnHander, this);
        initPos = new Phaser.Geom.Point(0, 0);
        this.tweenContainer.visible = false;
        this.payments = [];
        this.createPayments();
    }

    private btnHander(pointer, gameObject) {
        // console.log(gameObject);
        switch (gameObject.name) {
            case "ButHelpLeft": {
                // console.log("left");
                for (const p in this.settingsImg) {
                    if (this.settingsImg.hasOwnProperty(p)) {
                        this.settingsImg[p].setVisible(false);
                    }
                }
                if (this.currFrameIndex === 0) {
                    this.currFrameIndex = 2;
                } else {
                    this.currFrameIndex--;
                }
                settingsScene.payments.forEach((element) => {
                    element.setVisible(false);
                });
                if (this.currFrameIndex === 0) {
                    settingsScene.payments.forEach((element) => {
                        element.setVisible(true);
                    });
                }
                this.settingsImg[this.currFrameIndex].setVisible(true);
                this.sc.playSound({ NAME: "ButtonBet", LOOP: false });

                break;
            }
            case "ButHelpRight": {
                // console.log("right");
                for (const p in this.settingsImg) {
                    if (this.settingsImg.hasOwnProperty(p)) {
                        this.settingsImg[p].setVisible(false);
                    }
                }
                if (this.currFrameIndex === 2) {
                    this.currFrameIndex = 0;
                } else {
                    this.currFrameIndex++;
                }
                settingsScene.payments.forEach((element) => {
                    element.setVisible(false);
                });
                if (this.currFrameIndex === 0) {
                    settingsScene.payments.forEach((element) => {
                        element.setVisible(true);
                    });
                }
                this.settingsImg[this.currFrameIndex].setVisible(true);
                this.sc.playSound({ NAME: "ButtonBet", LOOP: false });
                break;
            }
            default: {
                break;
            }
        }
    }
    private onTopTweenComplete() {
        settingsScene.payments.forEach((element) => {
            element.setVisible(true);
        });
        // settingsScene.gi.setBtnStatuses("settings_on");
    }
    private onRightTweenComplete() {
        settingsScene.gc.currentScene = GameScene.Main;
        settingsScene.container.visible = false;
        settingsScene.gi.showDefault();
    }
    private initSettings() {
        this.time.removeAllEvents();
    }
}
