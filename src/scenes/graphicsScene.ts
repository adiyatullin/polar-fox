import gameConfig from "../gameConfig";
import GameController from "./gameController";
const SCALE = gameConfig.SCALE;

const leftDigits = [4, 2, 9, 6, 1, 7, 8, 3, 5];
const rightDigits = [4, 2, 8, 6, 1, 7, 9, 3, 5];

const graphicsConfig = {
    key: "graphicsscene",
    height: gameConfig.CANVAS_SIZE.HEIGHT / 2,
    width: gameConfig.CANVAS_SIZE.WIDTH / 2
};
export default class GraphicsScene extends Phaser.Scene {
    public graphics: Phaser.GameObjects.Graphics;
    public digits = [];
    public container: Phaser.GameObjects.Container;
    public digit = {
        image: Phaser.GameObjects.Image,
        coords: Phaser.Math.Vector2
    };
    private gc: GameController;
    constructor() {
        super(graphicsConfig);
        // this.digits = [];
        this.container = null;
    }

    public tintAllDigits() {
        this.digits.forEach((element) => {
            element.image.setTint(0x354f77);
        });
    }

    public clearTintAllDigits() {
        this.digits.forEach((element) => {
            element.image.clearTint();
        });
    }

    public showDigits(key) {
        this.digits.forEach((element) => {
            element.image.setVisible(key);
        });
    }

    public clearTintForLine(lineIndex) {
        let i = leftDigits.indexOf(lineIndex);
        const im1 = this.digits[i].image;
        im1.clearTint();
        i = rightDigits.indexOf(lineIndex);
        const im2 = this.digits[(i) + 9].image;
        im2.clearTint();
    }

    public getDigitCoords() {
        const width = gameConfig.CANVAS_SIZE.WIDTH;
        const height = gameConfig.CANVAS_SIZE.HEIGHT;

        const centerPoint = {
            x: width / 2,
            y: height / 2
        };

        // const coords = [];
        // for (let i = 0; i < 2; i++) {
        //     for (let j = 1; j < 10; j++) {
        //         if (i === 0) {
        //             const x1 = centerPoint.x - 539 * SCALE;
        //             let y1 = centerPoint.y - 285 * SCALE + j * 88;
        //             if (j === 1) { y1 = y1 + 2 * SCALE; }
        //             if (j === 2) { y1 = y1 + 5 * SCALE; }
        //             if (j === 3) { y1 = y1 + 4 * SCALE; }
        //             if (j === 4) { y1 = y1 - 4 * SCALE; }
        //             if (j === 6) { y1 = y1 + 5 * SCALE; }
        //             if (j === 8) { y1 = y1 - 4 * SCALE; }
        //             if (j === 9) { y1 = y1 + 1 * SCALE; }
        //             coords.push({ x: x1, y: y1 });
        //         } else {
        //             const x1 = centerPoint.x + 545 * SCALE;
        //             let y1 = centerPoint.y - 285 * SCALE + j * 88;
        //             if (j === 1) { y1 = y1 + 2 * SCALE; }
        //             if (j === 2) { y1 = y1 + 5 * SCALE; }
        //             if (j === 3) { y1 = y1 + 4 * SCALE; }
        //             if (j === 4) { y1 = y1 - 4 * SCALE; }
        //             if (j === 6) { y1 = y1 + 5 * SCALE; }
        //             if (j === 8) { y1 = y1 - 4 * SCALE; }
        //             if (j === 9) { y1 = y1 + 1 * SCALE; }
        //             coords.push({ x: x1, y: y1 });
        //         }
        //     }
        // }
        // console.log(coords);

        const coords = [];
        for (let i = 0; i < 2; i++) {
            for (let j = 1; j < 10; j++) {
                if (i === 0) {
                    const x1 = centerPoint.x - 539 * SCALE;
                    let y1 = centerPoint.y - 287 * SCALE + j * 61 * SCALE;
                    if (j === 5) { y1 = y1 - 7 * SCALE; } // 1
                    if (j === 1) { y1 = y1 - 7 * SCALE; } // 2
                    if (j === 9) { y1 = y1 - 17 * SCALE; } // 5
                    if (j === 8) { y1 = y1 - 19 * SCALE; } // 3
                    if (j === 7) { y1 = y1 - 16 * SCALE; } // 8
                    if (j === 2) { y1 = y1 + 3 * SCALE; } // 2
                    if (j === 1) { y1 = y1 + 7 * SCALE; } // 4
                    if (j === 4) { y1 = y1 - 13 * SCALE; } // 6
                    if (j === 6) { y1 = y1 - 6 * SCALE; } // 7
                    if (j === 3) { y1 = y1 - 2 * SCALE; } // 9
                    coords.push({ x: x1, y: y1 });
                } else {
                    const x1 = centerPoint.x + 545 * SCALE;
                    let y1 = centerPoint.y - 287 * SCALE + j * 61 * SCALE;
                    if (j === 5) { y1 = y1 - 7 * SCALE; } // 1
                    if (j === 1) { y1 = y1 - 7 * SCALE; } // 2
                    if (j === 9) { y1 = y1 - 17 * SCALE; } // 5
                    if (j === 8) { y1 = y1 - 19 * SCALE; } // 3
                    if (j === 7) { y1 = y1 - 16 * SCALE; } // 8
                    if (j === 2) { y1 = y1 + 3 * SCALE; } // 2
                    if (j === 1) { y1 = y1 + 7 * SCALE; } // 4
                    if (j === 4) { y1 = y1 - 13 * SCALE; } // 6
                    if (j === 6) { y1 = y1 - 6 * SCALE; } // 7
                    if (j === 3) { y1 = y1 - 2 * SCALE; } // 9
                    coords.push({ x: x1, y: y1 });
                }
            }
        }


        return coords;
    }

    public create(data) {

        const width = screen.width * devicePixelRatio;
        const height = screen.height * devicePixelRatio;
        this.graphics = this.add.graphics();
        // this.graphics.lineStyle(10, 0x4286f4, 1);
        // this.graphics.strokeRect(0, 0, width / 2, height / 2);

        const centerPoint = {
            x: width / 2,
            y: height / 2
        };

        // console.log(width, height, centerPoint, screen.width, screen.height);



        const coords = this.getDigitCoords();
        const gc = this.scene.get("gamecontroller");
        for (let i = 0; i < 2; i++) {
            for (let j = 1; j < 10; j++) {
                if (i === 0) {
                    this.digits.push(
                        {
                            // image: this.add.image(0, 0, "img", "digits/" + j.toString() + ".png").setVisible(false).setScale(SCALE),
                            image: this.add.image(0, 0, "img", "digits/" + leftDigits[j - 1] + ".png").setVisible(false).setScale(SCALE),
                            coords: null,
                            side: i
                        }
                    );
                }
                if (i === 1) {
                    this.digits.push(
                        {
                            // image: this.add.image(0, 0, "img", "digits/" + j.toString() + ".png").setVisible(false).setScale(SCALE),
                            image: this.add.image(0, 0, "img", "digits/" + rightDigits[j - 1] + ".png").setVisible(false).setScale(SCALE),
                            coords: null,
                            side: i
                        }
                    );
                }
            }
        }
        this.container = this.make.container({ x: 0, y: 0, add: true });
        this.add.existing(this.container);
        let k = 0;
        this.digits.forEach((element) => {
            this.container.add(element.image);
            element.image.setPosition(coords[k].x, coords[k].y).setVisible(true);
            k++;
        });
        // this.container.add(this.digits);
        // this.graphics.setPosition(this.cameras.main.x, this.cameras.main.y);
        // console.log(this.digits);
        // console.log(this.container);
    }
}
