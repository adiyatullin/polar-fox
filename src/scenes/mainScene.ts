import gameConfig, { GameScene, ScreenMode } from "../gameConfig";
import GameController from "./gameController";
import GameInterface from "./gameInterface";
import GraphicsScene from "./graphicsScene";
import SoundController from "./soundController";
const SCALE = gameConfig.SCALE;
const mainConfig = {
    key: "main",
    height: gameConfig.CANVAS_SIZE.HEIGHT / 2,
    width: gameConfig.CANVAS_SIZE.WIDTH / 2
};
let mainScene;
export default class MainScene extends Phaser.Scene {

    public container: Phaser.GameObjects.Container;
    public currScreenOrientation: ScreenMode;
    public prevScreenOrientation: ScreenMode;
    public backgroundMain: Phaser.GameObjects.Image;
    public graphics: Phaser.GameObjects.Graphics;
    // private title: Phaser.GameObjects.Image;
    private gc: GameController;
    private gi: GameInterface;
    private gs: GraphicsScene;
    private sc: SoundController;


    constructor() {
        super(mainConfig);
        this.backgroundMain = null;
        // this.title = null;
        this.container = null;
    }

    public getNewSize(ClientWidth, ClientHeight) {
        const FAspectRatio = 1280 / 720;
        let Height = (ClientWidth / FAspectRatio);
        let Width = (ClientHeight * FAspectRatio);
        if (Height < ClientHeight) {
            Height = (ClientWidth / FAspectRatio);
            Width = (Height * FAspectRatio);
        }
        if (Height > ClientHeight) {
            Height = (Width / FAspectRatio);
            Width = (ClientHeight * FAspectRatio);
        }
        return { Height, Width };
    }

    public checkRotate() {
        // tslint:disable-next-line:variable-name
        const width = window.innerWidth * window.devicePixelRatio;
        // tslint:disable-next-line:variable-name
        const height = window.innerHeight * window.devicePixelRatio;
        const originalAspectRatio = 1280 / 720;
        // Get the actual device aspect ratio
        const currentAspectRatio = width / height;
        if (currentAspectRatio < 1 && window.devicePixelRatio !== 1) {
            // console.log("(currentAspectRatio < 1 && window.devicePixelRatio !== 1)");
            this.currScreenOrientation = ScreenMode.Portrait;
        } else {
            if (currentAspectRatio > originalAspectRatio) {
                this.currScreenOrientation = ScreenMode.Landscape;
                // console.log("(currentAspectRatio > originalAspectRatio)");
            } else {
                this.currScreenOrientation = ScreenMode.Landscape;
                // console.log("(currentAspectRatio <= originalAspectRatio)");
            }
        }
        if (this.prevScreenOrientation !== this.currScreenOrientation) {
            this.prevScreenOrientation = this.currScreenOrientation;
            const newSize = this.getNewSize(window.innerHeight, window.innerWidth);
            if (this.currScreenOrientation === ScreenMode.Portrait) {
                if (currentAspectRatio <= (1 / originalAspectRatio)) {
                    this.backgroundMain.setTexture("img", "back/BackPortrait.png").setDisplaySize(newSize.Width * window.devicePixelRatio,
                        newSize.Height * window.devicePixelRatio);
                    this.gc.setViewPort(this.backgroundMain.originX + 186 * SCALE * 0.667,
                        this.backgroundMain.originY + 215 * SCALE,
                        newSize.Width * window.devicePixelRatio, newSize.Height * window.devicePixelRatio, true, false);
                    console.log("1");
                } else {
                    const widthToHeight = 1280 / 720;
                    let newWidth = window.innerWidth * window.devicePixelRatio;
                    let newHeight = window.innerHeight * window.devicePixelRatio;
                    const newWidthToHeight = newWidth / newHeight;

                    if (newWidthToHeight > widthToHeight) {
                        newWidth = newHeight * widthToHeight;
                    } else {
                        newHeight = newWidth / widthToHeight;
                    }
                    this.backgroundMain.setTexture("img", "back/BackPortraitPlan.png").setScale(SCALE * 0.63, SCALE * 0.265);
                    this.gc.setViewPort(this.backgroundMain.originX + 130 * SCALE, this.backgroundMain.originY + 170 * SCALE,
                        SCALE * 1.039, SCALE * 0.27, true, true);
                    console.log("2");
                }
                this.scene.bringToTop("main");
                this.scene.bringToTop("gamecontroller");
                this.scene.bringToTop("graphicsscene");
                this.scene.bringToTop("gameinterface");
            } else {
                this.backgroundMain.setTexture("img", "back/BackBonus.png").setScale(SCALE);
                this.scene.bringToTop("main");
                this.scene.bringToTop("gamecontroller");
                this.scene.bringToTop("graphicsscene");
                this.scene.bringToTop("gameinterface");
                this.gc.setViewPort(this.backgroundMain.originX + 153 * SCALE,
                    this.backgroundMain.originY + 105 * SCALE,
                    SCALE, SCALE, false, false);
                console.log("3");
            }
            this.gi.showDefault();
            this.backgroundMain.setBlendMode(Phaser.BlendModes.SCREEN);
            this.gi.setRotate(this.currScreenOrientation, newSize);
            this.events.emit("setRotate");
            this.gc.graphics.setPosition(this.gc.cameras.main.x, this.gc.cameras.main.y);
        }

    }

    public setGameMode(key) {
        gameConfig.CURRENT_GAME_MODE = key;
        const sc = this.scene.get("soundcontroller");
        const vc = this.scene.get("videocontroller");
        const gi = this.scene.get("gameinterface");
        const gc = this.scene.get("gamecontroller");
        const gs = this.scene.get("graphicsscene");

        const main = this.scene.get("main");
        const settings = this.scene.get("settings");
        const jackpot = this.scene.get("jackpot");
        const risk = this.scene.get("risk");
        const bonus = this.scene.get("bonus");
        const superb = this.scene.get("super");
        if ((sc instanceof SoundController) && (gi instanceof GameInterface) && (gc instanceof GameController) &&
            (main instanceof MainScene)) {
            this.sc = sc; this.gc = gc; this.gi = gi;
            this.gc.currentGameMode = key;
        }
    }
    public create(data) {
        mainScene = this;
        const width = gameConfig.CANVAS_SIZE.WIDTH;
        const height = gameConfig.CANVAS_SIZE.HEIGHT;
        const gc = this.scene.get("gamecontroller");
        const vc = this.scene.get("videocontroller");
        const bootscene = this.scene.get("boot");
        const gs = this.scene.get("graphicsscene");

        this.scene.moveUp("gamecontroller");
        this.scene.moveUp("main");
        this.scene.moveUp("gameinterface");
        // const title = this.textures.getFrame("img", gameConfig.IMAGES.MAIN_IMAGES.TITLE.PATH + gameConfig.IMAGES.MAIN_IMAGES.TITLE.NAME + ".png");
        // this.backgroundMain = this.add.image(width / 2, (height / 2), "img", gameConfig.IMAGES.MAIN_IMAGES.BACKGROUND_MAIN.PATH + gameConfig.IMAGES.MAIN_IMAGES.BACKGROUND_MAIN.NAME + ".epng");
        this.backgroundMain = this.add.image(width / 2, (height / 2), "img", null);
        // this.backgroundMain.setScale(SCALE);
        // this.title = this.add.image(width / 2, 20 * SCALE + (title.height) / 2 * SCALE,
        //     "img", gameConfig.IMAGES.MAIN_IMAGES.TITLE.PATH + gameConfig.IMAGES.MAIN_IMAGES.TITLE.NAME + ".png");
        // this.title.setScale(SCALE);
        this.container = this.make.container({ x: 0, y: 0, add: true });
        this.add.existing(this.container);
        this.container.add(this.backgroundMain);
        // this.container.add(this.title);
        this.container.x = 0;
        this.container.y = 0;
        if (data.mobile) {
            this.setGameMode(gameConfig.GAME_MODES.MONRO);
        } else {
            this.setGameMode(gameConfig.GAME_MODES.VEKSEL);
        }



        this.checkRotate();
        this.gc.currentScene = GameScene.Main;
        this.scene.resume("soundcontroller");
        this.scene.resume("gamecontroller");
        this.scene.resume("videocontroller");
        this.scene.resume("settings");
        this.scene.resume("gameinterface");
        this.scene.resume("main");
        // console.log("create main");

        this.time.addEvent({
            delay: 250, callback: this.checkRotate, callbackScope: this, loop: true
        });





        // window.addEventListener("resize", this.resizeGame, false);
        // window.addEventListener("orientationchange", this.resizeGame, false);
    }

    public resizeGame() {


        // tslint:disable-next-line:variable-name
        const width = window.innerWidth * window.devicePixelRatio;
        // tslint:disable-next-line:variable-name
        const height = window.innerHeight * window.devicePixelRatio;
        const originalAspectRatio = 1280 / 720;
        // Get the actual device aspect ratio
        const currentAspectRatio = width / height;
        if (currentAspectRatio < 1 && window.devicePixelRatio !== 1) {
            // console.log("(currentAspectRatio < 1 && window.devicePixelRatio !== 1)");
            this.currScreenOrientation = ScreenMode.Portrait;
        } else {
            if (currentAspectRatio > originalAspectRatio) {
                this.currScreenOrientation = ScreenMode.Landscape;
                // console.log("(currentAspectRatio > originalAspectRatio)");
            } else {
                this.currScreenOrientation = ScreenMode.Landscape;
                // console.log("(currentAspectRatio <= originalAspectRatio)");
            }
        }

        // console.log(this.currScreenOrientation);


        const widthToHeight = 1280 / 720;
        let newWidth = window.innerWidth;
        let newHeight = window.innerHeight;
        const newWidthToHeight = newWidth / newHeight;

        if (newWidthToHeight > widthToHeight) {
            newWidth = newHeight * widthToHeight;
        } else {
            newHeight = newWidth / widthToHeight;
        }

        console.log("marginTop", (-newHeight / 2) + "px");
        console.log("marginLeft", (-newWidth / 2) + "px");

        console.log("newWidth", newWidth + "px");
        console.log("newHeight", newHeight + "px");
        // console.log(document);

        // gameArea.style.marginTop = (-newHeight / 2) + "px";
        // gameArea.style.marginLeft = (-newWidth / 2) + "px";

        // gameCanvas.style.marginTop = (-newHeight / 2) + "px";
        // gameCanvas.style.marginLeft = (-newWidth / 2) + "px";

        const gameCanvas = document.getElementById("canvas");
        const gameArea = document.getElementById("gameArea");

        if (this.currScreenOrientation === ScreenMode.Landscape) {
            gameCanvas.style.width = newWidth + "px";
            gameCanvas.style.height = newHeight + "px";
            // gameArea.style.marginTop = (-newHeight / 2) + "px";
            // gameArea.style.marginLeft = (-newWidth / 2) + "px";
        } else {
            gameCanvas.style.width = newHeight + "px";
            gameCanvas.style.height = newWidth + "px";
            // gameArea.style.marginTop = (-newWidth / 2) + "px";
            // gameArea.style.marginLeft = (-newHeight / 2) + "px";
        }
    }
}
