import gameConfig, { ScreenMode } from "../gameConfig";
import GameConfig from "../gameConfig";
const SCALE = gameConfig.SCALE;

const bootConfig = {
    key: "boot",
    active: true,
    pack: {
        key: "image",
        files: [
            {
                type: "image",
                key: gameConfig.PRELOADER.BACKGROUND_PRELOADER_LANDSCAPE.NAME,
                url: gameConfig.PRELOADER.BACKGROUND_PRELOADER_LANDSCAPE.PATH
            },
            {
                type: "image",
                key: gameConfig.PRELOADER.BACKGROUND_PRELOADER_PORTRAIT.NAME,
                url: gameConfig.PRELOADER.BACKGROUND_PRELOADER_PORTRAIT.PATH
            },
            {
                type: "bitmapFont",
                key: "azo-fire",
                textureURL: "assets/fonts/bitmap/azo-fire.png",
                fontDataURL: "assets/fonts/bitmap/azo-fire.xml"
            }
        ]
    }
    // ,
    // width: gameConfig.CANVAS_SIZE.WIDTH / 2,
    // height: gameConfig.CANVAS_SIZE.HEIGHT / 2
};

export default class BootScene extends Phaser.Scene {
    public currScreenOrientation: ScreenMode;
    public prevScreenOrientation: ScreenMode;
    public prelBack: any;
    public loadingText: any;
    public frames: {
        title: any; background_main: any; background_bonus: any; take_or_risk: any; symbols: any[]; animations: any[];
        panels: any[]; lines: any[]; fonts: any[]; buttons: any[]; scatters: any[]; cards: any[]; sounds: any[], mobile: boolean
    };

    constructor() {
        super(bootConfig);
        this.frames = {
            title: null,
            background_main: null,
            background_bonus: null,
            take_or_risk: null,
            symbols: [],
            animations: [],
            panels: [],
            lines: [],
            fonts: [],
            buttons: [],
            scatters: [],
            cards: [],
            sounds: [],
            mobile: false
        };
    }

    public getNewSize(ClientWidth, ClientHeight) {
        const FAspectRatio = 1280 / 720;
        let Height = (ClientWidth / FAspectRatio);
        let Width = (ClientHeight * FAspectRatio);
        if (Height < ClientHeight) {
            Height = (ClientWidth / FAspectRatio);
            Width = (Height * FAspectRatio);
        }
        if (Height > ClientHeight) {
            Height = (Width / FAspectRatio);
            Width = (ClientHeight * FAspectRatio);
        }
        return { Height, Width };
    }


    public checkRotate() {
        // console.log("check");
        // tslint:disable-next-line:variable-name
        const width = window.innerWidth * window.devicePixelRatio;
        // tslint:disable-next-line:variable-name
        const height = window.innerHeight * window.devicePixelRatio;
        const originalAspectRatio = 1280 / 720;
        // Get the actual device aspect ratio
        const currentAspectRatio = width / height;
        if (currentAspectRatio < 1 && window.devicePixelRatio !== 1) {
            // console.log("(currentAspectRatio < 1 && window.devicePixelRatio !== 1)");
            this.currScreenOrientation = ScreenMode.Portrait;
        } else {
            if (currentAspectRatio > originalAspectRatio) {
                this.currScreenOrientation = ScreenMode.Landscape;
                // console.log("(currentAspectRatio > originalAspectRatio)");
            } else {
                this.currScreenOrientation = ScreenMode.Landscape;
                // console.log("(currentAspectRatio <= originalAspectRatio)");
            }
        }
        if (this.prevScreenOrientation !== this.currScreenOrientation) {
            this.prevScreenOrientation = this.currScreenOrientation;
            // console.log("orientation changed", this.currScreenOrientation);
            this.events.emit("updateBonusDigits");
        }
    }

    public preload() {
        this.checkRotate();
        const newSize = this.getNewSize(window.innerHeight, window.innerWidth);

        const width = window.innerWidth;
        const height = window.innerHeight;
        const scaleX = window.devicePixelRatio * newSize.Width / width / 2;
        const scaleY = window.devicePixelRatio * newSize.Height / height / 2;

        if (this.currScreenOrientation === ScreenMode.Portrait) {
            this.prelBack = new Phaser.GameObjects.Image(this, gameConfig.PRELOADER.BACKGROUND_PRELOADER_PORTRAIT.X, gameConfig.PRELOADER.BACKGROUND_PRELOADER_PORTRAIT.Y,
                gameConfig.PRELOADER.BACKGROUND_PRELOADER_PORTRAIT.NAME).setScale(SCALE * scaleX, SCALE * scaleY);
            this.add.existing(this.prelBack);
        } else {
            this.prelBack = new Phaser.GameObjects.Image(this, gameConfig.PRELOADER.BACKGROUND_PRELOADER_LANDSCAPE.X, gameConfig.PRELOADER.BACKGROUND_PRELOADER_LANDSCAPE.Y,
                gameConfig.PRELOADER.BACKGROUND_PRELOADER_LANDSCAPE.NAME).setScale(SCALE);
            this.add.existing(this.prelBack);
        }
        const centerX = GameConfig.CANVAS_SIZE.WIDTH / 2;
        const centerY = GameConfig.CANVAS_SIZE.HEIGHT / 2;
        // this.loadingText = new Phaser.GameObjects.BitmapText(this, 120 * SCALE, 631 * SCALE, "azo-fire", "Loading...", 8 * SCALE).setOrigin(0).setScale(SCALE * scaleX, SCALE * scaleY);
        // this.loadingText = new Phaser.GameObjects.BitmapText(this, centerX - 360 * SCALE, centerY + 270 * SCALE, "azo-fire", "Loading...", 8 * SCALE).setOrigin(0).setScale(SCALE * scaleX, SCALE * scaleY);
        this.loadingText = new Phaser.GameObjects.BitmapText(this, centerX - 360 * SCALE, centerY + 270 * SCALE,
            "azo-fire", "Loading...", window.devicePixelRatio * 2).setOrigin(0).setScale(scaleX, scaleY);
        this.add.existing(this.loadingText);
        const progressBar = this.add.graphics();
        const loadingText = this.loadingText;
        let pbHeight = gameConfig.PRELOADER.PROGRESS_BAR.HEIGHT;
        if (this.currScreenOrientation === ScreenMode.Portrait) {
            pbHeight = pbHeight * 0.5;
        }
        this.load.on("progress", function (value) {
            progressBar.clear();
            progressBar.fillStyle(0xffff00, 1.0);
            progressBar.fillRect(
                (gameConfig.PRELOADER.PROGRESS_BAR.X), (gameConfig.PRELOADER.PROGRESS_BAR.Y),
                gameConfig.PRELOADER.PROGRESS_BAR.WIDTH * value, pbHeight);
            loadingText.setText("Loading...                               " +
                Math.round(value * 100) + "%").setDepth(1);
        });

        this.load.on("complete", function () {
            progressBar.destroy();
            loadingText.destroy();
        });

        this.load.atlas("tils", "assets/tils.png", "assets/tils.json");
        this.load.atlas("scatters", "assets/scatters.png", "assets/scatters.json");
        this.load.atlas("buttons", "assets/buttons.png", "assets/buttons.json");
        this.load.multiatlas("img", "assets/img.json", "assets");
        this.load.multiatlas("anims", "assets/anims.json", "assets");
        this.load.multiatlas("jackpot", "assets/jackpot.json", "assets");
        this.load.multiatlas("settingsback", "assets/settingsback.json", "assets");

        // Load Fonts
        for (const font in gameConfig.FONTS) {
            if (gameConfig.FONTS.hasOwnProperty(font)) {
                this.load.bitmapFont(gameConfig.FONTS[font].NAME, gameConfig.PATHS.FONTS + gameConfig.FONTS[font].NAME + ".png",
                    gameConfig.PATHS.FONTS + gameConfig.FONTS[font].NAME + ".xml");
            }
        }

        // Load Sounds
        for (const sound in gameConfig.SOUNDS) {
            if (gameConfig.SOUNDS.hasOwnProperty(sound)) {
                this.load.audio(gameConfig.SOUNDS[sound].NAME, gameConfig.PATHS.SOUNDS + gameConfig.SOUNDS[sound].NAME + ".mp3");
            }
        }
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            this.frames.mobile = true;
            // console.log("ITS mobile ! :)", navigator.userAgent);
        } else {
            // console.log("NOT mobile ! :(", navigator.userAgent);
            this.frames.mobile = false;
        }

    }
    public create() {

        this.currScreenOrientation = ScreenMode.Landscape;
        this.time.addEvent({
            delay: 250, callback: this.checkRotate, callbackScope: this, loop: true
        });

        this.frames.title = ["title"];
        this.frames.background_main = ["background_main"];
        this.frames.background_bonus = ["background_bonus"];
        this.frames.take_or_risk = ["take_or_risk"];
        // load buttons
        for (const btn in gameConfig.BUTTONS) {
            if (gameConfig.BUTTONS.hasOwnProperty(btn)) {
                this.frames.buttons.push({
                    NAME: gameConfig.BUTTONS[btn].NAME,
                    X: gameConfig.BUTTONS[btn].LANDSCAPE.X,
                    Y: gameConfig.BUTTONS[btn].LANDSCAPE.Y,
                    ZOOM: gameConfig.BUTTONS[btn].LANDSCAPE.Z
                });
            }
        }

        for (const panel in gameConfig.IMAGES.PANEL_IMAGES) {
            if (gameConfig.IMAGES.PANEL_IMAGES.hasOwnProperty(panel)) {
                this.frames.panels.push({
                    NAME: gameConfig.IMAGES.PANEL_IMAGES[panel].NAME,
                    PATH: gameConfig.IMAGES.PANEL_IMAGES[panel].PATH,
                    X: gameConfig.IMAGES.PANEL_IMAGES[panel].X,
                    Y: gameConfig.IMAGES.PANEL_IMAGES[panel].Y
                });
            }
        }
        // Load Lines
        for (const img in gameConfig.IMAGES.LINE_IMAGES) {
            if (gameConfig.IMAGES.LINE_IMAGES.hasOwnProperty(img)) {
                for (let i = 1; i <= (gameConfig.IMAGES.LINE_IMAGES[img].COUNT); i++) {
                    this.frames.lines.push({
                        NAME_PREFIX: gameConfig.IMAGES.LINE_IMAGES[img].NAME_PREFIX,
                        NAME: gameConfig.IMAGES.LINE_IMAGES[img].NAME_PREFIX + i,
                        X: gameConfig.IMAGES.LINE_IMAGES[img].X[i - 1],
                        Y: gameConfig.IMAGES.LINE_IMAGES[img].Y[i - 1],
                        SCALE_X: gameConfig.IMAGES.LINE_IMAGES[img].SCALE_X,
                        SCALE_Y: gameConfig.IMAGES.LINE_IMAGES[img].SCALE_Y
                    });
                }
            }
        }
        // load sounds
        for (const sound in gameConfig.SOUNDS) {
            if (gameConfig.SOUNDS.hasOwnProperty(sound)) {
                this.frames.sounds.push(gameConfig.SOUNDS[sound].NAME);
            }
        }
        // this.scene.start("boot", this.frames);
        this.scene.start("riskScene", this.frames);
        this.scene.start("soundcontroller", this.frames);
        this.scene.start("gamecontroller", this.frames);
        this.scene.start("videocontroller", this.frames);
        this.scene.start("settings", this.frames);
        this.scene.start("jackpot", this.frames);
        this.scene.start("risk", this.frames);
        this.scene.start("bonus", this.frames);
        this.scene.start("super", this.frames);
        this.scene.start("graphicsscene", this.frames);
        this.scene.start("gameinterface", this.frames);
        this.scene.start("main", this.frames);


        this.scene.pause("riskScene");
        this.scene.pause("soundcontroller");
        this.scene.pause("gamecontroller");
        this.scene.pause("videocontroller");
        this.scene.pause("settings");
        this.scene.pause("jackpot");
        this.scene.pause("risk");
        this.scene.pause("bonus");
        this.scene.pause("super");
        this.scene.pause("gameinterface");
        this.scene.pause("main");
    }
}
