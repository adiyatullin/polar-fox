import gameConfig from "../gameConfig";

const soundConfig = {
    key: "soundcontroller",
    height: gameConfig.CANVAS_SIZE.HEIGHT / 2,
    width: gameConfig.CANVAS_SIZE.WIDTH / 2
};

let soundScene: any;
export default class SoundController extends Phaser.Scene {
    public sounds = [];
    private enabled: boolean;
    private backPCImage: Phaser.GameObjects.TileSprite;
    constructor() {
        super(soundConfig);
    }
    public create(data) {
        soundScene = this;
        const width = gameConfig.CANVAS_SIZE.WIDTH;
        const height = gameConfig.CANVAS_SIZE.HEIGHT;

        // Load Sounds
        for (const s in data.sounds) {
            if (data.sounds.hasOwnProperty(s)) {
                const sound = this.sound.add(data.sounds[s]);
                this.sounds.push(sound);
            }
        }
        this.enabled = true;

        if (data.mobile === false) {
            this.backPCImage = this.add.tileSprite(0, 0, width * gameConfig.SCALE, height * gameConfig.SCALE, "img",
                gameConfig.IMAGES.MAIN_IMAGES.TILE_BACK.PATH + gameConfig.IMAGES.MAIN_IMAGES.TILE_BACK.NAME + ".png");
            this.backPCImage.setScale(gameConfig.SCALE);
        }
    }
    public playSound(key) {
        if (this.enabled === true) {
            for (const s in this.sounds) {
                if (this.sounds.hasOwnProperty(s)) {
                    if (this.sounds[s].key === key.NAME) {
                        // this.sounds[s].play({ loop: key.LOOP });
                        // console.log(key.NAME);
                    }
                }
            }
        }
    }
    public stopSound(key) {
        for (const s in this.sounds) {
            if (this.sounds.hasOwnProperty(s)) {
                if (this.sounds[s].key === key.NAME) {
                    this.sounds[s].stop();
                }
            }
        }
    }
    public stopAllSounds() {
        for (const s in this.sounds) {
            if (this.sounds.hasOwnProperty(s)) {
                this.sounds[s].stop();
            }
        }
    }
    public swithSound() {
        this.enabled = !this.enabled;
    }
    public isSoundEnabled() {
        return this.enabled;
    }
}
