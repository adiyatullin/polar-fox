import gameConfig, { GameScene } from "../src/gameConfig";

function lerp(a1, a2, t) {
    return a1 * (1 - t) + a2 * t;
}
function linear(amount) {
    return function (t) {
        return (t);
    };
}

let drums;

export default class Drums extends Phaser.GameObjects.Graphics {
    // public initStates = [
    //     [
    //         [2, 4, 4, 1, 4],
    //         [3, 0, 0, 0, 1],
    //         [8, 2, 1, 3, 2],
    //     ],
    //     [
    //         [1, 1, 7, 1, 1],
    //         [8, 8, 1, 3, 3],
    //         [5, 0, 2, 0, 4],
    //     ],
    //     [
    //         [2, 2, 5, 2, 3],
    //         [8, 5, 0, 4, 1],
    //         [1, 7, 1, 1, 0],
    //     ],
    //     [
    //         [3, 1, 2, 2, 1],
    //         [2, 8, 4, 3, 0],
    //         [1, 2, 5, 0, 2],
    //     ],
    //     [
    //         [2, 2, 1, 3, 3],
    //         [0, 0, 5, 8, 0],
    //         [1, 1, 4, 1, 4],
    //     ],
    //     [
    //         [2, 7, 2, 2, 1],
    //         [5, 8, 0, 1, 7],
    //         [3, 3, 1, 0, 2],
    //     ],
    //     [
    //         [1, 4, 8, 3, 1],
    //         [8, 1, 0, 2, 0],
    //         [3, 2, 1, 1, 5],
    //     ],
    //     [
    //         [4, 1, 1, 1, 5],
    //         [2, 7, 7, 8, 1],
    //         [1, 5, 0, 3, 3],
    //     ],
    //     [
    //         [1, 2, 3, 2, 1],
    //         [0, 7, 2, 1, 7],
    //         [3, 1, 1, 7, 0],
    //     ],
    //     [
    //         [2, 5, 7, 2, 1],
    //         [0, 1, 5, 3, 0],
    //         [7, 2, 0, 1, 3],
    //     ],
    //     [
    //         [7, 3, 3, 2, 2],
    //         [0, 1, 1, 5, 7],
    //         [3, 0, 5, 3, 0],
    //     ],
    //     [
    //         [3, 3, 5, 4, 1],
    //         [7, 0, 0, 5, 3],
    //         [1, 2, 1, 7, 0],
    //     ],
    //     [
    //         [1, 2, 1, 4, 1],
    //         [8, 8, 0, 2, 0],
    //         [0, 3, 3, 5, 4],
    //     ],
    //     [
    //         [1, 1, 6, 3, 2],
    //         [4, 0, 0, 0, 7],
    //         [0, 3, 4, 2, 8],
    //     ],
    //     [
    //         [2, 4, 2, 3, 1],
    //         [0, 7, 0, 7, 7],
    //         [1, 2, 1, 1, 4],
    //     ]
    // ];
    public initStates = [
        [
            [0, 1, 2, 0, 1],
            [1, 0, 3, 3, 4],
            [2, 0, 1, 5, 2]
        ]
    ];
    public SCALE; // Масштаб для тилов
    public scene; // Ссылка на сцену, в которой создаются барабаны
    public REEL_WIDTH; // Ширина барабана
    public reels; // Барабаны
    public reelContainer: Phaser.GameObjects.Container; // Общий контейнер для всех барабанов (нужен для централизованного изменения масштаба)
    private running = false; // Крутятся ли в данный момент барабаны
    private tweening = []; // Массив, хранящий информацию по текущим вращениям
    private SYMBOL_SIZE; // Размер текстуры
    private firstSpin; // Первый запуск
    private monroCount; // Чсило выпавших тилов с монро
    private symbolsCount; // Число различных тилов
    private reelsCount; // Число барабанов
    private gapY; // Расстояние между тилами по y-координате
    private state; // Текущее конечное состояние барабанов
    private scatter: {
        event: Phaser.Time.TimerEvent,
        switcher: boolean,
        animIndex: number
    };
    private jackpot: {
        event: Phaser.Time.TimerEvent,
        switcher: boolean,
        animIndex: number
    };
    private currDrumsStopped: number;

    public constructor(scene, reelWidth, symbolSize, symbolsCount, reelsCount, gapY, scale) {
        super(scene);
        this.REEL_WIDTH = reelWidth * scale;
        this.SYMBOL_SIZE = symbolSize * scale;
        this.reels = [];
        this.reelContainer = null;
        this.firstSpin = true;
        this.symbolsCount = symbolsCount;
        this.reelsCount = reelsCount;
        this.scene = scene;
        this.gapY = gapY;
        this.SCALE = scale;
        this.currDrumsStopped = 0;
        drums = this;
    }

    public getImage(reelIndex, imageIndex) {
        function convert(i) {
            switch (reelIndex) {
                case 0: {
                    switch (i) {
                        case 5: {
                            return 0;
                        }
                        case 4: {
                            return 1;
                        }
                        case 1: {
                            return 2;
                        }
                        case 2: {
                            return 3;
                        }
                        case 3: {
                            return 4;
                        }
                    }
                }
                case 1: {
                    switch (i) {
                        case 3: {
                            return 0;
                        }
                        case 5: {
                            return 1;
                        }
                        case 4: {
                            return 2;
                        }
                        case 1: {
                            return 3;
                        }
                        case 2: {
                            return 4;
                        }
                    }
                }
                case 2: {
                    switch (i) {
                        case 2: {
                            return 0;
                        }
                        case 3: {
                            return 1;
                        }
                        case 5: {
                            return 2;
                        }
                        case 4: {
                            return 3;
                        }
                        case 1: {
                            return 4;
                        }
                    }
                }
                case 3: {
                    switch (i) {
                        case 1: {
                            return 0;
                        }
                        case 2: {
                            return 1;
                        }
                        case 3: {
                            return 2;
                        }
                        case 5: {
                            return 3;
                        }
                        case 4: {
                            return 4;
                        }
                    }
                }
                case 4: {
                    switch (i) {
                        case 4: {
                            return 0;
                        }
                        case 1: {
                            return 1;
                        }
                        case 2: {
                            return 2;
                        }
                        case 3: {
                            return 3;
                        }
                        case 5: {
                            return 4;
                        }
                    }
                }
            }
        }
        const symbs = [];
        for (const r of drums.reels) {
            if (r.reelIndex === reelIndex) {
                return r.symbols[convert(imageIndex)];
            }
        }
        return symbs[imageIndex];
    }

    public getLineImages(line) {


        const images = [];

        switch (line) {
            case 1: {
                for (let i = 0; i < 5; i++) {
                    images.push({ reel: i, image: this.getImage(i, 2), line: 2 });
                }
                break;
            }
            case 2: {
                for (let i = 0; i < 5; i++) {
                    images.push({ reel: i, image: this.getImage(i, 1), line: 1 });
                }
                break;
            }
            case 3: {
                for (let i = 0; i < 5; i++) {
                    images.push({ reel: i, image: this.getImage(i, 3), line: 3 });
                }
                break;
            }
            case 4: {
                images.push({ reel: 0, image: this.getImage(0, 1), line: 1 });
                images.push({ reel: 1, image: this.getImage(1, 2), line: 2 });
                images.push({ reel: 2, image: this.getImage(2, 3), line: 3 });
                images.push({ reel: 3, image: this.getImage(3, 2), line: 2 });
                images.push({ reel: 4, image: this.getImage(4, 1), line: 1 });
                break;
            }
            case 5: {
                images.push({ reel: 0, image: this.getImage(0, 3), line: 3 });
                images.push({ reel: 1, image: this.getImage(1, 2), line: 2 });
                images.push({ reel: 2, image: this.getImage(2, 1), line: 1 });
                images.push({ reel: 3, image: this.getImage(3, 2), line: 2 });
                images.push({ reel: 4, image: this.getImage(4, 3), line: 3 });
                break;
            }
            case 6: {
                images.push({ reel: 0, image: this.getImage(0, 2), line: 2 });
                images.push({ reel: 1, image: this.getImage(1, 3), line: 3 });
                images.push({ reel: 2, image: this.getImage(2, 3), line: 3 });
                images.push({ reel: 3, image: this.getImage(3, 3), line: 3 });
                images.push({ reel: 4, image: this.getImage(4, 2), line: 2 });
                break;
            }
            case 7: {
                images.push({ reel: 0, image: this.getImage(0, 2), line: 2 });
                images.push({ reel: 1, image: this.getImage(1, 1), line: 1 });
                images.push({ reel: 2, image: this.getImage(2, 1), line: 1 });
                images.push({ reel: 3, image: this.getImage(3, 1), line: 1 });
                images.push({ reel: 4, image: this.getImage(4, 2), line: 2 });
                break;
            }
            case 8: {
                images.push({ reel: 0, image: this.getImage(0, 3), line: 3 });
                images.push({ reel: 1, image: this.getImage(1, 3), line: 3 });
                images.push({ reel: 2, image: this.getImage(2, 2), line: 2 });
                images.push({ reel: 3, image: this.getImage(3, 1), line: 1 });
                images.push({ reel: 4, image: this.getImage(4, 1), line: 1 });
                break;
            }
            case 9: {
                images.push({ reel: 0, image: this.getImage(0, 1), line: 1 });
                images.push({ reel: 1, image: this.getImage(1, 1), line: 1 });
                images.push({ reel: 2, image: this.getImage(2, 2), line: 2 });
                images.push({ reel: 3, image: this.getImage(3, 3), line: 3 });
                images.push({ reel: 4, image: this.getImage(4, 3), line: 3 });
                break;
            }
        }

        // console.log(0, getImage(0, 1).frame.name);
        // console.log(0, getImage(0, 2).frame.name);
        // console.log(0, getImage(0, 3).frame.name);

        // console.log(1, getImage(1, 1).frame.name);
        // console.log(1, getImage(1, 2).frame.name);
        // console.log(1, getImage(1, 3).frame.name);

        // console.log(2, getImage(2, 1).frame.name);
        // console.log(2, getImage(2, 2).frame.name);
        // console.log(2, getImage(2, 3).frame.name);

        // console.log(3, getImage(3, 1).frame.name);
        // console.log(3, getImage(3, 2).frame.name);
        // console.log(3, getImage(3, 3).frame.name);

        // console.log(4, getImage(4, 1).frame.name);
        // console.log(4, getImage(4, 2).frame.name);
        // console.log(4, getImage(4, 3).frame.name);

        // images.push(getImage(0, 2));
        return images;
    }

    public initialize(data) {
        this.reels = [];
        this.scatter = {
            event: null,
            switcher: false,
            animIndex: 0
        };
        this.jackpot = {
            event: null,
            switcher: false,
            animIndex: 0
        };
        this.monroCount = 0;
        this.reelContainer = this.scene.add.container(0, 0);
        for (let i = 0; i < this.reelsCount; i++) {
            const rc = this.scene.make.container();
            rc.x = i * this.REEL_WIDTH + 10 * this.SCALE;
            this.reelContainer.add(rc);
            const reel = {
                container: rc,
                symbols: [],
                position: 0,
                previousPosition: 0,
                reelIndex: i,
                framesElapsed: 0,
                framesChanged: 0,
                monroRotate: false,
                prevRandomTil: -1
            };
            for (let j = 0; j < 5; j++) {
                const symbol = this.scene.make.image({
                    x: 0,
                    y: 0,
                    key: i,
                    add: false
                });
                symbol.x = 0;
                symbol.y = (j - 1) * (this.SYMBOL_SIZE + this.gapY * this.SCALE);
                // symbol.setScale(this.SCALE * 1.005, this.SCALE * 1.006);
                symbol.setScale(this.SCALE * 0.99, this.SCALE);
                // symbol.setScale(this.SCALE, this.SCALE);
                symbol.setOrigin(0);
                symbol.setActive(false);
                symbol.disableInteractive();
                reel.symbols.push(symbol);
                rc.add(symbol);
            }
            this.reels.push(reel);
        }
        this.scene.container.add(this.reelContainer);
    }
    public setTextures(state) {
        this.state = state;
        this.reels.forEach((reel) => {
            reel.framesChanged = 0;
        });
        for (let i = 0; i < this.reels.length; i++) {
            const reel = this.reels[i];
            for (let j = 0; j < reel.symbols.length; j++) {
                const symbol = reel.symbols[j];
                if (j === 0) {
                    const textureIndex = this.state[0][i];
                    symbol.setTexture("tils", textureIndex + ".png");
                } else if (j === 4) {
                    const textureIndex = this.state[2][i];
                    symbol.setTexture("tils", textureIndex + ".png");
                } else {
                    const textureIndex = this.state[j - 1][i];
                    symbol.setTexture("tils", textureIndex + ".png");
                }
            }
        }
    }
    public update() {
        if (this.running === true) {
            for (let i = 0; i < this.reels.length; i++) {
                const r = this.reels[i];
                r.previousPosition = r.position;
                for (let j = 0; j < r.symbols.length; j++) {
                    const s = r.symbols[j];
                    const prevy = s.y;
                    s.y = (r.position + j) % r.symbols.length * (this.SYMBOL_SIZE + this.gapY) - this.SYMBOL_SIZE;
                    if (s.y < 0 && prevy > this.SYMBOL_SIZE) {
                        r.prevRandomTil = this.getNextRandomTil(r.prevRandomTil);
                        s.setTexture("tils", r.prevRandomTil + ".png");
                        if (r.reelIndex === i) {
                            r.framesElapsed++;
                        }
                        // tslint:disable-next-line:prefer-for-of
                        for (let m = 0; m < this.tweening.length; m++) {
                            const t = this.tweening[m];
                            if ((t.index - 1 === r.reelIndex)) {
                                if ((t.target - t.object[t.property] <= 3.01)) {
                                    if (r.framesChanged < 3) {
                                        const textureIndex = this.state[2 - r.framesChanged][r.reelIndex];
                                        s.setTexture("tils", textureIndex + ".png");
                                        s.name = textureIndex;
                                        r.framesChanged++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            const now = Date.now();
            const remove = [];
            // tslint:disable-next-line:prefer-for-of
            for (let i1 = 0; i1 < this.tweening.length; i1++) {
                const t = this.tweening[i1];
                const phase = Math.min(1, (now - t.start) / t.time);
                t.object[t.property] = lerp(t.propertyBeginValue, t.target, t.easing(phase));
                if (t.change) { t.change(t); }
                if (phase === 1) {
                    t.object[t.property] = t.target;
                    if (t.complete) {
                        t.complete(t);
                    }
                    remove.push(t);
                }
            }
            // tslint:disable-next-line:prefer-for-of
            for (let i2 = 0; i2 < remove.length; i2++) {
                this.tweening.splice(this.tweening.indexOf(remove[i2]), 1);
            }
            // if ((this.tweening.length === 0) && (remove.length === 0)) {
            //     this.onCompleteJump();
            // }

            if ((this.tweening.length === 0) && (remove.length === 0)) {
                this.onCompleteAllBarebons();
            }
        }
    }
    public startPlay(state) {
        if (this.running) { return; }
        const gameInt = this.scene.scene.get("gameinterface");

        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.reels.length; i++) {
            const r = this.reels[i];
            r.position = 0;
            r.framesElapsed = 0;
            r.framesChanged = 0;
            r.monroRotate = false;
        }
        this.currDrumsStopped = 0;
        this.state = state;
        for (let i = 1; i <= this.reels.length; i++) {
            const r = this.reels[i - 1];
            // this.tweenTo(i, r, "position", 7 + (2 * i), 700 + i * 213, linear(0),
            //     null, this.startJump);
            this.tweenTo(i, r, "position", (4 * i), i * 320, linear(0),
                null, this.startJump);
        }
        this.setMonroRotate();
        this.running = true;
    }
    public onScatterEvent() {
        if (this.scatter.animIndex > 3) {
            this.scatter.animIndex = 0;
        }
        this.scatter.animIndex++;
        for (const r of this.reels) {
            for (const s of r.symbols) {
                if (s.name === 12) {
                    s.setTexture("scatters", "Scatter" + this.scatter.animIndex + ".png");
                }
            }
        }
    }
    public onJackpotStartEvent() {
        if (this.jackpot.animIndex > 32) {
            this.jackpot.animIndex = 1;
        }
        this.jackpot.animIndex++;
        const state = this.state;
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.reels.length; i++) {
            const reel = this.reels[i];
            // tslint:disable-next-line:prefer-for-of
            for (let j = 0; j < reel.symbols.length; j++) {
                const symbol = reel.symbols[j];
                if (symbol.name === 9) {
                    symbol.setTexture("jackpot", this.jackpot.animIndex + ".png");
                }
            }
        }
    }
    public showScatter() {
        console.log("showScatter");
        this.scatter.event = this.scene.time.addEvent({
            delay: 125, callback: this.onScatterEvent, callbackScope: this, loop: true
        });
    }

    // private onCompleteJump() {
    //     this.scene.soundController.stopSound({ NAME: "Baraban" });
    //     this.running = false;
    //     for (let i = 0; i < this.reels.length; i++) {
    //         const r = this.reels[i];
    //         r.reelIndex = i;
    //         r.position = 0;
    //         r.framesElapsed = 0;
    //         r.framesChanged = 0;
    //         this.firstSpin = false;
    //         this.monroCount = 0;
    //     }
    //     console.log("onCompleteJump");
    //     this.scene.reelsComplete();
    // }

    private onCompleteAllBarebons() {
        // this.scene.soundController.stopSound({ NAME: "Baraban" });
        // this.running = false;
        // for (let i = 0; i < this.reels.length; i++) {
        //     const r = this.reels[i];
        //     r.reelIndex = i;
        //     r.position = 0;
        //     r.framesElapsed = 0;
        //     r.framesChanged = 0;
        //     this.firstSpin = false;
        //     this.monroCount = 0;
        // }
        // console.log("onCompleteAllBarebons");
        // this.scene.reelsComplete();
    }

    private onCompleteJump() {
        drums.currDrumsStopped++;
        drums.scene.soundController.stopSound({ NAME: "Baraban" });
        if (drums.currDrumsStopped === 5) {
            drums.running = false;
            for (let i = 0; i < drums.reels.length; i++) {
                const r = drums.reels[i];
                r.reelIndex = i;
                r.position = 0;
                r.framesElapsed = 0;
                r.framesChanged = 0;
                drums.firstSpin = false;
                drums.monroCount = 0;
            }
            drums.scene.reelsComplete();
        }
    }

    private setMonroRotate() {
        let k = 0;
        for (let i = 0; i < this.reels.length; i++) {
            for (let j = 0; j < 3; j++) {
                if (this.state[j][i] === 13) {
                    k++;
                }
                if (k === 2) {
                    for (let l = i + 1; l < this.reels.length; l++) {
                        this.reels[l].monroRotate = true;
                    }
                }
            }
        }
        // tslint:disable-next-line:prefer-for-of
        let m = 1;
        drums.tweening.forEach((t) => {
            if (t.object.monroRotate === true) {
                t.time = 700 + m * 213 * 10;
                t.target = 7 + (20 * m);
                m++;
            }
        });
    }
    private getNextRandomTil(value) {
        let rand;
        do {
            rand = Math.floor(Math.random() * this.symbolsCount);
        } while (rand === value);
        return rand;
    }
    private startJump(t) {
        drums.scene.tweens.killTweensOf(t.targets);
        drums.scene.soundController.stopSound({ NAME: gameConfig.SOUNDS.JACKPOT_START.NAME });
        drums.scene.soundController.playSound({ NAME: "BarabanStop", LOOP: false });
        if ((drums.reels[t.object.reelIndex + 1] !== undefined) && (drums.reels[t.object.reelIndex + 1].monroRotate === true)) {
            drums.scene.soundController.playSound({ NAME: gameConfig.SOUNDS.JACKPOT_START.NAME, LOOP: true });
        }
        drums.scene.tweens.add({
            targets: t.object.container,
            y: t.object.container.y + 40,
            ease: "Bounche",
            duration: 100,
            yoyo: true,
            repeat: 0,
            repeatDelay: 0,
            onComplete: drums.onCompleteJump
        });
    }
    private tweenTo(index, object, property, target, time, easing, onchange, oncomplete) {
        const tween = {
            index,
            object,
            property,
            propertyBeginValue: object[property],
            target,
            easing,
            time,
            change: onchange,
            complete: oncomplete,
            start: Date.now()
        };
        this.tweening.push(tween);
        return tween;
    }
}
