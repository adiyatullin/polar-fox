<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.10.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Area</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>anims.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">KeepTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>0</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">anims/0/1.png</key>
            <key type="filename">anims/0/17.png</key>
            <key type="filename">anims/9/1.png</key>
            <key type="filename">anims/9/17.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,37,65,74</rect>
                <key>scale9Paddings</key>
                <rect>32,37,65,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/0/10.png</key>
            <key type="filename">anims/0/11.png</key>
            <key type="filename">anims/0/12.png</key>
            <key type="filename">anims/0/6.png</key>
            <key type="filename">anims/0/7.png</key>
            <key type="filename">anims/0/8.png</key>
            <key type="filename">anims/9/10.png</key>
            <key type="filename">anims/9/11.png</key>
            <key type="filename">anims/9/12.png</key>
            <key type="filename">anims/9/6.png</key>
            <key type="filename">anims/9/7.png</key>
            <key type="filename">anims/9/8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,40,71,79</rect>
                <key>scale9Paddings</key>
                <rect>35,40,71,79</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/0/13.png</key>
            <key type="filename">anims/0/5.png</key>
            <key type="filename">anims/9/13.png</key>
            <key type="filename">anims/9/5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,39,70,79</rect>
                <key>scale9Paddings</key>
                <rect>35,39,70,79</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/0/14.png</key>
            <key type="filename">anims/0/4.png</key>
            <key type="filename">anims/9/14.png</key>
            <key type="filename">anims/9/4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,39,69,79</rect>
                <key>scale9Paddings</key>
                <rect>35,39,69,79</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/0/15.png</key>
            <key type="filename">anims/0/16.png</key>
            <key type="filename">anims/0/2.png</key>
            <key type="filename">anims/0/3.png</key>
            <key type="filename">anims/4/10.png</key>
            <key type="filename">anims/4/11.png</key>
            <key type="filename">anims/4/12.png</key>
            <key type="filename">anims/4/6.png</key>
            <key type="filename">anims/4/7.png</key>
            <key type="filename">anims/4/8.png</key>
            <key type="filename">anims/9/15.png</key>
            <key type="filename">anims/9/16.png</key>
            <key type="filename">anims/9/2.png</key>
            <key type="filename">anims/9/3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,39,69,78</rect>
                <key>scale9Paddings</key>
                <rect>34,39,69,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/0/9.png</key>
            <key type="filename">anims/9/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>37,40,75,80</rect>
                <key>scale9Paddings</key>
                <rect>37,40,75,80</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/1/1.png</key>
            <key type="filename">anims/1/17.png</key>
            <key type="filename">anims/10/1.png</key>
            <key type="filename">anims/10/17.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,35,72,70</rect>
                <key>scale9Paddings</key>
                <rect>36,35,72,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/1/10.png</key>
            <key type="filename">anims/1/11.png</key>
            <key type="filename">anims/1/12.png</key>
            <key type="filename">anims/1/6.png</key>
            <key type="filename">anims/1/7.png</key>
            <key type="filename">anims/1/8.png</key>
            <key type="filename">anims/10/10.png</key>
            <key type="filename">anims/10/11.png</key>
            <key type="filename">anims/10/12.png</key>
            <key type="filename">anims/10/6.png</key>
            <key type="filename">anims/10/7.png</key>
            <key type="filename">anims/10/8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,38,79,76</rect>
                <key>scale9Paddings</key>
                <rect>39,38,79,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/1/13.png</key>
            <key type="filename">anims/1/5.png</key>
            <key type="filename">anims/10/13.png</key>
            <key type="filename">anims/10/5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,38,78,76</rect>
                <key>scale9Paddings</key>
                <rect>39,38,78,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/1/14.png</key>
            <key type="filename">anims/1/4.png</key>
            <key type="filename">anims/10/14.png</key>
            <key type="filename">anims/10/4.png</key>
            <key type="filename">anims/5/14.png</key>
            <key type="filename">anims/5/4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,38,77,76</rect>
                <key>scale9Paddings</key>
                <rect>39,38,77,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/1/15.png</key>
            <key type="filename">anims/1/16.png</key>
            <key type="filename">anims/1/2.png</key>
            <key type="filename">anims/1/3.png</key>
            <key type="filename">anims/10/15.png</key>
            <key type="filename">anims/10/16.png</key>
            <key type="filename">anims/10/2.png</key>
            <key type="filename">anims/10/3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,38,77,75</rect>
                <key>scale9Paddings</key>
                <rect>39,38,77,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/1/9.png</key>
            <key type="filename">anims/10/36.png</key>
            <key type="filename">anims/10/9.png</key>
            <key type="filename">anims/11/1.png</key>
            <key type="filename">anims/11/10.png</key>
            <key type="filename">anims/11/11.png</key>
            <key type="filename">anims/11/12.png</key>
            <key type="filename">anims/11/13.png</key>
            <key type="filename">anims/11/14.png</key>
            <key type="filename">anims/11/15.png</key>
            <key type="filename">anims/11/16.png</key>
            <key type="filename">anims/11/17.png</key>
            <key type="filename">anims/11/18.png</key>
            <key type="filename">anims/11/19.png</key>
            <key type="filename">anims/11/2.png</key>
            <key type="filename">anims/11/20.png</key>
            <key type="filename">anims/11/21.png</key>
            <key type="filename">anims/11/22.png</key>
            <key type="filename">anims/11/23.png</key>
            <key type="filename">anims/11/24.png</key>
            <key type="filename">anims/11/25.png</key>
            <key type="filename">anims/11/26.png</key>
            <key type="filename">anims/11/27.png</key>
            <key type="filename">anims/11/28.png</key>
            <key type="filename">anims/11/29.png</key>
            <key type="filename">anims/11/3.png</key>
            <key type="filename">anims/11/30.png</key>
            <key type="filename">anims/11/31.png</key>
            <key type="filename">anims/11/32.png</key>
            <key type="filename">anims/11/33.png</key>
            <key type="filename">anims/11/34.png</key>
            <key type="filename">anims/11/35.png</key>
            <key type="filename">anims/11/36.png</key>
            <key type="filename">anims/11/37.png</key>
            <key type="filename">anims/11/4.png</key>
            <key type="filename">anims/11/5.png</key>
            <key type="filename">anims/11/6.png</key>
            <key type="filename">anims/11/7.png</key>
            <key type="filename">anims/11/8.png</key>
            <key type="filename">anims/11/9.png</key>
            <key type="filename">anims/12/1.png</key>
            <key type="filename">anims/12/10.png</key>
            <key type="filename">anims/12/11.png</key>
            <key type="filename">anims/12/12.png</key>
            <key type="filename">anims/12/13.png</key>
            <key type="filename">anims/12/14.png</key>
            <key type="filename">anims/12/15.png</key>
            <key type="filename">anims/12/16.png</key>
            <key type="filename">anims/12/17.png</key>
            <key type="filename">anims/12/18.png</key>
            <key type="filename">anims/12/19.png</key>
            <key type="filename">anims/12/2.png</key>
            <key type="filename">anims/12/20.png</key>
            <key type="filename">anims/12/21.png</key>
            <key type="filename">anims/12/22.png</key>
            <key type="filename">anims/12/23.png</key>
            <key type="filename">anims/12/24.png</key>
            <key type="filename">anims/12/25.png</key>
            <key type="filename">anims/12/26.png</key>
            <key type="filename">anims/12/27.png</key>
            <key type="filename">anims/12/28.png</key>
            <key type="filename">anims/12/29.png</key>
            <key type="filename">anims/12/3.png</key>
            <key type="filename">anims/12/30.png</key>
            <key type="filename">anims/12/31.png</key>
            <key type="filename">anims/12/32.png</key>
            <key type="filename">anims/12/33.png</key>
            <key type="filename">anims/12/34.png</key>
            <key type="filename">anims/12/35.png</key>
            <key type="filename">anims/12/36.png</key>
            <key type="filename">anims/12/37.png</key>
            <key type="filename">anims/12/38.png</key>
            <key type="filename">anims/12/39.png</key>
            <key type="filename">anims/12/4.png</key>
            <key type="filename">anims/12/40.png</key>
            <key type="filename">anims/12/41.png</key>
            <key type="filename">anims/12/5.png</key>
            <key type="filename">anims/12/6.png</key>
            <key type="filename">anims/12/7.png</key>
            <key type="filename">anims/12/8.png</key>
            <key type="filename">anims/12/9.png</key>
            <key type="filename">anims/3/9.png</key>
            <key type="filename">anims/4/9.png</key>
            <key type="filename">anims/5/9.png</key>
            <key type="filename">anims/6/39.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>40,40,80,80</rect>
                <key>scale9Paddings</key>
                <rect>40,40,80,80</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/10/18.png</key>
            <key type="filename">anims/10/19.png</key>
            <key type="filename">anims/10/20.png</key>
            <key type="filename">anims/10/21.png</key>
            <key type="filename">anims/10/22.png</key>
            <key type="filename">anims/10/23.png</key>
            <key type="filename">anims/10/24.png</key>
            <key type="filename">anims/10/25.png</key>
            <key type="filename">anims/10/26.png</key>
            <key type="filename">anims/10/27.png</key>
            <key type="filename">anims/10/28.png</key>
            <key type="filename">anims/10/29.png</key>
            <key type="filename">anims/10/30.png</key>
            <key type="filename">anims/10/31.png</key>
            <key type="filename">anims/10/32.png</key>
            <key type="filename">anims/10/33.png</key>
            <key type="filename">anims/10/34.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,38,76,76</rect>
                <key>scale9Paddings</key>
                <rect>38,38,76,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/10/35.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,38,75,76</rect>
                <key>scale9Paddings</key>
                <rect>38,38,75,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/2/1.png</key>
            <key type="filename">anims/2/17.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,35,58,69</rect>
                <key>scale9Paddings</key>
                <rect>29,35,58,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/2/10.png</key>
            <key type="filename">anims/2/11.png</key>
            <key type="filename">anims/2/12.png</key>
            <key type="filename">anims/2/13.png</key>
            <key type="filename">anims/2/5.png</key>
            <key type="filename">anims/2/6.png</key>
            <key type="filename">anims/2/7.png</key>
            <key type="filename">anims/2/8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,38,64,75</rect>
                <key>scale9Paddings</key>
                <rect>32,38,64,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/2/14.png</key>
            <key type="filename">anims/2/4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,37,63,75</rect>
                <key>scale9Paddings</key>
                <rect>32,37,63,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/2/15.png</key>
            <key type="filename">anims/2/16.png</key>
            <key type="filename">anims/2/2.png</key>
            <key type="filename">anims/2/3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,37,63,73</rect>
                <key>scale9Paddings</key>
                <rect>31,37,63,73</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/2/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,40,69,79</rect>
                <key>scale9Paddings</key>
                <rect>35,40,69,79</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/3/1.png</key>
            <key type="filename">anims/3/17.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,37,71,73</rect>
                <key>scale9Paddings</key>
                <rect>36,37,71,73</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/3/10.png</key>
            <key type="filename">anims/3/11.png</key>
            <key type="filename">anims/3/12.png</key>
            <key type="filename">anims/3/13.png</key>
            <key type="filename">anims/3/5.png</key>
            <key type="filename">anims/3/6.png</key>
            <key type="filename">anims/3/7.png</key>
            <key type="filename">anims/3/8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,40,77,79</rect>
                <key>scale9Paddings</key>
                <rect>39,40,77,79</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/3/14.png</key>
            <key type="filename">anims/3/4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,40,77,79</rect>
                <key>scale9Paddings</key>
                <rect>38,40,77,79</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/3/15.png</key>
            <key type="filename">anims/3/16.png</key>
            <key type="filename">anims/3/2.png</key>
            <key type="filename">anims/3/3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,39,76,79</rect>
                <key>scale9Paddings</key>
                <rect>38,39,76,79</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/4/1.png</key>
            <key type="filename">anims/4/17.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,36,63,73</rect>
                <key>scale9Paddings</key>
                <rect>31,36,63,73</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/4/13.png</key>
            <key type="filename">anims/4/14.png</key>
            <key type="filename">anims/4/4.png</key>
            <key type="filename">anims/4/5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,39,68,78</rect>
                <key>scale9Paddings</key>
                <rect>34,39,68,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/4/15.png</key>
            <key type="filename">anims/4/16.png</key>
            <key type="filename">anims/4/2.png</key>
            <key type="filename">anims/4/3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,39,67,77</rect>
                <key>scale9Paddings</key>
                <rect>33,39,67,77</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/5/1.png</key>
            <key type="filename">anims/5/17.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,35,71,71</rect>
                <key>scale9Paddings</key>
                <rect>36,35,71,71</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/5/10.png</key>
            <key type="filename">anims/5/11.png</key>
            <key type="filename">anims/5/12.png</key>
            <key type="filename">anims/5/6.png</key>
            <key type="filename">anims/5/7.png</key>
            <key type="filename">anims/5/8.png</key>
            <key type="filename">anims/9/18.png</key>
            <key type="filename">anims/9/19.png</key>
            <key type="filename">anims/9/20.png</key>
            <key type="filename">anims/9/21.png</key>
            <key type="filename">anims/9/22.png</key>
            <key type="filename">anims/9/23.png</key>
            <key type="filename">anims/9/24.png</key>
            <key type="filename">anims/9/25.png</key>
            <key type="filename">anims/9/26.png</key>
            <key type="filename">anims/9/27.png</key>
            <key type="filename">anims/9/28.png</key>
            <key type="filename">anims/9/29.png</key>
            <key type="filename">anims/9/30.png</key>
            <key type="filename">anims/9/31.png</key>
            <key type="filename">anims/9/32.png</key>
            <key type="filename">anims/9/33.png</key>
            <key type="filename">anims/9/34.png</key>
            <key type="filename">anims/9/35.png</key>
            <key type="filename">anims/9/36.png</key>
            <key type="filename">anims/9/37.png</key>
            <key type="filename">anims/9/38.png</key>
            <key type="filename">anims/9/39.png</key>
            <key type="filename">anims/9/40.png</key>
            <key type="filename">anims/9/41.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,39,77,77</rect>
                <key>scale9Paddings</key>
                <rect>39,39,77,77</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/5/13.png</key>
            <key type="filename">anims/5/5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,38,77,77</rect>
                <key>scale9Paddings</key>
                <rect>39,38,77,77</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/5/15.png</key>
            <key type="filename">anims/5/16.png</key>
            <key type="filename">anims/5/2.png</key>
            <key type="filename">anims/5/3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,38,77,75</rect>
                <key>scale9Paddings</key>
                <rect>38,38,77,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/6/1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,31,65,61</rect>
                <key>scale9Paddings</key>
                <rect>32,31,65,61</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/6/10.png</key>
            <key type="filename">anims/6/11.png</key>
            <key type="filename">anims/6/12.png</key>
            <key type="filename">anims/6/13.png</key>
            <key type="filename">anims/6/14.png</key>
            <key type="filename">anims/6/15.png</key>
            <key type="filename">anims/6/16.png</key>
            <key type="filename">anims/6/17.png</key>
            <key type="filename">anims/6/18.png</key>
            <key type="filename">anims/6/19.png</key>
            <key type="filename">anims/6/20.png</key>
            <key type="filename">anims/6/21.png</key>
            <key type="filename">anims/6/22.png</key>
            <key type="filename">anims/6/23.png</key>
            <key type="filename">anims/6/24.png</key>
            <key type="filename">anims/6/25.png</key>
            <key type="filename">anims/6/26.png</key>
            <key type="filename">anims/6/27.png</key>
            <key type="filename">anims/6/28.png</key>
            <key type="filename">anims/6/29.png</key>
            <key type="filename">anims/6/30.png</key>
            <key type="filename">anims/6/31.png</key>
            <key type="filename">anims/6/32.png</key>
            <key type="filename">anims/6/33.png</key>
            <key type="filename">anims/6/34.png</key>
            <key type="filename">anims/6/35.png</key>
            <key type="filename">anims/6/36.png</key>
            <key type="filename">anims/6/37.png</key>
            <key type="filename">anims/6/38.png</key>
            <key type="filename">anims/6/4.png</key>
            <key type="filename">anims/6/5.png</key>
            <key type="filename">anims/6/6.png</key>
            <key type="filename">anims/6/7.png</key>
            <key type="filename">anims/6/8.png</key>
            <key type="filename">anims/6/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,33,69,65</rect>
                <key>scale9Paddings</key>
                <rect>35,33,69,65</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/6/2.png</key>
            <key type="filename">anims/6/3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,33,68,65</rect>
                <key>scale9Paddings</key>
                <rect>34,33,68,65</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/7/1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,65,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,65,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/7/10.png</key>
            <key type="filename">anims/7/11.png</key>
            <key type="filename">anims/7/12.png</key>
            <key type="filename">anims/7/13.png</key>
            <key type="filename">anims/7/14.png</key>
            <key type="filename">anims/7/15.png</key>
            <key type="filename">anims/7/16.png</key>
            <key type="filename">anims/7/17.png</key>
            <key type="filename">anims/7/18.png</key>
            <key type="filename">anims/7/19.png</key>
            <key type="filename">anims/7/2.png</key>
            <key type="filename">anims/7/20.png</key>
            <key type="filename">anims/7/21.png</key>
            <key type="filename">anims/7/3.png</key>
            <key type="filename">anims/7/4.png</key>
            <key type="filename">anims/7/5.png</key>
            <key type="filename">anims/7/6.png</key>
            <key type="filename">anims/7/7.png</key>
            <key type="filename">anims/7/8.png</key>
            <key type="filename">anims/7/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,33,73,65</rect>
                <key>scale9Paddings</key>
                <rect>36,33,73,65</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/7/22.png</key>
            <key type="filename">anims/7/23.png</key>
            <key type="filename">anims/7/24.png</key>
            <key type="filename">anims/7/25.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,33,73,66</rect>
                <key>scale9Paddings</key>
                <rect>36,33,73,66</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/7/26.png</key>
            <key type="filename">anims/7/27.png</key>
            <key type="filename">anims/7/28.png</key>
            <key type="filename">anims/7/29.png</key>
            <key type="filename">anims/7/30.png</key>
            <key type="filename">anims/7/31.png</key>
            <key type="filename">anims/7/32.png</key>
            <key type="filename">anims/7/33.png</key>
            <key type="filename">anims/7/34.png</key>
            <key type="filename">anims/7/35.png</key>
            <key type="filename">anims/7/36.png</key>
            <key type="filename">anims/7/37.png</key>
            <key type="filename">anims/7/38.png</key>
            <key type="filename">anims/7/39.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,33,71,66</rect>
                <key>scale9Paddings</key>
                <rect>36,33,71,66</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/8/1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,32,57,64</rect>
                <key>scale9Paddings</key>
                <rect>29,32,57,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/8/10.png</key>
            <key type="filename">anims/8/11.png</key>
            <key type="filename">anims/8/12.png</key>
            <key type="filename">anims/8/13.png</key>
            <key type="filename">anims/8/14.png</key>
            <key type="filename">anims/8/15.png</key>
            <key type="filename">anims/8/16.png</key>
            <key type="filename">anims/8/17.png</key>
            <key type="filename">anims/8/18.png</key>
            <key type="filename">anims/8/19.png</key>
            <key type="filename">anims/8/20.png</key>
            <key type="filename">anims/8/21.png</key>
            <key type="filename">anims/8/22.png</key>
            <key type="filename">anims/8/23.png</key>
            <key type="filename">anims/8/24.png</key>
            <key type="filename">anims/8/25.png</key>
            <key type="filename">anims/8/26.png</key>
            <key type="filename">anims/8/27.png</key>
            <key type="filename">anims/8/28.png</key>
            <key type="filename">anims/8/29.png</key>
            <key type="filename">anims/8/30.png</key>
            <key type="filename">anims/8/31.png</key>
            <key type="filename">anims/8/32.png</key>
            <key type="filename">anims/8/33.png</key>
            <key type="filename">anims/8/34.png</key>
            <key type="filename">anims/8/35.png</key>
            <key type="filename">anims/8/4.png</key>
            <key type="filename">anims/8/5.png</key>
            <key type="filename">anims/8/6.png</key>
            <key type="filename">anims/8/7.png</key>
            <key type="filename">anims/8/8.png</key>
            <key type="filename">anims/8/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,34,61,69</rect>
                <key>scale9Paddings</key>
                <rect>31,34,61,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/8/2.png</key>
            <key type="filename">anims/8/3.png</key>
            <key type="filename">anims/8/38.png</key>
            <key type="filename">anims/8/39.png</key>
            <key type="filename">anims/8/40.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,33,61,67</rect>
                <key>scale9Paddings</key>
                <rect>31,33,61,67</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">anims/8/36.png</key>
            <key type="filename">anims/8/37.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,34,61,67</rect>
                <key>scale9Paddings</key>
                <rect>31,34,61,67</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>anims</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
