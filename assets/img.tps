<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.10.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantHigh</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>1</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>2</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png8</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>img.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">PremultiplyAlpha</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>0</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">img/Double/DoubleBack.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>238,84,477,168</rect>
                <key>scale9Paddings</key>
                <rect>238,84,477,168</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/Double/DoubleBtnBlack.png</key>
            <key type="filename">img/Double/DoubleBtnRed.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>200,200,400,400</rect>
                <key>scale9Paddings</key>
                <rect>200,200,400,400</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/Double/DoubleBuba/DoubleBuba10.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBuba2.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBuba3.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBuba4.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBuba5.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBuba6.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBuba7.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBuba8.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBuba9.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBubaA.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBubaJ.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBubaK.png</key>
            <key type="filename">img/Double/DoubleBuba/DoubleBubaQ.png</key>
            <key type="filename">img/Double/DoubleCardBack.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirva10.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirva2.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirva3.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirva4.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirva5.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirva6.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirva7.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirva8.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirva9.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirvaA.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirvaJ.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirvaK.png</key>
            <key type="filename">img/Double/DoubleChirva/DoubleChirvaQ.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrest10.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrest2.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrest3.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrest4.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrest5.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrest6.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrest7.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrest8.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrest9.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrestA.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrestJ.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrestK.png</key>
            <key type="filename">img/Double/DoubleKrest/DoubleKrestQ.png</key>
            <key type="filename">img/Double/DoublePika/DoublePika10.png</key>
            <key type="filename">img/Double/DoublePika/DoublePika2.png</key>
            <key type="filename">img/Double/DoublePika/DoublePika3.png</key>
            <key type="filename">img/Double/DoublePika/DoublePika4.png</key>
            <key type="filename">img/Double/DoublePika/DoublePika5.png</key>
            <key type="filename">img/Double/DoublePika/DoublePika6.png</key>
            <key type="filename">img/Double/DoublePika/DoublePika7.png</key>
            <key type="filename">img/Double/DoublePika/DoublePika8.png</key>
            <key type="filename">img/Double/DoublePika/DoublePika9.png</key>
            <key type="filename">img/Double/DoublePika/DoublePikaA.png</key>
            <key type="filename">img/Double/DoublePika/DoublePikaJ.png</key>
            <key type="filename">img/Double/DoublePika/DoublePikaK.png</key>
            <key type="filename">img/Double/DoublePika/DoublePikaQ.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>44,66,88,132</rect>
                <key>scale9Paddings</key>
                <rect>44,66,88,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/Double/DoubleCardBuba.png</key>
            <key type="filename">img/Double/DoubleCardChirva.png</key>
            <key type="filename">img/Double/DoubleCardKrest.png</key>
            <key type="filename">img/Double/DoubleCardPika.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>40,40,80,80</rect>
                <key>scale9Paddings</key>
                <rect>40,40,80,80</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/Double/DoubleCenterCard.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,40,59,80</rect>
                <key>scale9Paddings</key>
                <rect>30,40,59,80</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/back/BackBonus.png</key>
            <key type="filename">img/back/BackFreeGame.png</key>
            <key type="filename">img/back/BackFreeGameAll.png</key>
            <key type="filename">img/back/scr_preload.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>320,180,640,360</rect>
                <key>scale9Paddings</key>
                <rect>320,180,640,360</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/back/BackPortrait.png</key>
            <key type="filename">img/back/FoxPortrait.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>180,320,360,640</rect>
                <key>scale9Paddings</key>
                <rect>180,320,360,640</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/back/GameBonusText.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>136,35,273,69</rect>
                <key>scale9Paddings</key>
                <rect>136,35,273,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/back/GameBonusWin.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>136,56,271,111</rect>
                <key>scale9Paddings</key>
                <rect>136,56,271,111</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/back/orientation.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>78,46,156,93</rect>
                <key>scale9Paddings</key>
                <rect>78,46,156,93</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/back/tile.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/back/title.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>140,14,279,27</rect>
                <key>scale9Paddings</key>
                <rect>140,14,279,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/digits/1.png</key>
            <key type="filename">img/digits/2.png</key>
            <key type="filename">img/digits/3.png</key>
            <key type="filename">img/digits/4.png</key>
            <key type="filename">img/digits/5.png</key>
            <key type="filename">img/digits/7.png</key>
            <key type="filename">img/digits/8.png</key>
            <key type="filename">img/digits/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,12,29,23</rect>
                <key>scale9Paddings</key>
                <rect>14,12,29,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">img/digits/6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,12,29,24</rect>
                <key>scale9Paddings</key>
                <rect>14,12,29,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>img</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
